extends Node2D
class_name InitiativeBar

@export var battle:MultiplayerBattle:set=_set_battle
var creature_portraits:Dictionary={}
const CREATURE_PORTRAIT = preload("res://scenes/creature_portrait_2.tscn")
var PORTRAIT_HEIGHT = 200
@onready var current_creature_indicator = $CurrentCreatureIndicator
@onready var animation_player = $AnimationPlayer

func _ready():
	for child in get_children():
		if "example" in child.name:
			remove_child(child)
			child.queue_free()

func _set_battle(value:MultiplayerBattle)->void:
	if battle:
		battle.turn_manager.on_initiative_change.disconnect(handle_initiative_roll)
		battle.turn_manager.on_turn_start.disconnect(handle_turn_started)
		battle.turn_manager.on_turn_resume.disconnect(handle_turn_resume)
	battle = value
	if not is_node_ready():
		await ready
	if not battle.is_node_ready():
		await battle.ready
	battle.turn_manager.on_turn_resume.connect(handle_turn_resume)
	battle.turn_manager.on_initiative_change.connect(handle_initiative_roll)
	battle.turn_manager.on_turn_start.connect(handle_turn_started)
	
	update_initiative()
func handle_initiative_roll():
	update_initiative()
func handle_turn_started(_creature:Creature):
	udpate_current_creature_indicator()
func handle_turn_resume(_creature:Creature):
	udpate_current_creature_indicator()
func update_initiative():
	var creatures = battle.creatures
	var i = 0
	for creature in creatures:
		if not creature in creature_portraits:
			var portrait = CREATURE_PORTRAIT.instantiate()
			portrait.creature = creature
			add_child(portrait)
			portrait.transform.origin = Vector2(0,get_y_for_init_index(i))
			creature_portraits[creature] = portrait
			i+=1
	var move_animations:Array[Tween] = []
	i =0
	for creature in battle.turn_manager.initiative:
		var target_y = get_y_for_init_index(i)
		move_animations.append(animate_moving_to_place(creature,target_y))
		i+=1
	for animation in move_animations:
		if animation.is_running():
			await animation.finished
		
	udpate_current_creature_indicator()

func udpate_current_creature_indicator():
	if battle.turn_manager.has_battle_started():
		if not current_creature_indicator.visible:
			animation_player.play("appear_current_indicator")
		
		var current_creature_portrait = creature_portraits[battle.turn_manager.get_current_creature()]
		var tween:Tween = GameEventService.get_tween()
		tween.tween_property(current_creature_indicator,"position",Vector2(current_creature_indicator.position.x,current_creature_portrait.transform.origin.y),0.3).set_ease(Tween.EASE_OUT)
		tween.play()
	
func animate_moving_to_place(creature:Creature,target:float):
	var portrait = creature_portraits[creature]
	var tween = GameEventService.get_tween()
	tween.tween_property(portrait,"position",Vector2(portrait.position.x,target),0.7).set_trans(Tween.TRANS_QUAD)
	tween.play()
	return tween
func get_y_for_init_index(index:int)->float:
	return PORTRAIT_HEIGHT*index
