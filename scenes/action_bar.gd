extends Node2D
var BUTTON_OFFSET = 100
const ACTION_BUTTON = preload("res://scenes/action_button.tscn")
const RESOURCE_ICON_VIEW = preload("res://scenes/resource_icon_view.tscn")
const RESOURCE_LEVEL_PICKER = preload("res://scenes/action_bar/resource_level_picker.tscn")

var RESOURCE_OFFSEt=-50
@export var creature:Creature:set=_set_creature
var turn_manager:TurnManager:set=_set_turn_manager


@onready var end_turn_button:TextureButton = $EndTurnButton
@onready var actions = $PanelContainer/Actions
@onready var resource_container = $PanelContainer/Control/ResourceContainer
@onready var message = $PanelContainer/message

@onready var reaction_confirmation = $ReactionConfirmation
@onready var concentration_button = $ConcentrationButton

var resource_level_picker :ResourceLevelPicker

func _ready():
	self.hide()
	CreaturesEventBus.on_reaction_confirmation.connect(handle_reaction_confirmation)
	GameEventService.on_battle_start.connect(func(_c):self.show())
	GameEventService.on_battle_resume.connect(handle_battle_resume)
	end_turn_button.pressed.connect(handle_end_turn_button_pressed)
	reaction_confirmation.hide()
	reaction_confirmation.on_done.connect(handle_reaction_confirmation_done)
	concentration_button.pressed.connect(handle_concentration_button_pressed)
func refresh_resource_list():
	for resource in resource_container.get_children():
		resource_container.remove_child(resource)
		resource.queue_free()
	if not creature:
		return
	for resource in creature.battle_resources.get_resources():
		var resource_display = RESOURCE_ICON_VIEW.instantiate()
		resource_display.amount = creature.battle_resources.get_resource(resource)
		resource_display.depletted = creature.battle_resources.get_default_value(resource)-creature.battle_resources.get_resource(resource)
		resource_display.resource = resource
		resource_display.position = Vector2(RESOURCE_OFFSEt*resource_container.get_child_count(),0)
		resource_container.add_child(resource_display)
func refresh_action_list():
	if not turn_manager.has_battle_started():
		return
	for action in actions.get_children():
		actions.remove_child(action)
		action.queue_free()

	if not creature:
		return
	var current_creature = creature
	for action in current_creature.actions.get_children():
		if action.design.in_action_bar:
			add_action(action)
func add_action(action:CreatureAction):
	var button = ACTION_BUTTON.instantiate()
	button.action = action
	button.on_selected.connect(handle_action_selected)
	actions.add_child(button)
func select_resource_level(action:CreatureAction,levels:Array[BattleResource],tooltip:String,level_picked_handler:Callable,current_level:BattleResource):
	resource_level_picker = RESOURCE_LEVEL_PICKER.instantiate()
	resource_level_picker.levels = levels
	resource_level_picker.action = action
	resource_level_picker.tooltip = tooltip
	resource_level_picker.current_level = current_level
	resource_level_picker.on_level_picked.connect(level_picked_handler)
	actions.hide()
	add_child(resource_level_picker)
	resource_level_picker.tree_exited.connect(func():actions.show())
	return resource_level_picker
func handle_action_selected(action):
	for button in actions.get_children():
		if button.action!=action:
			button.deselect()
func remove_resource_selection():
	if resource_level_picker.get_parent()!=null:
		resource_level_picker.get_parent().remove_child(resource_level_picker)
		resource_level_picker.queue_free()
	actions.show()
func handle_battle_resume(_c):
	refresh()
func handle_turn_resume(_c):
	refresh()
func refresh():
	var c = turn_manager.get_current_creature()
	self.show()
	if not c:
		self.hide()
	elif MultiplayerService.is_my_creature(c) and not c.is_ai:
		creature=turn_manager.get_current_creature()
		refresh_action_list()
		refresh_resource_list()
		end_turn_button.show()
		message.hide()
		actions.show()
		resource_container.show()
	else:
		end_turn_button.hide()
		resource_container.hide()
		message.show()
		message.text = "%s is playing"%[c.game_name]
		actions.hide()
func handle_turn_start(_c:Creature):
	refresh()
func handle_end_turn_button_pressed():
	for button  in actions.get_children():
		button.deselect()
	actions.hide()
	message.text= "Waiting for turn end"
	message.show()
	end_turn_button.hide()
	turn_manager.end_turn.rpc()

func handle_concentration_button_pressed():
	if creature.concentration:
		creature.end_concentration.rpc()
#region Reaction Handlers
func handle_reaction_confirmation(asking_creature:Creature,reaction_condition:CreatureCondition):
	if creature!=asking_creature:
		return
	reaction_confirmation.reaction= reaction_condition
	reaction_confirmation.show()
	actions.hide()
func handle_reaction_confirmation_done():
	reaction_confirmation.hide()
	actions.show()

#endregion
#region Creature Change Handlers
func handle_actions_change(_c:Creature):
	refresh_action_list()
func handle_resource_change(_r:CreatureBattleResources):
	refresh_resource_list()
	for button in actions.get_children():
		button.refresh_texture()
func handle_concentration_change(_c:Creature):
	if creature.concentration:
		concentration_button.show()
		concentration_button.texture_normal = creature.concentration.spell.icon
	else:
		concentration_button.hide()
#endregion
#region Getters/Setters

func _set_turn_manager(value:TurnManager):
	if turn_manager:
		turn_manager.on_turn_start.disconnect(handle_turn_start)
		turn_manager.on_turn_resume.disconnect(handle_turn_resume)
	turn_manager=value
	turn_manager.on_turn_start.connect(handle_turn_start)
	turn_manager.on_turn_resume.connect(handle_turn_resume)
	
	if not is_node_ready():
		await ready
	
	if turn_manager.has_battle_started():
		creature = turn_manager.get_current_creature()
func _set_creature(value:Creature):
	if creature:
		creature.battle_resources.on_resource_change.disconnect(handle_resource_change)
		creature.on_actions_change.disconnect(handle_actions_change)
		creature.on_concentrating.disconnect(handle_concentration_change)
	creature = value
	if not is_node_ready():
		await ready
	refresh_action_list()
	refresh_resource_list()
	if creature:
		handle_concentration_change(creature)	
		creature.battle_resources.on_resource_change.connect(handle_resource_change)
		creature.on_actions_change.connect(handle_actions_change)
		creature.on_concentrating.connect(handle_concentration_change)

#endregion
