extends VBoxContainer

@onready var creature_name = $CreatureName
@onready var creature_icon = $PanelContainer/CreatureIcon
@onready var health_bar = $HealthBar
const CREATURE_CHARACTER_SHEET = preload("res://scenes/creature/creature_character_sheet.tscn")
var creature		:Creature:set=_set_creature

func _ready():
	creature_icon.pressed.connect(handle_creature_pressed)

func _set_creature(value:Creature):
	creature = value
	if not is_node_ready():
		await ready
	creature_name.text=creature.game_name
	creature_icon.texture_normal = creature.icon
	health_bar.text = "%d/%d"%[creature.health,creature.max_health]
func handle_creature_pressed():
	var stat_popup = PopupPanel.new()
	var character_sheet = CREATURE_CHARACTER_SHEET.instantiate()
	character_sheet.creature = creature
	stat_popup.add_child(character_sheet)
	add_child(stat_popup)
	stat_popup.min_size = Vector2(480,700)
	stat_popup.popup()
