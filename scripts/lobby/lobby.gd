class_name Lobby
extends Control
const BATTLE_SCENE = preload("res://scenes/multiplayer_battle.tscn")
const LOBBY_CREATURE_PORTRAIT = preload("res://scenes/lobby/lobby_creature_portrait.tscn")
var CREATOR_SCENE 		=preload("res://scenes/creature/creation/creature_creation.tscn")
@onready var game_state = $GameState : set=_set_game_state

@onready var team_list = $MainPanel/VBoxContainer/TeamList
@onready var add_member_template = $AddButtonTemplate

@onready var save = $MainPanel/VBoxContainer/Saves/VBoxContainer/Save
@onready var game_save_name = $MainPanel/VBoxContainer/Saves/VBoxContainer/GameSaveName
@onready var load_button = $MainPanel/VBoxContainer/Saves/VBoxContainer/Load
@onready var start_button = $MainPanel/VBoxContainer/hbox/Start
var running_battle:MultiplayerBattle
var teams:
	get:
		return game_state.teams.get_children()
var creatures:
	get:
		return game_state.creatures.get_children()
var save_string :String
var _save_loaded	:bool = false

signal on_save_loaded(save_string)

# Called when the node enters the scene tree for the first time.
func _ready():
	refresh_teams()
	GameEventService.game_state = game_state
	save.pressed.connect(handle_save_pressed)
	load_button.pressed.connect(handle_load_pressed)
	start_button.pressed.connect(handle_start_pressed)
func _exit_tree():
	if running_battle:
		running_battle.get_parent().remove_child.call_deferred(running_battle)
		running_battle.queue_free()
func handle_start_pressed():
	if multiplayer.is_server():
		start_game()
func handle_save_pressed():
	if OS.is_debug_build():
		var file_dialog =FileDialog.new()
		file_dialog.add_filter("*.save")
		file_dialog.access = FileDialog.ACCESS_RESOURCES
		file_dialog.file_mode = FileDialog.FILE_MODE_SAVE_FILE
		file_dialog.min_size = Vector2(280,280)
		add_child(file_dialog)
		file_dialog.popup_centered()
		var file =await file_dialog.file_selected
		GameElementSaver.save(file,game_state)
func handle_load_pressed():
	if OS.is_debug_build():
		var file_dialog =FileDialog.new()
		file_dialog.add_filter("*.save")
		file_dialog.access = FileDialog.ACCESS_RESOURCES
		file_dialog.file_mode = FileDialog.FILE_MODE_OPEN_FILE
		file_dialog.min_size = Vector2(280,280)
		add_child(file_dialog)
		file_dialog.popup_centered()
		var file =await file_dialog.file_selected
		load_save(file)
func load_save(save_name:String):
	save_string =GameElementSaver.get_save_string(save_name)
	load_string(save_string)
@rpc("authority","call_local","reliable")
func load_string(target_save_string:String):
	var new_game_state = GameElementSaver.load_string(target_save_string)
	remove_child(game_state)
	game_state.queue_free()
	game_state = new_game_state
	add_child(game_state)
	game_state.name = "GameState"
	_save_loaded = true
	on_save_loaded.emit(save_string)
	refresh_teams()
@rpc("any_peer","call_local","reliable")
func refresh_teams():
	for child in team_list.get_children():
		team_list.remove_child(child)
		child.queue_free()
	for team in game_state.teams.get_children():
		var label = Label.new()
		label.text = team.team_name
		team_list.add_child(label)
		var member_list = HBoxContainer.new()
		for member in team.members:
			var portrait = LOBBY_CREATURE_PORTRAIT.instantiate()
			portrait.creature = member
			member_list.add_child(portrait)
		var add_member_button=add_member_template.duplicate()
		add_member_button.pressed.connect(handle_add_team_member.bind(team))
		add_member_button.show()
		member_list.add_child(add_member_button)
		team_list.add_child(member_list)
func handle_add_team_member(team:TeamConfig):
	var creator = CREATOR_SCENE.instantiate()
	creator.on_creature_done.connect(add_creature_to_team.bind(team,creator),CONNECT_ONE_SHOT)
	get_tree().root.call_deferred("add_child",creator)
	get_tree().set_deferred("current_scene",creator)
	
func add_creature_to_team(creature:Creature,team:TeamConfig,creator):
	creature.get_parent().remove_child(creature)		
	get_tree().root.call_deferred("remove_child",creator)
	get_tree().set_deferred("current_scene",self)
	game_state.add_creature.rpc(SI.s(creature))
	team.add_member.rpc(SI.s(creature.unique_id))
	refresh_teams.rpc()
	creature.queue_free()
func start_game():
	print("start_game")
	init_battle.rpc()
@rpc("authority","call_local","reliable")
func init_battle():
	register_teams()
	if multiplayer.is_server():
		register_players()
	
	if game_state.battle!=null:
		running_battle = game_state.battle
		running_battle.name = "CurrentBattle"
	else:
		running_battle = BATTLE_SCENE.instantiate()
		running_battle.creatures.append_array(creatures)
		running_battle.game_state=game_state
		running_battle.name = "CurrentBattle"
	get_tree().root.call_deferred("add_child",running_battle)
	get_tree().set_deferred("current_scene",running_battle)
	await running_battle.ready
	await MultiplayerService.sync_clients("Lobby:InitBattle")		
	if multiplayer.is_server():
		if running_battle.is_started:
			running_battle.resume_battle.rpc()
		else:
			running_battle.start_battle.rpc()


func register_teams():
	for team in teams:
		TeamService.register_team(team.team_name,team.team_size)
	for team in teams:
		for ally in team.team_allies:
			TeamService.make_allies(team.team_name,ally)
		for enemy in team.team_hostiles:
			TeamService.make_hostile(team.team_name,enemy)
		for creature in team.members:
			TeamService.add_to_team(team.team_name,creature)
func register_players():
	
	var team_index  = 0
	var team_size = teams[team_index].team_size
	MultiplayerService.register_player.rpc(1,1)
	var team = teams[team_index]
	if team_size == 0:
		team_index+=1
		if team_index<teams.size():
			team_size = teams[team_index].team_size
	MultiplayerService.assign_team.rpc(team.team_name,1)
	team_size-=1
	for peer in multiplayer.get_peers():
		if team_index >= teams.size():
			MultiplayerService.register_spectator.rpc(peer,peer)
		else:
			MultiplayerService.register_player.rpc(peer,peer)
			team = teams[team_index]
			team_size-=1			
			while team_size <= 0 and team_index<teams.size():
				team_index+=1
				if team_index<teams.size():
					team_size = teams[team_index].team_size
			MultiplayerService.assign_team.rpc(team.team_name,peer)
func hosting():
	multiplayer.peer_connected.connect(handle_peer_connected)
func is_client():
	pass
func handle_peer_connected(peer_id:int):
	if peer_id ==1:
		return
	if not _save_loaded:
		var _s=await on_save_loaded
	load_string.rpc_id(peer_id,save_string)
func _set_game_state(value:GameState):
	game_state =value
	GameEventService.game_state = game_state
