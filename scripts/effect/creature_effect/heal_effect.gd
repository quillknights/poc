class_name HealEffect
extends CreatureEffect

func apply(creature:Creature):
	GameEventService.enter_event()
	var value = get_total_value(creature)
	creature.heal.rpc(value)
	await GameEventService.leave_event(get_game_event(value))
func get_game_event(value:int)->GameEvent:
	var event = GameEvent.new()
	event.type = GameEvent.Type.Heal
	event.sentence = "Healed by %d"%[value]
	return event
