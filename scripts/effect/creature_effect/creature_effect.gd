class_name CreatureEffect
extends Resource

@export var effect_name		:String
@export var icon			:Texture2D
@export var value_getters	:Array[ValueGetterDesign]


func apply(_creature:Creature):
	push_error("%s should have overiden it's parent apply method"%[effect_name])

func get_total_value(creature:Creature) -> int:
	var total = 0
	for value in value_getters:
		total+=value.get_value(creature)
	return total
		
