class_name SpellSlotEffect
extends CreatureEffect

func apply(creature:Creature):
	var value = get_total_value(creature)
	var resources = creature.battle_resources
	var count = 0
	for level in range(5,0,-1):
		if resources.has_slot_level(level) and value >=level:
			var spell_slot = resources.get_spell_slot(level)
			if resources.get_spent_resource(spell_slot)>0:
				creature.resplenish_spell_slot.rpc(level,1)
				value -= level
				count +=1
	GameEventService.register_child(get_game_event(creature,count))
func get_game_event(creature:Creature,count:int)->GameEvent:
	var event = GameEvent.new()
	event.type = GameEvent.Type.Heal
	event.sentence = "%s Restored %s spell slots"%[creature.game_name,count]
	return event
