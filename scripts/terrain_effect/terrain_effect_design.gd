class_name TerrainEffectDesign
extends Resource

@export var unique_id:String:set=_set_unique_id 
@export var shape:TerrainShape
@export var name:String
@export var icon:Texture2D
@export var conditions:Array[CreatureConditionDesign] = []
@export var effect_script	:GDScript = load("res://scripts/terrain_effect/terrain_effect.gd")

var terrain:Terrain

func attach(t:Terrain,center:Vector2 = Vector2.INF):
	var node:TerrainEffect = effect_script.new()
	node.design = self
	node.center = center
	t.add_effect(node)

func _set_unique_id(_value:String):
	unique_id = resource_path
