class_name TerrainShape
extends Resource

@export var shape_view:PackedScene
@export var texture:Texture2D
@export var material:Material
var name:String
var terrain_shape_view:Node2D

func attach(terrain:Terrain,center:Vector2,effect:TerrainEffect):
	var polygons = get_init_shapes(effect)
	terrain_shape_view = shape_view.instantiate()
	terrain_shape_view.shape = self
	terrain_shape_view.polygons = polygons
	terrain_shape_view.center = center
	terrain.add_effect_shape(terrain_shape_view)
	return terrain_shape_view
func get_init_shapes(_effect:TerrainEffect) -> Array:
	push_error("Basic Shapes should not be used as is, but with one of it's child class")
	return []
