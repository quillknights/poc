extends Node2D
@export var shape:TerrainShapeRange

@onready var area:Area2D = $Area2D
@onready var shape_range = $Area2D/Range
@onready var range_view = $Sprite2D
@export var center:Vector2
@export var polygons :Array
signal on_creature_entered(creature:Creature)
signal on_creature_exited(creature:Creature)

# Called when the node enters the scene tree for the first time.
func _ready():
	shape_range.shape.radius = shape.shape_range*0.8
	area.area_shape_entered.connect(handle_area_shape_entered)
	area.area_exited.connect(handle_area_shape_exited)
	self.scale = Vector2.ONE*(float(HexCoordTool.TILE_PIXEL_HEIGHT)/2)*0.9
	range_view.texture = shape.texture
func handle_area_shape_entered(_area_rid: RID, other: Area2D, _area_shape_index: int, _local_shape_index: int):
	if other.get_parent() is CreatureMini:
		on_creature_entered.emit(other.get_parent().creature)
func handle_area_shape_exited(other_area: Area2D):
	if other_area:
		if other_area.get_parent() is CreatureMini:
			on_creature_exited.emit(other_area.get_parent().creature)
