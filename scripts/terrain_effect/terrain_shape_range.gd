class_name TerrainShapeRange
extends TerrainShape

@export var shape_range:float

func get_init_shapes(effect:TerrainEffect):
	var polygon = ShapeTools.generate_circle_polygon(shape_range,1000,effect.center)
	return [polygon]
