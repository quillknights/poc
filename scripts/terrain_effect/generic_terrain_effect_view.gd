class_name GenericTerrainShapeView
extends Node2D
@export var shape:TerrainShape
@onready var area = $Area

@onready var polygons = $Polygons

const TEXTURE_MASK_SHADER = preload("res://shaders/texture_mask_shader.gdshader")
var mask:ImageTexture
func _ready():
	shape.init_shapes()
	init_collision_area()
	init_view_polygon()
	mask = create_polygon_mask()
	#mask_view.texture=mask
	var mask_material = ShaderMaterial.new()
	mask_material.shader=TEXTURE_MASK_SHADER
	mask_material.set_shader_parameter("mask",mask)
	polygons.material = mask_material
	polygons.texture_offset = Vector2(128,128)
func init_view_polygon():
	var point_index = 0
	var flattened_points = PackedVector2Array()
	for polygon in shape.polygons:
		var polygon_group = PackedInt32Array([])
		flattened_points.append_array(polygon)
		for point in polygon:
			polygon_group.append(point_index)
			point_index+=1
		polygons.polygons.append(polygon_group)
	polygons.polygon = flattened_points
	#polygons.position=-get_shape_center()
func init_collision_area():
	for polygon in shape.polygons:
		var collision_polygon = CollisionPolygon2D.new()
		collision_polygon.polygon=polygon
		area.add_child(collision_polygon)
		
func get_shape_width()->float:
	return 200
func get_shape_height()->float:
	return 200
func is_in_shapes(x,y):
	for exclusion in shape.exclusions:
		if Geometry2D.is_point_in_polygon(Vector2(x,y),exclusion):
			return false
	for inclusion in shape.polygons:
		if Geometry2D.is_point_in_polygon(Vector2(x,y),inclusion):
			return true
	return false
func get_shape_center():
	return Vector2(100,100)
func create_polygon_mask()->ImageTexture:
	var width = get_shape_width()
	var height = get_shape_height()
	var center = get_shape_center()
	var dynImage = Image.create(width,height,false,Image.FORMAT_RGBA8)
	dynImage.fill(Color.TRANSPARENT)
	for x in range(width):
		for y in range(height):
			if is_in_shapes(x-center.x,y-center.y):
				dynImage.set_pixel(x,y,Color.WHITE)
	dynImage.save_png("res://test.png")
	var imageTexture = ImageTexture.create_from_image(dynImage)
	imageTexture.resource_name = "The created texture!"
	return imageTexture
	
		
