class_name TerrainEffect
extends Node

@export var design	:TerrainEffectDesign
@export var center	:Vector2
@export var terrain:Terrain

var _creature_conditions = {}
var shape_view = GenericTerrainShapeView
func _ready():
	shape_view = design.shape.attach(terrain,center,self)
	shape_view.on_creature_entered.connect(handle_creature_enter_shape)
	shape_view.on_creature_exited.connect(handle_creature_leave_shape)
	
func handle_creature_enter_shape(creature:Creature):
	if not MultiplayerService.is_server():
		return
	_creature_conditions[creature] = []
	for condition in design.conditions:
		var condition_id = MultiplayerService.generate_unique_id()
		creature.attach_new_condition.rpc(condition.resource_path,SI.s(condition_id))
		_creature_conditions[creature].append(condition_id)
		
func handle_creature_leave_shape(creature:Creature):
	if not MultiplayerService.is_server():
		return
	for condition in _creature_conditions[creature]:
		creature.detach_condition.rpc(SI.s(condition))
	_creature_conditions.erase(creature)


func get_class_save_id():
	return "TerrainEffect"

func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"design":saver.to_save(design),
		"center":saver.to_save(center),
		"terrain":saver.to_save(terrain)
		}
	return data

func from_save(save:Dictionary,saver:GameElementSaver):
	design  = saver.from_save(save["design"])
	center  = saver.from_save(save["center"])
	terrain  = saver.from_save(save["terrain"])
