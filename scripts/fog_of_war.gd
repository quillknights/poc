
extends Node2D
@onready var sub_viewport:SubViewport = $SubViewport
@onready var texture_rect = $TextureRect
@export var terrain :Terrain


func _process(_delta):
	update_fog_polygons()

func update_fog_polygons():
	for fov in get_tree().get_nodes_in_group("fov"):
		if not fov.has_meta("VisionPolygon"):
			var vision = Polygon2D.new()
			vision.color = Color.WHITE
			sub_viewport.add_child(vision)
			fov.set_meta("VisionPolygon",vision)
		var polygon = fov.get_meta("VisionPolygon")
		var vision_shape = fov.get_node("VisionShape")
		polygon.global_position = vision_shape.global_position
		polygon.polygon = vision_shape.polygon
		#polygon.polygon = Array(vision_shape.polygon).map(vision_shape.to_global)
		polygon.polygon = Array(vision_shape.polygon).map(polygon.to_local)
		#polygon.polygon = Array(vision_shape.polygon).map(vision_shape.to_global).map(polygon.to_local)
