extends Node
#Autoload handling all creature related events
#all signals through here must have as first argument a creature

signal on_creature_died(Creature)
signal on_reaction_confirmation(ReactionCondition)
