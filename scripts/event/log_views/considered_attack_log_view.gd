class_name ConsideredAttackLogView
extends Node2D

@onready var dice_roll_view :DiceRollView = $DiceRollView
@onready var ac_group = $ACGroup
@onready var ac_value = $ACGroup/ACValue
@onready var success_rate_view = $SuccessRate

@export var event:GameEvent

var ac:int
var success_rate:float
var attack_dice:Dice
func _ready():
	var payload = event.payload
	ac = payload["ac"]
	success_rate = payload["success_rate"]
	if payload["attack_roll"]["dice"]:
		attack_dice = BaseResourceService.get_resource(payload["attack_roll"]["dice"])
		dice_roll_view.preparing(attack_dice)
	success_rate_view.text = " %d%%"%[success_rate]
	ac_value.text = str(ac)
