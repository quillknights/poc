extends Node2D
@onready var attacker_icon = $AttackAniM/Attacker
@onready var attack_animation = $AttackAniM/AttackAnimation
@onready var target_icon = $AttackAniM/Target
@onready var dice_roll:DiceRollView = $DiceRollAttacked
@onready var ac_value = $AC/AC
@onready var ac_container = $AC
@onready var lost_hp = $LostHP

@export var weapon:Weapon
@export var attack_roll:Dictionary
@export var damage_roll:Dictionary
@export var ac:int
@export var event:GameEvent
@export var is_successfull:bool
@export var attacker:Creature
@export var target:Creature

func _ready():
	dice_roll.frozen=false	
	attack_roll = event.payload["attack_roll"]
	if "damage_roll" in event.payload and event.payload["damage_roll"]:
		damage_roll = event.payload["damage_roll"]
	ac 			= event.payload["ac"]
	is_successfull = event.payload["is_successfull"]
	attacker = BaseResourceService.get_resource(event.payload["attacker"])
	target = BaseResourceService.get_resource(event.payload["target_creature"])
	weapon = BaseResourceService.get_resource(event.payload["used_weapon"])
	attacker_icon.texture = attacker.icon
	target_icon.texture = target.icon
	attack_animation.sprite_frames = weapon.attack_animation
	if is_successfull:
		display_success()
	else:
		display_failed()
func display_success():
	
	attack_animation.play("successfullAttack")
	lost_hp.text= "-%dHP"%[damage_roll["result"]]
	ac_container.hide()
	await attack_animation.animation_finished		
	attack_animation.frame=5
	
	
func display_failed():
	attack_animation.play("failedAttack")
	lost_hp.hide()
	ac_value.text = str(ac)
	await attack_animation.animation_finished
	attack_animation.set_frame(5)
