extends Node

signal on_game_event(GameEvent)
signal on_battle_start(GameEvent)
signal on_battle_resume(GameEvent)
signal on_game_event_considered(GameEvent)
signal on_new_running_event(GameEvent)
signal on_pong_done_tweens()
signal on_event_done()
signal on_no_event_running()
signal on_peer_entered_event(int)
var battle_running		:bool=false
var game_state			:GameState = null
var running_animations : Array = []
var _event_stack		:Array=[]
var _animation_stack	:Array=[]
var _current_event_id	:int =-1
var _child_events		:Dictionary = {}
var is_event_running : bool:get=_get_is_event_running

var event_log			:Array[GameEvent]:
	get:
		return game_state.event_log
var last_event_id 		:int:
	get:
		return game_state.last_event_id

func instant_event(event:GameEvent):
	_instant_event.rpc(event.to_dict())
@rpc("authority","call_local","reliable")
func _instant_event(data:Dictionary):
	var event = GameEvent.new().from_dict(data)
	on_game_event.emit(event)
	event_log.append(event)
	
func enter_event()-> int:
	if not multiplayer.is_server():
		push_error("Only server can enter an event")
		return -1
	print("event entered")
	last_event_id+=1
	var event_id = last_event_id
	_enter_event(event_id)
	_enter_event.rpc(event_id)
	return event_id
func register_animation(animation):
	if not animation in running_animations:
		running_animations.append(animation)
func register_child(event:GameEvent):
	if not _current_event_id in _child_events:
		_child_events[_current_event_id] = []
	_child_events[_current_event_id].append(event)
@rpc("authority","call_remote","reliable")
func _enter_event(id : int):
	if _current_event_id !=-1:
		_interupt_event()
	_current_event_id = id
	
func _interupt_event():
	_event_stack.append(_current_event_id)
	_animation_stack.append(running_animations)

	for animation in running_animations:
		animation.pause()
	running_animations = []
	_print(str(_event_stack))
	
func leave_event(value:GameEvent):
	if not multiplayer.is_server():
		push_error("Only server can leave an event")
		return -1
	print("Event Left")
	if _current_event_id in _child_events:
		value.children.append_array(_child_events[_current_event_id])
	_child_events.erase(_current_event_id)
	_leave_event.rpc(value.to_dict())		
	await _leave_event(value.to_dict())
	
	
@rpc("authority","call_remote","reliable")
func _leave_event(event_value:Dictionary):
	var event = GameEvent.new().from_dict(event_value)
	for animation in running_animations:
		if is_instance_valid(animation) and animation.is_running():
			await animation.finished
	_current_event_id = -1
	if _event_stack:
		_resume_event()
		register_child(event)
	else:
		on_game_event.emit(event)
		event_log.append(event)
	if not is_event_running:
		on_no_event_running.emit()
func _resume_event():
	_current_event_id = _event_stack.pop_back()
	running_animations	=_animation_stack.pop_back()
	for animation in running_animations:
		if animation.is_valid():
			animation.play()
func get_tween()->Tween:
	var tween = get_tree().create_tween()
	running_animations.append(tween)
	return tween
func tween_settled():
	for tween in running_animations:
		if is_instance_valid(tween) and tween.is_running():
			await tween.finished
	return
func _print(string:String):
	print("%d:%d - %s"%[Time.get_ticks_msec(),multiplayer.get_unique_id(),string])
func _get_is_event_running()->bool:
	return _current_event_id!=-1

