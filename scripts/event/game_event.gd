class_name GameEvent
extends Resource
var type			:Type
var payload			:Dictionary
var event_tree_depth:int= 0
var children		:Array[GameEvent]=[]
var sentence		:String
var event_id		:int
enum Type{
	Attack,
	Move,
	ConsideredAttack,
	OpportunityAttack,
	InitiativeRoll,
	DiceRoll,
	SecondWind,
	ArcaneRecovery,
	SpellCasted,
	ConsideredSpellEffect,
	TurnEnd,
	TurnStart,
	Damage,
	Heal,
	ConditionSavingThrow,
}
const type_names = [
	"Attack",
	"Move",
	"ConsideredAttack",
	"OpportunityAttack",
	"InitiativeRoll",
	"DiceRoll",
	"SecondWind",
	"SpellCasted",
	"ConsideredSpellEffect",
	"TurnEnd",
	"TurnStart",
]

func to_dict()->Dictionary:
	return {"type":type,
	"payload":payload,
	"event_tree_depth":event_tree_depth,
	"children":children.map(func(e):return e.to_dict()),
	"sentence":sentence}
func from_dict(dict:Dictionary)->GameEvent:
	type 				= dict["type"]
	payload				= dict["payload"]
	event_tree_depth 	= dict["event_tree_depth"]
	children 			= []
	for child in dict["children"]:
		children.append(GameEvent.new().from_dict(child))
	sentence 			= dict["sentence"]
	return self
func print_event():
	print("%s - %d"%[type_names[type],event_id])
	print(sentence)
	print(payload)
	for child in children:
		print_event()
		
func get_class_save_id()->String:
	return "GameEvent"




func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
	"type":type,
	"payload":saver.to_save(payload),
	"event_tree_depth":event_tree_depth,
	"children":saver.to_save(children),
	"sentence":sentence,
	"event_id":event_id
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	type  = save["type"]
	payload  = saver.from_save(save["payload"])
	event_tree_depth = save["event_tree_depth"]

	var new_children :Array[GameEvent] = []
	new_children.append_array(saver.from_save(save["children"]))
	children = new_children
	sentence = save["sentence"]
	event_id = save["event_id"]

