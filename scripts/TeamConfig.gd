class_name TeamConfig
extends Node
@export var team_name			:String
@export var team_allies			:Array[String]
@export var team_hostiles		:Array[String]
@export var team_size			:int
@export var members				:Array[Creature]
@rpc("call_local","reliable","any_peer")
func add_member(creature_id_data:String):
	var creature_id = SI.p(creature_id_data)
	print("%s adding creature by id %s"%[multiplayer.get_unique_id(),creature_id.id])
	var creature = BaseResourceService.get_resource(creature_id)
	members.append(creature)


func get_class_save_id()->String:
	return "TeamConfig"

func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"team_name":team_name,
		"team_allies":team_allies,
		"team_hostiles":team_hostiles,
		"team_size":team_size,
		"members":saver.to_save(members)
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
		team_name = save["team_name"]
		var new_team_allies :Array[String] = []
		new_team_allies.append_array(save["team_allies"])
		team_allies = new_team_allies

		var new_team_hostiles :Array[String] = []
		new_team_hostiles.append_array(save["team_hostiles"])
		team_hostiles = new_team_hostiles
		team_size = save["team_size"]
		var new_members :Array[Creature] = []
		new_members.append_array(save["members"]["data"].map(func(d):return saver.from_save(d)))
		members = new_members
