class_name Armor
extends Equipment

@export var raw_ac						:int
@export var ac_modifier				:CreatureBattleStatistics.Type
@export var modifier_cap				:int=10000000
