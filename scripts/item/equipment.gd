class_name Equipment
extends GameItem

@export var creature_conditions:Array[CreatureConditionDesign]

var wearer:Creature
var conditions= {}
func equip(creature:Creature):
	if MultiplayerService.is_server():
		conditions[creature] = []
		for condition in creature_conditions:
			var condition_id = MultiplayerService.generate_unique_id()
			conditions[creature].append(condition_id)
			creature.attach_new_condition.rpc(condition.resource_path,SI.s(condition_id))
	wearer=creature
func unequip(creature:Creature):
	if MultiplayerService.is_server():
		for condition in conditions[creature]:
			creature.detach_condition.rpc(SI.s(condition))
	wearer=null
