class_name Weapon
extends Equipment

@export var attack_range		:float
@export var attack_animation	:SpriteFrames
@export var is_finesse			:bool
@export var is_two_handed		:bool=false
@export var damage_sources		:Array[DamageSource]

func configure_attack_roll(attack_roll:AttackRoll):
	var modifier=get_modifier()
	attack_roll.add_modifier(modifier)
	attack_roll.add_modifier(wearer.statistics.get_proficiency())

func equip(creature:Creature):
	super.equip(creature)
	creature.attack_range = int(attack_range)
func get_damages(target):
	if not InterfaceCheckTool.is_hittable(target) and InterfaceCheckTool.is_damageable(target):
		return []
	var damages = []
	for source in damage_sources:
		damages.append(source.get_damages(wearer,target,self))
	return damages
func get_modifier()->int:
	if is_finesse:
		return wearer.statistics.get_dexterity_modifier()
	else:
		return wearer.statistics.get_strength_modifier()
		
