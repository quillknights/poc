class_name GameItem
extends Resource
@export var unique_id:String:set=_set_unique_id
@export var inventory_icon:Texture2D
var icon:
	get:
		return inventory_icon
@export var name:String
@export_multiline var description:String

func enter(_inventory):
	pass
func leave(_inventory):
	pass
func _set_unique_id(value:String):
	unique_id = value
	unique_id = resource_path
