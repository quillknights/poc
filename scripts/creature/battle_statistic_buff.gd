class_name BattleStatisticBuff
extends Resource
# Change the species ability buff system to use a specific resource each time
@export var types 		:Array[CreatureBattleStatistics.Type]
@export var value_buff	:int=0
@export var mod_buff	:int=0
var creature:Creature

func apply_value(type:CreatureBattleStatistics.Type,value)->int:
	if value_buff!=0 and type in types:
		return value+value_buff
	return value
func apply_modifier(type:CreatureBattleStatistics.Type,mod:int)->int:
	if value_buff!=0 and type in types:
		return mod+mod_buff
	return mod
