class_name BattleResource
extends Resource

@export var every_turn:bool
@export var every_short_rest:bool
@export var every_long_rest:bool
@export var icon:Texture2D
@export var spent_icon:Texture2D
@export var name:String
