@tool
class_name SpellLevelIcon
extends TextureRect

var SPELL_ICON =preload("res://assets/UI/GameResource/SpellSlotActive.png")
var CANTRIP_ICON= preload("res://assets/UI/GameResource/CantripIcon.png")

@export var level:int:set=_set_level
@onready var level_label = $LevelLabel

func refresh():
	refresh_icon()
	refresh_level()
func refresh_icon():
	if level == 0:
		self.texture = CANTRIP_ICON
	else:
		self.texture = SPELL_ICON

func refresh_level():
	if level == 0:
		level_label.hide()
		return
	level_label.show()
	level_label.text = TextTools.to_roman(level)
	
func _set_level(value:int):
	level = value
	if not is_node_ready():
		await ready
	refresh()
