class_name BattleStatisticsConditionalBuff
extends BattleStatisticBuff

@export var predicates:Array[CreaturePredicate]

func is_applying()->bool:
	for predicate in predicates:
		if not predicate.get_value(creature):
			return false
	return true

func apply_value(type:CreatureBattleStatistics.Type,value)->int:
	if not is_applying():
		return value
	if value_buff!=0 and type in types:
		return value+value_buff
	return value
func apply_modifier(type:CreatureBattleStatistics.Type,mod:int)->int:
	if not is_applying():
		return mod
	if value_buff!=0 and type in types:
		return mod+mod_buff
	return mod
