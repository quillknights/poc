extends Node2D
class_name CreatureMini

const CONDITION_OFFSET = 40
const CREATURE 		=preload("res://scenes/creature.tscn")
const HIGHLIGHT_MATERIAL = preload("res://shaders/creature_mini_sprite_hovered.tres")
const HIGHLIGHTABLE_MATERIAL = preload("res://shaders/creature_mini_sprite_outlined.tres")
@export var creature:Creature
@export var terrain_container:Node2D
@export var is_movable	:bool = true
@onready var sprite = $Sprite
@onready var weapon = $Weapon
@onready var animation_player = $AnimationPlayer
@onready var condition_row = $ConditionRow
@onready var mouvement:TerrainMouvement = $Mouvement
@onready var collision_area = $CollisionArea
@onready var fov:FieldOfView	=$FOV
@onready var death_icon = $DeathIcon
@onready var occlusion_shape = $OcclusionShape

var proxy_for:
	get:
		return creature
var unique_id:
	get:
		return "CreatureMini:%s"%[creature.unique_id]
var hex_coord	:
	get:
		return creature.hex_coord
signal on_click(CreatureMini)
signal on_mouse_hovered(CreatureMini)
signal on_mouse_exited(CreatureMini)
signal on_path_step(CreatureMini)
signal on_path_done(CreatureMini)
signal on_push_done(CreatureMini)
signal on_colide_object(CreatureMini,Variant)
signal on_collide_terrain(CreatureMini,Terrain)

var is_highlightable	:bool = false

func _ready():
	BaseResourceService.register_resource(self)
	refresh_texture()
	refresh_conditions()
	refresh_weapon_animation()
	
	collision_area.area_entered.connect(handle_area_entered)
	collision_area.body_entered.connect(handle_body_entered)
	creature.on_conditions_changed.connect(handle_conditions_changed)
	creature.on_weapon_changed.connect(handle_weapon_changed)
	creature.on_death.connect(handle_death)
func refresh_texture():
	sprite.texture = creature.icon
func refresh_weapon_animation():
	weapon.sprite_frames = creature.weapon.attack_animation
func refresh_conditions():
	for child in condition_row.get_children():
		condition_row.remove_child(child)
		child.queue_free()
	for condition in creature.conditions.get_children():
		if condition.design.mini_icon:
			var condition_sprite = Sprite2D.new()
			condition_sprite .texture = condition.design.mini_icon
			condition_sprite.position.x = CONDITION_OFFSET*condition_row.get_child_count()
			condition_row.add_child(condition_sprite)
func take_damage(_amount:int):
	if creature.is_dead:
		animation_player.play("die")
	else:
		animation_player.play("take_damage")

func handle_conditions_changed(_creature:Creature):
	refresh_conditions()
#================VISUAL Shenanigans==============

func highlight(value:bool):
	if is_highlightable:
		if value:
			sprite.material = HIGHLIGHT_MATERIAL
		else:
			sprite.material = HIGHLIGHTABLE_MATERIAL
func highlightable(value:bool):
	is_highlightable = value 
	if is_highlightable:
		sprite.material = HIGHLIGHTABLE_MATERIAL
	else:
		sprite.material = null

func animate_attack(target):
	var pixel_position = HexCoordTool.hex_to_pixel(target.hex_coord)
	look_at(ResourceService.get_battle().terrain.to_global(pixel_position))
	weapon.visible=true
	weapon.play("successfullAttack")
	await weapon.animation_finished
	weapon.visible=false
	
func animate_failed_attack(target_hex_coord):
	var pixel_position = ResourceService.get_battle().terrain.to_global(HexCoordTool.hex_to_pixel(target_hex_coord))
	look_at(pixel_position)
	weapon.visible=true
	weapon.play("failedAttack")
	await weapon.animation_finished
	weapon.visible=false


func handle_weapon_changed(_creature:Creature):
	refresh_weapon_animation()
func _is_selectable()->bool:
	return on_click.get_connections().size()>0
func handle_body_entered(other):
	if mouvement.is_moving and other is Terrain:
		on_collide_terrain.emit(self,other)
func handle_area_entered(other:Area2D):
	var parent = other.get_parent()
	if mouvement.is_moving and parent is CreatureMini or parent is TerrainObject:
		on_colide_object.emit(self,parent)
func handle_death(_c:Creature):
	sprite.modulate = Color.from_string("#cfcfcf80",Color.WHITE)
	death_icon.show()
	collision_area.hide()
	collision_area.set_deferred("monitorable",false)
	collision_area.set_deferred("monitoring", false)
	occlusion_shape.hide()
func get_class_save_id()->String:
	return "CreatureMini"
func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
	"creature":saver.to_save(creature)
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	creature  = saver.from_save(save["creature"])
