class_name CreatureBattleStatistics
extends Node
enum Type{STR,DEX,CON,INT,WIS,CHA,AC,PROFICIENCY,NONE}
static var BASIC_CREATURE_STATS = [
	Type.STR,Type.DEX,Type.CON,Type.INT,Type.WIS,Type.CHA
]

@export var _base_strength		:int=8
@export var _base_dexterity		:int=8
@export var _base_constitution	:int=8
@export var _base_inteligence	:int=8
@export var _base_wisdom		:int=8
@export var _base_charisma		:int=8
@export var buffs				:Array[BattleStatisticBuff] = []

var creature:Creature
func update_base_value(type:Type,by:int):
	match(type):
		Type.STR:
			_base_strength +=by
		Type.DEX:
			_base_dexterity +=by
		Type.CON:
			_base_constitution +=by
		Type.INT:
			_base_inteligence +=by
		Type.WIS:
			_base_wisdom +=by
		Type.CHA:
			_base_charisma +=by
func add_buff(buff:BattleStatisticBuff):
	buff.creature = creature
	buffs.append(buff)
func remove_buff(buff:BattleStatisticBuff):
	buff.creature = null
	buffs.erase(buff)
func get_base_value(type:Type)->int:
	match type:
		Type.STR:
			return _base_strength
		Type.DEX:
			return _base_dexterity
		Type.CON:
			return _base_constitution
		Type.INT:
			return _base_inteligence
		Type.WIS:
			return _base_wisdom
		Type.CHA:
			return _base_charisma
		Type.AC:
			return _compute_base_ac()
		Type.PROFICIENCY:
			return _compute_proficiency()
	return -1
	
func _compute_base_ac()->int:
	if creature.armor==null:
		return 10+get_modifier(Type.DEX)
	return creature.armor.raw_ac+min(get_modifier(creature.armor.ac_modifier),creature.armor.modifier_cap)
	
func _compute_proficiency()->int:
	var level = creature.get_overall_level()
	return CreatureBattleStatistics.proficiency_for_level(level)
static func proficiency_for_level(level:int):
	return int(2+float((level-1))/4)
	
func _value_to_mod(value:int)->int:
	return floor((float(value)-10)/2)
	
func get_value(type:Type)->int:
	var value = get_base_value(type)
	for buff in buffs:
		value = buff.apply_value(type,value)
	return value
func get_modifier(type:Type)->int:
	var value = get_value(type)
	var mod = _value_to_mod(value)
	for buff in buffs:
		mod = buff.apply_modifier(type,mod)
	return mod
func get_strength_modifier() -> int:
	return get_modifier(Type.STR)
func get_dexterity_modifier() -> int:
	return get_modifier(Type.DEX)
func get_proficiency()->int:
	return get_value(Type.PROFICIENCY)
func get_ac()->DifficultyClass:
	var dc = DifficultyClass.new()
	dc.base_value = get_value(Type.AC)
	return dc
static func get_ability_short_name(ability:Type):
	match(ability):
		Type.STR:
			return "STR"
		Type.DEX:
			return "DEX"
		Type.CON:
			return "CON"
		Type.INT:
			return "INT"
		Type.WIS:
			return "WIS"
		Type.CHA:
			return "CHA"
		
func get_class_save_id()->String:
	return "CreatureBattleStatistics"

func to_save(_saver:GameElementSaver)->Dictionary:
	var data = {
		"_base_strength":_base_strength,
		"_base_dexterity":_base_dexterity,
		"_base_constitution":_base_constitution,
		"_base_inteligence":_base_inteligence,
		"_base_wisdom":_base_wisdom,
		"_base_charisma":_base_charisma,
		"buffs":_saver.to_save(buffs)
		}
	return data	

func from_save(save:Dictionary,_saver:GameElementSaver):
	_base_strength = save["_base_strength"]
	_base_dexterity = save["_base_dexterity"]
	_base_constitution = save["_base_constitution"]
	_base_inteligence = save["_base_inteligence"]
	_base_wisdom = save["_base_wisdom"]
	_base_charisma = save["_base_charisma"]
	if "buffs" in save:	
		buffs.assign(_saver.from_save(save["buffs"]))
