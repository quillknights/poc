class_name CreatureEquipedCondition
extends CreaturePredicate
@export var weapon_equiped			:bool
@export var other_hand_free			:bool
@export var wears_armor 			:bool
#@export var wears_boots				:bool
#@export var wears_gloves			:bool
#@export var wears_rings				:bool


func get_value(creature:Creature)->bool:
	if weapon_equiped and not creature.weapon:
		return false
	if other_hand_free:
		if creature.second_hand!=null or creature.weapon.is_two_handed:
			return false
	if wears_armor and not creature.armor:
		return false
	return true 
