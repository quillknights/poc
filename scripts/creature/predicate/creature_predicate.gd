class_name CreaturePredicate
extends Resource

func get_value(_creature:Creature) -> bool:
	push_error("A child should have overiden this function")
	return false
