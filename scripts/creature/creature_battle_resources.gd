class_name CreatureBattleResources
extends Node

signal on_resource_change(CreatureBattleResource)
static var ACTION = preload("res://resources/battle_resources/action.tres")
static var REACTION =  preload("res://resources/battle_resources/reaction.tres")
static var MOUVEMENT = preload("res://resources/battle_resources/mouvement.tres")
static var BONUS_ACTION = preload("res://resources/battle_resources/bonus_action.tres")

static var SPELL_SLOTS = [
	preload("res://resources/battle_resources/spell_slot_lvl1.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl2.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl3.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl4.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl5.tres"), 
	preload("res://resources/battle_resources/spell_slot_lvl6.tres")
]

@export var start_actions			:int:set = _set_actions
@export var start_reactions			:int:set = _set_reactions
@export var start_mouvements		:float:set= _set_mouvements
@export var start_bonus_action 		:int:set= _set_bonus_action

var action			:get=_get_action
var reaction		:get=_get_reaction
var mouvement		:get=_get_mouvement
var bonus_action	:get=_get_bonus_action

var current_battle_resources = {ACTION:start_actions,
								REACTION:start_reactions,
 								MOUVEMENT:start_mouvements,
								BONUS_ACTION:start_bonus_action
								}
var default_battle_resources = {ACTION:start_actions,
								REACTION:start_reactions,
 								MOUVEMENT:start_mouvements,
								BONUS_ACTION:start_bonus_action
								}
								
func new_resource(resource:BattleResource,default_value:int):
	default_battle_resources[resource] = default_value
	current_battle_resources[resource] = default_value
func spend_resource(resource:BattleResource,):
	current_battle_resources[resource]-=1
	on_resource_change.emit(self)
func resplenish_turn_resources():
	for r in get_resources():
		if r.every_turn:
			resplenish_resource(r)
	
func resplenish_resource(resource:BattleResource):
	current_battle_resources[resource] = default_battle_resources[resource]
	on_resource_change.emit(self)

func get_resource(resource:BattleResource):
	return current_battle_resources[resource]
func get_default_value(resource:BattleResource):
	return default_battle_resources[resource]
func get_spent_resource(resource:BattleResource)->int:
	return get_default_value(resource)-get_resource(resource)
func get_resources()->Array:
	return current_battle_resources.keys()
func _set_actions(value:int):
	start_actions=value
	default_battle_resources[ACTION] = value
	current_battle_resources[ACTION] = value

func _set_mouvements(value:float):
	start_mouvements = value
	default_battle_resources[MOUVEMENT] = start_mouvements
	current_battle_resources[MOUVEMENT] = start_mouvements
func _set_reactions(value:int):
	start_reactions = value
	default_battle_resources[REACTION] = start_reactions
	current_battle_resources[REACTION] = start_reactions
func _set_bonus_action(value:int):
	start_bonus_action=value
	default_battle_resources[BONUS_ACTION] = start_bonus_action
	current_battle_resources[BONUS_ACTION] = start_bonus_action
	
func _get_action():
	return get_resource(ACTION)
func _get_reaction():
	return get_resource(REACTION)
func _get_mouvement():
	return get_resource(MOUVEMENT)
func _get_bonus_action():
	return get_resource(BONUS_ACTION)
func increase_resource(resource:BattleResource,value:int=1):
	set_resource_value(resource,get_resource(resource)+value)
func update_resource(resource:BattleResource,f):
	current_battle_resources[resource] = f.call(get_resource(resource))
func set_resource_value(resource:BattleResource,value:int):
	current_battle_resources[resource] = value
func has_slot_level(level:int)->bool:
	var slot = get_spell_slot(level)
	return slot in default_battle_resources
func has_unspent_slot_level(level:int)->bool:
	for r in current_battle_resources.keys():
		if r is SpellSlot:
			if r.level==level:
				return get_resource(r)>0	
	return false
func get_spell_slot(level:int):
	return SPELL_SLOTS[level-1]
func get_class_save_id()->String:
	return "CreatureBattleResources"

func to_save(_saver:GameElementSaver)->Dictionary:
	var data = {
		"start_actions":start_actions,
		"start_reactions":start_reactions,
		"start_mouvements":start_mouvements,
		"start_bonus_action":start_bonus_action,
		"current_battle_resources":_saver.to_save(current_battle_resources),
		"default_battle_resources":_saver.to_save(default_battle_resources),
	}
	return data


func from_save(save:Dictionary,_saver:GameElementSaver):
	start_actions = save["start_actions"]
	start_reactions = save["start_reactions"]
	start_mouvements = save["start_mouvements"]
	start_bonus_action = save["start_bonus_action"]
	if "current_battle_resources" in save:
		current_battle_resources = _saver.from_save(save["current_battle_resources"])
	if "default_battle_resources" in save:
		default_battle_resources = _saver.from_save(save["default_battle_resources"])
