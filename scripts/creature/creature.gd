extends Node2D
class_name Creature


var unique_id		:GameUniqueId
@export var icon			:Texture2D
@export var game_name		:String

@export var basic_actions	:Array[CreatureActionDesign]			:set=_set_basic_actions


@export var max_health			:int=0
@export var health				:int=0
@export var hex_coord			:Vector2= Vector2.INF
@export var attack_range		:int
@export var weapon				:Weapon
@export var second_hand			:Weapon
@export var armor				:Armor
@export var inventory			:Array[GameItem]
@export var range_of_sight		:float=1
@export var is_ai				:bool = false :set=_set_is_ai
@warning_ignore("shadowed_global_identifier")
@export var creature_mini				:CreatureMini	:set=_set_mini
@export_group("Statistics")
@export var statistics			:CreatureBattleStatistics					:set=_set_statistics
#region statistics setters
@export var STR					:int:
	set(v):
		statistics._base_strength=v
@export var DEX					:int:
	set(v):
		statistics._base_dexterity=v
@export var CON					:int:
	set(v):
		statistics._base_constitution=v
@export var INT					:int:
	set(v):
		statistics._base_inteligence=v
@export var WIS					:int:
	set(v):
		statistics._base_wisdom=v
@export var CHA					:int:
	set(v):
		statistics._base_charisma=v
#endregion
@export_group("Battle Resources")
@export var battle_resources	:CreatureBattleResources	
#region Starting Rersources Setters

@export var start_actions			:int:
	set(v):
		battle_resources.start_actions=v
@export var start_reactions			:int:
	set(v):
		battle_resources.start_reactions=v
@export var start_mouvements		:float:
	set(v):
		battle_resources.start_mouvements=v
@export var start_bonus_action 		:int:
	set(v):
		battle_resources.start_bonus_action=v
#endregion
signal on_health_change(Creature)
signal on_damage_taken(Creature,Damage)
signal on_death(Creature)
signal on_rolling(Creature,DiceRoll)
signal on_turn_start(Creature)
signal on_turn_resume(Creature)
signal on_turn_end(Creature)
signal on_moved(Creature)
signal on_pushed_back(Creature)
signal on_conditions_changed(Creature)
signal on_new_condition(CreatureCondition,Creature)
signal on_weapon_changed(Creature)
signal on_mini_change(Creature)
signal on_actions_change(Creature)
signal on_concentrating(Creature)

var is_dead =false
var rng = RandomNumberGenerator.new()
var is_moving = false
var concentration:Concentration
var fov:FieldOfView
var is_acting:bool = false
var available_actions: Array[CreatureActionDesign] = []
@export_group("sub nodes")
@export var conditions:Node
@export var species:Species
@export var game_classes:Node
@onready var actions = $Actions
@onready var ai:CreatureAI = $AI


func _ready():
	if not unique_id:
		unique_id = MultiplayerService.generate_unique_id()
	BaseResourceService.register_resource(self)	
	if is_ai:
		ai.is_active = true
		
func start_turn():
	battle_resources.resplenish_turn_resources()
	on_turn_start.emit(self)
func resume_turn():
	on_turn_resume.emit(self)
func end_turn():
	on_turn_end.emit(self)
func get_attack_roll(target)->AttackRoll:
	var roll = AttackRoll.new()
	roll.target = target
	roll.dice= preload("res://resources/D20.tres")
	weapon.configure_attack_roll(roll)
	apply_conditions(roll)
	roll.on_rolling.connect(handle_dice_rolling)
	return roll
func get_spell_attack_roll(target,spell:Spell)->SpellAttackRoll:
	var roll = SpellAttackRoll.new()
	roll.target = target
	roll.dice = preload("res://resources/D20.tres")
	roll.spell = spell
	var spell_casting_ability=spell.get_spell_casting_ability(self)
	var spell_casting_modifier = statistics.get_modifier(spell_casting_ability)
	roll.add_modifier(spell_casting_modifier)
	apply_conditions(roll)
	roll.on_rolling.connect(handle_dice_rolling)
	return roll
func get_spell_damage_roll(target,dice:Dice,_cast_spell:CastSpell)->DiceRoll:
	var roll = DamageRoll.new()
	roll.on_rolling.connect(handle_dice_rolling)
	roll.target = target
	roll.dice=dice
	apply_conditions(roll)
	return roll
	
func get_damage_roll(dice:Dice,target)->DiceRoll:
	var roll = DamageRoll.new()
	roll.weapon=weapon
	roll.dice = dice
	roll.on_rolling.connect(handle_dice_rolling)
	roll.target = target
	apply_conditions(roll)
	return roll
func get_initiative_roll()->DiceRoll:
	var roll = DiceRoll.new()
	roll.dice=preload("res://resources/D20.tres")
	roll.add_modifier(statistics.get_dexterity_modifier())
	apply_conditions(roll)
	roll.on_rolling.connect(handle_dice_rolling)
	return roll
func get_dice_roll(dice:Dice)->DiceRoll:
	var roll = DiceRoll.new()
	roll.dice = dice
	roll.on_rolling.connect(handle_dice_rolling)
	return roll
func get_saving_throw(characteristic:CreatureBattleStatistics.Type,against = null) -> SavingThrow:
	var roll = SavingThrow.new()
	roll.dice = preload("res://resources/D20.tres")
	roll.on_rolling.connect(handle_dice_rolling)
	roll.characteristic = characteristic
	roll.add_modifier(statistics.get_modifier(characteristic))
	roll.against=against
	apply_conditions(roll)
	return roll
func get_spell_difficulty_saving_class(spell:Spell)->DifficultyClass:
	var dc = DifficultyClass.new()
	dc.base_value = 8
	dc.add_modifier(statistics.get_modifier(spell.get_spell_casting_ability(self)))
	dc.add_modifier(statistics.get_proficiency())
	apply_conditions(dc)
	return dc
@rpc("authority","call_local","reliable")
func attach_new_condition(design_id:String,instance_id_save:String):
	var condition_instance_id = SI.p(instance_id_save)
	var design = BaseResourceService.get_resource(design_id)
	var condition = design.condition_script.new()
	condition.design = design
	condition.instance_id = condition_instance_id
	BaseResourceService.register_resource(condition,condition_instance_id)	
	attach_condition(condition)
	on_new_condition.emit(condition,self)
	
func attach_condition(condition:CreatureCondition):
	condition.attach(self)	
	if not condition in conditions.get_children():
		conditions.add_child(condition)	
	on_conditions_changed.emit(self)	
func has_condition_with_id(game_id:GameUniqueId)->bool:
	for condition in conditions.get_children():
		if condition.instance_id == game_id:
			return true
	return false
@rpc("authority","call_local","reliable")
func detach_condition(condition_unique_id_save:String):
	var id = SI.p(condition_unique_id_save)
	var condition = BaseResourceService.get_resource(id)
	condition.detach(self)
	conditions.remove_child(condition)
	on_conditions_changed.emit(self)
	condition.queue_free()
	BaseResourceService.unregister_resource(id)
func add_level(level:GameClassLevel):
	var game_class = get_game_class(level.design.game_class)
	if not game_class:
		game_class = level.design.game_class.get_game_class()
		add_class(game_class)
	game_class.add_level(level)
func add_class(game_class:GameClass):
	game_class.creature = self
	game_class.attach()
	game_classes.add_child(game_class)
func has_game_class_level(class_level_design:GameClassLevelDesign):
	var game_class = get_game_class(class_level_design.game_class)
	if game_class == null:
		return false
	for level in game_class.get_levels():
		if level.design == class_level_design:
			return true 
	return false
func get_game_class(game_class_design:GameClassDesign)->GameClass:
	for game_class in game_classes.get_children():
		if game_class.design == game_class_design:
			return game_class
	return null

func set_species(new_species:Species):
	if species:
		remove_child(species)
		species.queue_free()
	species = new_species
	species.attach(self)	
	add_child(species)
	
	
func add_action(config:CreatureActionDesign) -> CreatureAction:
	var action_node = config.action.new()
	action_node.design = config
	action_node.actor = self
	action_node.name = config.name
	self.actions.add_child(action_node)
	available_actions.append(config)
	on_actions_change.emit(self)
	action_node.on_acting.connect(func(_a):self.is_acting=true)
	action_node.on_acted.connect(func(_a):self.is_acting=false)
	return action_node
func add_item(item:GameItem):
	inventory.append(item)
func equip_weapon(new_weapon:Weapon):
	if weapon != new_weapon:
		weapon = new_weapon
		weapon.equip(self)		
		on_weapon_changed.emit(self)
func unequip_weapon(old_weapon:Weapon):
	if weapon==old_weapon:
		weapon.unequip(self)
		weapon=null
		on_weapon_changed.emit(self)
func apply_conditions(condition_target):
	for condition in conditions.get_children():
		condition_target.accept_condition(condition)
@rpc("any_peer","call_local","reliable")
func attack(target_id_data:String,_roll_result:int,_ac:int):
	var target_id = SI.p(target_id_data)
	var target = BaseResourceService.get_resource(target_id)
	self.creature_mini.animate_attack(target)
@rpc('any_peer',"call_local","reliable")
func fail_attack(target_hex_coord:Vector2):
	self.creature_mini.animate_failed_attack(target_hex_coord)
@rpc("any_peer","call_local","reliable")
func heal(amount:int):
	health = min(max_health,health+amount)
	on_health_change.emit(self)
@rpc("authority","reliable","call_local")
func resplenish_spell_slot(level:int,by:int):
	if battle_resources.has_slot_level(level):
		battle_resources.increase_resource(battle_resources.get_spell_slot(level),by)
func get_damage_taken(amount:int,type:DamageType):
	var damage = Damage.new()
	damage.base_value = amount
	damage.type = type
	damage.target = self
	apply_conditions(damage)
	return damage
	
func take_damage(damage:Damage):
	if not multiplayer.is_server:
		return
	_take_damage.rpc(SI.s(damage))
	check_concentration(damage)
@rpc("any_peer","call_local","reliable")
func _take_damage(damage_data:String):
	var damage:Damage = SI.p(damage_data)
	health=max(0,health-damage.value)
	on_damage_taken.emit(self,damage)
	on_health_change.emit(self)
	if health==0:
		is_dead = true
		on_death.emit(self)
		CreaturesEventBus.on_creature_died.emit(self)
		
func concentrate(spell_cast:CastSpell):
	if concentration !=null:
		concentration.end()
	concentration = Concentration.new()
	concentration.spell = spell_cast.design
	spell_cast.configure_concentration(concentration)
	_concentrate.rpc(spell_cast.design.resource_path)
	on_concentrating.emit(self)
@rpc("authority","call_remote","reliable")
func _concentrate(spell_id:String):
	var spell = BaseResourceService.get_resource(spell_id)
	concentration = Concentration.new()
	concentration.spell = spell
	on_concentrating.emit(self)
@rpc("any_peer","call_local","reliable")
func end_concentration():
	concentration.end()
	concentration = null
	on_concentrating.emit(self)
	
@rpc("any_peer","call_local","reliable")
func fail_concentration():
	concentration.fail()
	concentration = null
func check_concentration(damage:Damage):
	if not concentration:
		return
	var saving_throw = get_saving_throw(CreatureBattleStatistics.Type.CON)
	var dc = concentration.get_saving_dc(damage)
	saving_throw.roll(dc)
	if not saving_throw.result >=dc.value:
		fail_concentration().rpc()
func can_save()->bool:
	return true
func spend_action():
	battle_resources.spend_resource(battle_resources.ACTION)

@rpc("any_peer","call_local","reliable")
func move_along_path(path:Array):
	is_moving=true
	creature_mini.mouvement.move_along(path)
@rpc("any_peer","call_local","reliable")
func push_back(hex_direction:Vector2,distance:int):
	is_moving=true
	creature_mini.mouvement.push_back(hex_direction,distance)
func has_line_of_sight(to_element:Node2D):
	return fov.has_line_of_sight(to_element)
	
func handle_dice_rolling(roll:DiceRoll):
	roll_dice.rpc(SI.s(roll))
func handle_mini_step(_mouvement:TerrainMouvement):
	battle_resources.spend_resource(battle_resources.MOUVEMENT)
	
	if battle_resources.get_resource(battle_resources.MOUVEMENT)<=0:
		creature_mini.mouvement.stop_mouving()
func handle_mini_path_done(_mouvement:TerrainMouvement):
	is_moving=false
	on_moved.emit(self)
func handle_mini_push_done(_mouvement:TerrainMouvement):
	is_moving=false
	on_pushed_back.emit(self)
#region Setters/Getters


@rpc("any_peer","call_local","reliable")
func update_hex_coord(coord:Vector2):
	self.hex_coord=coord
func get_ac()->DifficultyClass:
	var ac =  statistics.get_ac()
	apply_conditions(ac)
	return ac
func _set_mini(value:CreatureMini):
	if creature_mini:
		creature_mini.mouvement.on_path_step.disconnect(handle_mini_step)
		creature_mini.mouvement.on_path_done.disconnect(handle_mini_path_done)
		creature_mini.mouvement.on_push_done.disconnect(handle_mini_push_done)
		fov = null
	creature_mini = value
	if creature_mini:
		if not creature_mini.is_node_ready():
			await creature_mini.ready
		creature_mini.mouvement.on_path_done.connect(handle_mini_path_done)
		creature_mini.mouvement.on_path_step.connect(handle_mini_step)
		creature_mini.mouvement.on_push_done.connect(handle_mini_push_done)
		fov = creature_mini.fov
	on_mini_change.emit(value)
func _set_basic_actions(values:Array[CreatureActionDesign]):
	basic_actions= values
	if not is_node_ready():
		await ready
	for value in values:
		add_action(value)
		
func _set_statistics(value:CreatureBattleStatistics):
	statistics = value
	value.creature=self

func get_overall_level()-> int:
	return game_classes.get_children().map(func(c):return c.get_level()).reduce(func(a,b):return a+b)

func get_game_classes() -> Array[GameClass]:
	var classes:Array[GameClass] = []
	classes.append_array( game_classes.get_children())
	return classes
#endregion


@rpc("any_peer","call_local","reliable")
func roll_dice(dice_roll_data:String):
	var dice_roll = SI.p(dice_roll_data)
	on_rolling.emit(self,dice_roll)

func _exit_tree():
	BaseResourceService.unregister_resource(self.unique_id)

func _is_acting()->bool:
	return actions.get_children().any(func(a):return a.is_acting)
func _set_is_ai(value:bool):
	is_ai=value
	if not is_node_ready():
		await ready
	ai.is_active=is_ai


func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"unique_id"			:saver.to_save(unique_id),
		"icon"				:saver.to_save(icon),
		"game_name"			:game_name,
		"basic_actions"	:    saver.to_save(basic_actions),
		"statistics"		:saver.to_save(statistics),
		"max_health"		:max_health,
		"health"			:health,
		"hex_coord"			:saver.to_save(hex_coord),
		"attack_range"		:attack_range,
		"weapon"			:saver.to_save(weapon),
		"second_hand"		:saver.to_save(second_hand),
		"armor"				:saver.to_save(armor),
		"inventory"			:saver.to_save(inventory),
		"battle_resources"	:saver.to_save(battle_resources),
		"range_of_sight"	:range_of_sight,
		"is_ai"				:is_ai,
		"conditions"		:saver.to_save(conditions.get_children()),
		"concentration"		:saver.to_save(concentration),
		"species"			:saver.to_save(species),
		"game_classes"		:saver.to_save(game_classes.get_children()),
		}
	
	return data





func get_class_save_id()->String:
	return "Creature"
func from_save(save:Dictionary,saver:GameElementSaver):
		if save["unique_id"] is String:
			unique_id = MultiplayerService.get_custom_unique_id(save["unique_id"])
			if unique_id == null:
				push_error("Failed parsing %s unique id already exists"%[saver["unique_id"]])
				return
		else:
			unique_id = saver.from_save(save["unique_id"])
		icon  = saver.from_save(save["icon"])
		game_name = save["game_name"]
		remove_child(statistics)
		statistics.queue_free()
		statistics  = saver.from_save(save["statistics"])
		add_child(statistics)
		max_health  = save["max_health"]
		health  = save["health"]
		hex_coord = saver.from_save(save["hex_coord"])
		attack_range = save["attack_range"]
		#weapon  = saver.from_save(save["weapon"])
		second_hand  = saver.from_save(save["second_hand"])
		armor  = saver.from_save(save["armor"])
		remove_child(battle_resources)
		battle_resources.queue_free()
		battle_resources  = saver.from_save(save["battle_resources"])
		add_child(battle_resources)
		range_of_sight  = save["range_of_sight"]
		is_ai = save["is_ai"]
	
		# Actions
		var new_basic_actions :Array[CreatureActionDesign]=[]
		new_basic_actions.append_array(saver.from_save(save["basic_actions"]))
		basic_actions = new_basic_actions
		# Inventory
		var new_inventory :Array[GameItem]=[]
		new_inventory.append_array(saver.from_save(save["inventory"]))
		inventory = new_inventory 
		
		# Conditions	
		for child in conditions.get_children():
			conditions.remove_child(child)
			child.queue_free()
		for condition in saver.from_save(save["conditions"]):
			conditions.add_child(condition)
		# Concentration
		concentration = saver.from_save(save["concentration"])
		#Species
		if "species" in save:
			remove_child(species)
			species.queue_free()
			species = saver.from_save(save["species"])
			species.creature = self
			species.name="Species"

			add_child(species)
		#GameClasses
		if "game_classes" in save:
			NodeTools.clear_children(game_classes)
			for game_class in saver.from_save(save["game_classes"]):
				game_class.name = game_class.design.name
				game_class.creature = self
				game_classes.add_child(game_class)
			
		#Handling legacy levels:
		# Class Levels
		if "class_levels" in save:
			var new_class_levels :Array[GameClassLevelDesign] = []
			new_class_levels.append_array(saver.from_save(save["class_levels"]))
			for level in new_class_levels:
				add_level(level.get_game_class_level())
		if not is_node_ready():
			await ready
		for game_class in game_classes.get_children():
				game_class.reattach()
		species.reattach(self)
