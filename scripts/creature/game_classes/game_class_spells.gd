class_name GameClassSpells
extends Resource


@export var game_class		:GameClassDesign
@export var spells			:Array[Spell]


func get_level(level:int) -> Array[Spell]:
	var result:Array[Spell] = []
	for spell in spells:
		if spell.level == level:
			result.append(spell)
	return result
