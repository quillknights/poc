class_name GameClassRegistry
extends Resource
@export var game_classes:Array[GameClassDesign]:set=_set_game_classes
@export var game_class_levels:Array[GameClassLevelDesign]:set=_set_game_class_levels
var class_levels = {}
func _set_game_classes(value):
	game_classes = value
	for game_class in game_classes:
		game_class.registry = self
func _set_game_class_levels(value):
	game_class_levels = value
	class_levels = {}
	for level in game_class_levels:
		if level.game_class not in class_levels:
			var array:Array[GameClassLevelDesign] = []
			class_levels[level.game_class] = array
		class_levels[level.game_class].append(level)
		
func get_levels(class_design)-> Array[GameClassLevelDesign]:
	return class_levels[class_design]
