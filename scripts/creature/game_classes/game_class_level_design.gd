class_name GameClassLevelDesign
extends Resource
@export var game_class		:GameClassDesign
@export var level			:int

@export var actions			:Array[CreatureActionDesign]		:set=_set_actions
@export var conditions		:Array[CreatureConditionDesign]	:set=_set_conditions
@export var masteries 		:Array
@export var resources 		:Array[BattleResource] 		
@export var spell_choices	:Array[CreationOptionDesign]
@export var options			:Array[CreationOptionDesign]
@export var game_class_level_script	:GDScript=preload("res://scripts/creature/game_classes/game_class_level.gd")
var creature:Creature
func configure(class_level:GameClassLevel):
	class_level.actions.append_array(actions)
	class_level.level = level
#region Setters/Getters

func _set_actions(value:Array[CreatureActionDesign]):
	actions = []
	for action in value:
		actions.append(action)
func _set_conditions(value:Array[CreatureConditionDesign]):
	conditions = []
	for condition in value:
		conditions.append(condition)
func get_game_class_level()->GameClassLevel:
	var class_level = game_class_level_script.new()
	class_level.design = self
	configure(class_level)
	return class_level
#endregion
