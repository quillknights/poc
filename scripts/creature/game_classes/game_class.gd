class_name GameClass
extends Node


@export var design 			:GameClassDesign
@export var creature		:Creature
var levels = CreatureAttachableHolder.new()
var starting_equipments	= CreatureAttachableHolder.new()
func _init():
	levels.name = "Levels"
	add_child(levels)
	starting_equipments.name = "Starting Equipments"
	add_child(starting_equipments)
	
func attach():
	levels.attach(creature)
	starting_equipments.attach(creature)
	if creature.game_classes.get_child_count()==0:
		creature.max_health = design.health_dice.maximum + creature.statistics.get_modifier(CreatureBattleStatistics.Type.CON)
		creature.health = creature.max_health
	
	
func reattach():
	levels.reattach(creature)
	starting_equipments.reattach(creature)
		
func add_level(level:GameClassLevel):
	if not level.design.game_class == design:
		return
	if creature:
		level.attach(creature)	
	levels.add_attachable(level)
func set_equipment_choices(choices:Array[CreationChoice]):
	starting_equipments.clear()
	for choice in choices:
		starting_equipments.add_attachable(choice)
func get_levels()->Array[GameClassLevel]:
	var result:Array[GameClassLevel] = []
	result.assign(levels.get_children())
	return result
func get_level()->int:
	return get_levels().map(func(l):return l.level).reduce(func(a,b):return max(a,b))
func is_spell_active(spell:Spell):
	return creature.actions.get_children().any(func(a):return a.design==spell)
func get_class_save_id():
	return "GameClass"
	
	
func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"design":saver.to_save(design),
		"creature":saver.to_save(creature),
		"levels":saver.to_save(levels),
		"starting_equipments":saver.to_save(starting_equipments)
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	design  = saver.from_save(save["design"])
	creature  = saver.from_save(save["creature"])
	
	if save["levels"]["type"] == "Array":
		for level in saver.from_save(save["levels"]):
			levels.add_attachable(level)
	else:
		levels = NodeTools.replace_child(self,levels,saver.from_save(save["levels"]))
	if "starting_equipments" in save:
		starting_equipments = NodeTools.replace_child(self,starting_equipments,saver.from_save(save["starting_equipments"]))
	
