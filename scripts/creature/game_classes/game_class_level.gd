class_name GameClassLevel
extends Node
@export var level			:int
@export var design			:GameClassLevelDesign
@export var creature		:Creature
@export var actions			:Array[CreatureActionDesign]
@export var active_spells	:Array[Spell]
var condition_ids			:Dictionary
var options_choices			:Node = CreatureAttachableHolder.new()
var spell_choices			:Node = CreatureAttachableHolder.new()
func _init():
	options_choices.name="OptionsChoices"
	add_child(options_choices)
	spell_choices.name = "SpellChoices"
	add_child(spell_choices)
	
	
func reattach(new_creature:Creature)->void:
	if not new_creature.is_node_ready():
		await new_creature.ready
	creature=new_creature
	add_actions()
	check_conditions()
	add_active_spells()
	reattach_choices()
func attach(new_creature:Creature) -> void :
	if new_creature.has_game_class_level(design):
		push_error("A creature can only have one of each class_levels")
		return 
	if not new_creature.is_node_ready():
		await new_creature.ready
	
	creature = new_creature
	add_actions()
	if MultiplayerService.is_server():
		add_conditions()
	add_active_spells()
	add_resources()
	attach_choices()
	
func attach_choices():
	for choice in options_choices.get_children():
		choice.attach(creature)
	for choice in spell_choices.get_children():
		choice.attach(creature)
func reattach_choices():
	for choice in options_choices.get_children():
		choice.reattach(creature)
	for choice in spell_choices.get_children():
		choice.reattach(creature)
func add_actions():
	for action in actions:
		creature.add_action(action)
func add_conditions():
	for condition in design.conditions:
		attach_condition(condition)
func check_conditions():
	var missing = []
	for id in condition_ids:
		if not creature.has_condition_with_id(id):
			missing.append(id)
	for id in missing:
		print("%s missing "%[id.id])
		reattach_condition(id)
		condition_ids.erase(id)
func reattach_condition(id:GameUniqueId):
	attach_condition(condition_ids[id])
func attach_condition(condition):
	var condition_id = MultiplayerService.generate_unique_id()
	condition_ids[condition_id]=condition
	creature.attach_new_condition.rpc(condition.resource_path,SI.s(condition_id))
	
func add_active_spells():
	for spell in active_spells:
		creature.add_action(spell)
func add_resources():
	var resource_counts = {}
	for r in design.resources:
		if not r in resource_counts:
			resource_counts[r]=0
		resource_counts[r]+=1
	for r in resource_counts.keys():
		creature.battle_resources.new_resource(r,resource_counts[r])
func is_spell_active(spell:Spell):
	return spell in active_spells
func add_choice(choice:CreationChoice):
	if choice.design in design.spell_choices:
		add_spell_choice(choice)
	else:
		for other in options_choices.get_children():
			if other.design == choice.design:
				options_choices.remove_child(other)
				other.queue_free()
		options_choices.add_child(choice)
func add_spell_choice(choice):
	for other in spell_choices.get_children():
		if other.design == choice.design:
			spell_choices.remove_child(other)
			other.queue_free()
	spell_choices.add_child(choice)
func is_fully_configured() -> bool:
	return spell_choices.get_child_count() == design.spell_choices.size()\
			and options_choices.get_child_count() == design.options.size()
func get_class_save_id():
	return "GameClassLevel"


func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"level":level,
		"design":saver.to_save(design),
		"creature":saver.to_save(creature),
		"actions":saver.to_save(actions),
		"condition_ids":saver.to_save(condition_ids),
		"active_spells":saver.to_save(active_spells),
		"options_choices":saver.to_save(options_choices),
		"spell_choices":saver.to_save(spell_choices),
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	if "level" in save:
		level = save["level"]
	design  = saver.from_save(save["design"])
	creature  = saver.from_save(save["creature"])

	var new_actions :Array[CreatureActionDesign] = []
	new_actions.append_array(saver.from_save(save["actions"])) 
	actions = new_actions

	condition_ids = saver.from_save(save["condition_ids"])
	
	var new_active_spells :Array[Spell] = []
	new_active_spells.append_array(saver.from_save(save["active_spells"])) 
	active_spells = new_active_spells
	
	if "options_choices" in save:
		if save["options_choices"]["type"] == "Array":
			#Leggacy management
			for choice in saver.from_save(save["options_choices"]):
				options_choices.add_attachable(choice)
		else:
			options_choices = NodeTools.replace_child(self,options_choices,saver.from_save(save["options_choices"]))
	if "spell_choices" in save:
		if save["spell_choices"]["type"] == "Array":
			#Leggacy management
			for choice in saver.from_save(save["spell_choices"]):
				spell_choices.add_attachable(choice)
		else:
			spell_choices = NodeTools.replace_child(self,spell_choices,saver.from_save(save["spell_choices"]))
