class_name GameClassDesign
extends Resource

@export var icon							:Texture2D
@export var name							:String
@export_multiline var tagline 				:String
@export var health_dice						: Dice
@export var is_caster						:bool
@export var casting_ability					:CreatureBattleStatistics.Type
@export var advantage_saving_abilities		:Array[CreatureBattleStatistics.Type]
@export var equiment_choices				:Array[CreationOptionDesign]
@export var featured_ability_icon			:Texture2D
@export var game_class_script				:GDScript = preload("res://scripts/creature/game_classes/game_class.gd")

var registry:GameClassRegistry

func get_levels():
	if not registry:
		return []
	return registry.get_levels(self)
func get_game_class()->GameClass:
	var game_class = game_class_script.new()
	game_class.design = self
	game_class.name = name
	return game_class
