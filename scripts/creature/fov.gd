class_name FieldOfView
extends Node2D
@onready var general_area:Area2D= $GeneralArea
@onready var shape = $GeneralArea/Shape
@export var creature_mini				:CreatureMini
@onready var vision_shape = $VisionShape

var in_line_of_sight 	:Array[Node2D]
var need_update			:Array[Node2D]
@export var debug_ray = false
var debug_lines = []

# Called when the node enters the scene tree for the first time.
func _ready():
	if not MultiplayerService.is_server() and not MultiplayerService.is_creature_ally(creature_mini.creature):
		return 
	general_area.area_entered.connect(handle_area_entered)
	general_area.area_exited.connect(handle_area_exited)
	shape.shape.radius = HexCoordTool.TILE_PIXEL_HEIGHT*creature_mini.creature.range_of_sight
	init_visibility()
func _draw():
	for line in debug_lines:
		draw_line(to_local(line[0]),to_local(line[1]),Color.DARK_GREEN)
func init_visibility():
	for other_area in general_area.get_overlapping_areas():
		var parent = other_area.get_parent()
		if InterfaceCheckTool.is_visible_element(parent):
			need_update.append(parent)
func handle_area_entered(other:Area2D):
	if other.get_parent() is Node2D and  InterfaceCheckTool.is_visible_element(other.get_parent()):
		need_update.append(other.get_parent())
func handle_area_exited(other:Area2D):
	var parent = other.get_parent()
	if InterfaceCheckTool.is_visible_element(parent):
		need_update.erase(parent)
func _physics_process(_delta):
	debug_lines.clear()
	for element in need_update:
		update_visibility(element)
	in_line_of_sight = in_line_of_sight.filter(func(c):return is_instance_valid(c))
	if not MultiplayerService.is_creature_ally(creature_mini.creature):
		return 
	update_vision_shape()
func update_vision_shape():
	var walls = []
	for element in need_update:
		if not element.has_node("OcclusionShape"):
			continue
		var occlusion_shape = element.get_node("OcclusionShape")
		if not occlusion_shape or occlusion_shape.get_parent() == creature_mini:
			continue
		var polygon = occlusion_shape.occluder.polygon
		for i in range(len(polygon)):
			var start = occlusion_shape.to_global(polygon[i-1])
			var end = occlusion_shape.to_global(polygon[i])
			walls.append([start,end])
	var vision_range = creature_mini.creature.range_of_sight*HexCoordTool.TILE_PIXEL_HEIGHT
	var sight_circle = ShapeTools.generate_circle_polygon(vision_range,100,Vector2.ZERO)
	for i in range(len(sight_circle)):
		var start = to_global(sight_circle[i-1])
		var end = to_global(sight_circle[i])
		walls.append([start,end])
	var vision_center = creature_mini.global_position
	var visible_polygon = compute_fov(walls,vision_center,10)
	visible_polygon.sort_custom(func(a,b):return vision_center.angle_to_point(a)<vision_center.angle_to_point(b))
	vision_shape.polygon = visible_polygon
	
func compute_fov(walls:Array,center:Vector2,_max_distance:float) -> Array[Vector2]:
	debug_lines = []
	var polygon_points:Array[Vector2] = []
	for wall in walls:
		var start_ray=cast_ray_between(center,wall[0])
		var end_ray = cast_ray_between(center,wall[1])
		if not start_ray:
			polygon_points.append(wall[0])
		else:
			polygon_points.append(start_ray["position"])
		if not end_ray:
			polygon_points.append(wall[1])
		else:
			polygon_points.append(end_ray["position"])
	return polygon_points
func cast_ray(element:Node2D):
	var from = creature_mini.global_position
	var to = element.global_position
	return cast_ray_between(from,to)
func cast_ray_between(from:Vector2,to:Vector2):
	var space_state = get_world_2d().direct_space_state	
	var query = PhysicsRayQueryParameters2D.create(from,to)
	query.collide_with_areas = true
	query.collision_mask = 0b00000000_00000000_00000000_0000001
	var result = space_state.intersect_ray(query)
	if debug_ray:
		var start = from
		var finish = to
		if result:
			finish = result["position"]
		debug_lines.append([start,finish])
		queue_redraw()
	return result
func update_visibility(element:Node2D):
	if element is CreatureMini and MultiplayerService.is_creature_ally(element.creature):
		show_element(element)
		return
	var result = cast_ray(element)
	var sees = true
	if result:
		var parent = result["collider"].get_parent()
		if parent!=element:	
			sees = false
	if sees:
		if not element in in_line_of_sight:
			in_line_of_sight.append(element)
	else:
		if element in in_line_of_sight:
			in_line_of_sight.erase(element)
	TeamService.set_member_sees(creature_mini.creature,element,sees)
	if MultiplayerService.is_spectator() or TeamService.get_creature_team_sees(creature_mini.creature,element):
		show_element(element)
	else:
		hide_element(element)
	
func hide_element(element:Node2D):
	element.set_visibility_layer_bit(0,false)
	element.set_visibility_layer_bit(1,true)

func show_element(element:Node2D):
	element.set_visibility_layer_bit(1,false)
	element.set_visibility_layer_bit(0,true)
	
func has_line_of_sight(to_element:Node2D) -> bool:
	var result  = cast_ray(to_element)
	if not result:
		return true
	if result["collider"].get_parent() == to_element:
		return true
	return false
func get_creature_in_los()->Array[Creature]:
	var creatures:Array[Creature] = []
	for element in in_line_of_sight:
		if not is_instance_valid(element):
			continue
		if element is CreatureMini:
			creatures.append(element.creature)
	return creatures
