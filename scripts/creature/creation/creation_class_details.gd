@tool
extends Control

var CLASS_LEVEL_SCENE = preload("res://scenes/creature/creation/class_level_detail_entry.tscn")

@onready var portrait = $Header/Portrait
@onready var name_label = $Header/VBoxContainer/NameLabel
@onready var tagline = $Header/VBoxContainer/Tagline
@onready var health_dice = $GeneralCharacteristics/SubGrid/HealthDice
@onready var advantage_ability_1_label = $GeneralCharacteristics/SubGridAttributes/AdvantageAbilityOne/AdvantageAbility1Label
@onready var advantage_ability_2_label = $GeneralCharacteristics/SubGridAttributes/AdvantageAbilityOne2/AdvantageAbility2Label
@onready var casting_ability_label = $GeneralCharacteristics/CastingAbilitySubGrid/CastingAbility/CastingAbilityLabel

@onready var casting_ability_line_label = $GeneralCharacteristics/CastingAbilityLineLabel
@onready var casting_ability_sub_grid = $GeneralCharacteristics/CastingAbilitySubGrid
@onready var levels = $Levels

@export var class_design:GameClassDesign:set=_set_class_design
@export var class_levels_design:Array[GameClassLevelDesign]:set=_set_class_levels_design
@export var NO_CASTING_ABILITY_TEXT	:String
@export var CASTING_ABILITY_LABEL_TEXT	:String

var _level_values:Dictionary={}
var _game_class:GameClass

signal on_class_update(GameClass)

func refresh():
	refresh_header()
	refresh_general_characteristics()
	refresh_levels()

func refresh_header():
	portrait.texture = class_design.icon
	name_label.text = class_design.name
	tagline.text = class_design.tagline
	
func refresh_general_characteristics():
	health_dice.class_design = class_design
	advantage_ability_1_label.text = CreatureBattleStatistics.get_ability_short_name(class_design.advantage_saving_abilities[0])
	advantage_ability_2_label.text = CreatureBattleStatistics.get_ability_short_name(class_design.advantage_saving_abilities[1])
	if class_design.casting_ability!=CreatureBattleStatistics.Type.NONE:
		casting_ability_label.text = CreatureBattleStatistics.get_ability_short_name(class_design.casting_ability)
		casting_ability_line_label.modulate = Color.WHITE
		casting_ability_sub_grid.modulate = Color.WHITE
		casting_ability_line_label.text = CASTING_ABILITY_LABEL_TEXT
		
	else:
		casting_ability_line_label.text = NO_CASTING_ABILITY_TEXT
		casting_ability_sub_grid.modulate = Color.TRANSPARENT
	
func refresh_levels():
	NodeTools.clear_children(levels)
	for level in class_levels_design:
		var class_level_view = CLASS_LEVEL_SCENE.instantiate()
		class_level_view.class_level = level
		class_level_view.on_level_update.connect(handle_level_update)
		levels.add_child(class_level_view)
func handle_level_update(new_level:GameClassLevel,level_design:GameClassLevelDesign):
	_level_values[level_design]=new_level
	if new_level == null:
		_level_values.erase(level_design)
	update_game_class()
func update_game_class():
	if _game_class:
		_game_class.queue_free()
	_game_class = class_design.get_game_class()
	for design in _level_values:
		_game_class.add_level(_level_values[design])
	if _game_class.get_levels().size()==1:
		on_class_update.emit(_game_class)
func _set_class_design(value:GameClassDesign):
	class_design = value
	if not is_node_ready():
		await ready
	refresh()
	_game_class = class_design.get_game_class()	
	if class_design.registry:
		class_levels_design = class_design.get_levels()
	
func _set_class_levels_design(value:Array[GameClassLevelDesign]):
	class_levels_design = value
	if not is_node_ready():
		await ready
	refresh()
	for design in class_levels_design:
		_level_values[design]= design.get_game_class_level()
