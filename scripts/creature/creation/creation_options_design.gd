class_name CreationOptionDesign
extends Resource

@export var icon			:Texture2D
@export var game_name		:String
@export_multiline var game_description:String
@export var possibilities	:Array[Resource]:get=_get_possibilities
@export var choice_count	:int = 1
@export var display_scene	:PackedScene=preload("res://scenes/creature/creation/creation_option_display/default_options_display.tscn")
@export var creation_option_script	:GDScript = preload("res://scripts/creature/creation/creation_options.gd")
@export var spell_casting_ability	:CreatureBattleStatistics.Type = CreatureBattleStatistics.Type.NONE

func _get_possibilities() -> Array[Resource]:
	return possibilities
	
func get_display()->Control:
	var display = display_scene.instantiate()
	display.option_design = self
	return display
	
func get_creation_option()->CreationChoice:
	var option = creation_option_script.new()
	option.design = self
	return option
