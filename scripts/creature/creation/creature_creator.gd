extends Node2D

@export var species_list:Array[SpeciesDesign]:set=_set_species_list
@export var species_step:CreationSpeciesStep
@export var class_step :CreationClassStep
@export var statistics_step:CreationStatisticsStep
@export var equipement_step:CreationEquipmentStep
@export var name_step 	:CreationNameStep
@export var current_step:Node
@onready var back_button = $HBoxContainer/BackButton
@onready var confirm_button = $HBoxContainer/ConfirmButton
@onready var creature = $Creature
signal on_creature_done(Creature)
var next_step:Node
var picked_species : Species
var picked_class	:GameClass
var picked_statistics:CreatureBattleStatistics
func _ready():
	next_step = current_step.next_step
	species_step.on_species_picked.connect(handle_species_picked)
	class_step.on_class_picked.connect(handle_class_picked)
	statistics_step.on_statistics_valid.connect(handle_statistics_validity_change)
	equipement_step.on_equipment_picked.connect(handle_equipment_choosen)
	name_step.on_name_picked.connect(handle_name_picked)
	confirm_button.pressed.connect(handle_confirm_pressed)

func go_to_next_step():
	current_step.hide()
	current_step = next_step
	if current_step:
		next_step = current_step.next_step
		current_step.show()
		confirm_button.disabled =true
		if "accept_creator" in current_step:
			current_step.accept_creator(self)
	else:
		configure_creature()
	
func configure_creature():
	creature.statistics = picked_statistics
	creature.set_species(picked_species)
	creature.add_class(picked_class)
	creature.game_name = name_step.creature_name
	on_creature_done.emit(creature)
func handle_confirm_pressed():
	go_to_next_step()

func handle_species_picked(species:Species):
	confirm_button.disabled = false
	picked_species =species
	species.creature = creature
func handle_class_picked(game_class:GameClass=null):
	confirm_button.disabled = false
	picked_class = game_class
	picked_class.creature = creature
func handle_statistics_validity_change(value:bool):
	picked_statistics = statistics_step.base_statistics
	confirm_button.disabled = not value
func handle_equipment_choosen(_choosen:Array):
	confirm_button.disabled = false
func handle_name_picked(_creature_name:String):
	confirm_button.disabled = false
func _set_species_list(value:Array[SpeciesDesign]):
	species_list=value
	if not is_node_ready():
		await ready
	species_step.species_list = species_list
