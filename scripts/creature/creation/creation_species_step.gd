class_name CreationSpeciesStep
extends Control

var SPECIES_TAB = preload("res://scenes/creature/creation/species_tab.tscn")
var DETAILS_SCENE = preload("res://scenes/creature/creation/species_detail.tscn")

@export var species_list :Array[SpeciesDesign]:set=_set_species_list
@export var next_step:Node
@onready var margin_container = $MarginContainer/DetailsContainer/MarginContainer
@onready var species_container = $MarginContainer/DetailsContainer/MarginContainer/ScrollContainer/SpeciesContainer
@onready var previous_button = $HBoxContainer/PreviousButton
@onready var next_button = $HBoxContainer/NextButton
signal on_species_picked(Species)
var current_species_index = INF
var details: Control
var species :Species
func _ready():
	next_button.pressed.connect(handle_next_pressed)
	previous_button.pressed.connect(handle_previous_pressed)

func _set_species_list(value:Array):
	species_list = value
	if not is_node_ready():
		await ready
	refresh_species_display()
	
func refresh_species_display():
	for child in species_container.get_children():
		species_container.remove_child(child)
		child.queue_free()
	for species_design in species_list:
		var tab = SPECIES_TAB.instantiate()
		tab.species = species_design
		species_container.add_child(tab)		
		tab.on_click.connect(handle_species_clicked)
func handle_species_clicked(target):
	to_species_detail(target)
func to_species_detail(species_design:SpeciesDesign):
	if species:
		species.queue_free()
	
	species_container.hide()
	if details:
		margin_container.remove_child(details)
		details.queue_free()
	details = DETAILS_SCENE.instantiate()
	details.species_design = species_design
	details.on_species_configured.connect(handle_species_configured)
	margin_container.add_child(details)
	species_container.hide()
	current_species_index = species_list.find(species_design)
	show_next_button()
	show_previous_button()
	species = species_design.get_species()
	on_species_picked.emit(species)
func show_next_button():
	var next_species = species_list[(current_species_index+1)%len(species_list)]
	next_button.show()
	next_button.icon = next_species.icon
func show_previous_button():
	var previous =species_list[(current_species_index-1)%len(species_list)]
	previous_button.show()
	previous_button.icon = previous.icon
func handle_next_pressed():
	if current_species_index ==INF:
		return
	current_species_index = (current_species_index+1)%species_list.size()
	to_species_detail(species_list[current_species_index])
func handle_previous_pressed():
	if current_species_index ==INF:
		return
	current_species_index = (current_species_index-1)
	to_species_detail(species_list[current_species_index])
func handle_species_configured(value:Species):
	if species:
		species.queue_free()
	species = value
	on_species_picked.emit(species)
	
	
