@tool
extends Control

@onready var class_icon = $HBoxContainer/Portrait/ClassIcon
@onready var class_name_label = $HBoxContainer/Portrait/ClassName
@onready var health_dice = $HBoxContainer/SpeciesSummary/HealthDice
@onready var advantage_ability_1_label = $HBoxContainer/SpeciesSummary/HBoxContainer/GeneralCharacteristics/AdvantageAbilityOne/AdvantageAbility1Label
@onready var advantage_ability_2_label = $HBoxContainer/SpeciesSummary/HBoxContainer/GeneralCharacteristics/AdvantageAbilityOne2/AdvantageAbility2Label
@onready var casting_ability_label = $HBoxContainer/SpeciesSummary/HBoxContainer/GeneralCharacteristics/CastingAbility/CastingAbilityLabel
@onready var special_ability_texture = $HBoxContainer/SpeciesSummary/HBoxContainer/GeneralCharacteristics/ClassSpecialAbility/SpecialAbilityTexture
@onready var casting_ability = $HBoxContainer/SpeciesSummary/HBoxContainer/GeneralCharacteristics/CastingAbility

@export var class_design : GameClassDesign:set=_set_class_design

signal on_clicked(GameClassDesign)

func _ready():
	refresh()
	gui_input.connect(handle_gui_event)
func handle_gui_event(event:InputEvent):
	if event is InputEventMouseButton:
		on_clicked.emit(class_design)

func refresh():
	refresh_header()
	refresh_summary()
	
func refresh_header():
	class_icon.texture = class_design.icon
	class_name_label.text = class_design.name
func refresh_summary():
	health_dice.class_design =class_design
	refresh_general_characteristics()
func refresh_general_characteristics():
	advantage_ability_1_label.text = CreatureBattleStatistics.get_ability_short_name(class_design.advantage_saving_abilities[0])
	advantage_ability_2_label.text = CreatureBattleStatistics.get_ability_short_name(class_design.advantage_saving_abilities[1])
	if class_design.casting_ability == CreatureBattleStatistics.Type.NONE:
		casting_ability.hide()
	else:
		casting_ability.show()
		casting_ability_label.text = CreatureBattleStatistics.get_ability_short_name((class_design.casting_ability))
	special_ability_texture.texture = class_design.featured_ability_icon
func _set_class_design(value:GameClassDesign):
	class_design = value
	if not is_node_ready():
		await ready
	refresh()
	
