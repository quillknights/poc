extends Control
var species:SpeciesDesign:set=_set_species

@onready var str_display = $HBoxContainer/STR
@onready var dex_display = $HBoxContainer/DEX
@onready var con_display = $HBoxContainer/CON
@onready var int_display = $HBoxContainer/INT
@onready var wis_display = $HBoxContainer/WIS
@onready var cha_display = $HBoxContainer/CHA

@export var creature_battle_statistics:CreatureBattleStatistics


func _set_species(value:SpeciesDesign):
	species = value
	if not is_node_ready():
		await ready
	creature_battle_statistics.buffs.clear()
	for buff in species.ability_buffs:
		creature_battle_statistics.add_buff(buff)
	refresh()
func refresh():
	
	str_display.value_text = "%+d"%[creature_battle_statistics.get_value(CreatureBattleStatistics.Type.STR)]
	dex_display.value_text = "%+d"%[creature_battle_statistics.get_value(CreatureBattleStatistics.Type.DEX)]
	con_display.value_text = "%+d"%[creature_battle_statistics.get_value(CreatureBattleStatistics.Type.CON)]
	int_display.value_text = "%+d"%[creature_battle_statistics.get_value(CreatureBattleStatistics.Type.INT)]
	wis_display.value_text = "%+d"%[creature_battle_statistics.get_value(CreatureBattleStatistics.Type.WIS)]
	cha_display.value_text = "%+d"%[creature_battle_statistics.get_value(CreatureBattleStatistics.Type.CHA)]
	str_display.modifier_text = ""
	dex_display.modifier_text = ""
	con_display.modifier_text = ""
	int_display.modifier_text = ""
	wis_display.modifier_text = ""
	cha_display.modifier_text = ""
	refresh_optionals()
func refresh_optionals():
	pass
