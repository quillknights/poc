class_name CreationEquipmentStep
extends Control
@export var next_step:Node
@export var picked_species_design	:SpeciesDesign:set= _set_picked_species_design
@export var picked_class_design		:GameClassDesign:set= _set_picked_class_design
var picked_class					:GameClass
var picked_battle_statistics:CreatureBattleStatistics:set=_set_picked_battle_statistics
var choices:Dictionary= {}
@onready var options = $ChoiceContainer/Panel/MarginContainer/VBoxContainer/Options
@onready var species_tab = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/SpeciesAndClass/SpeciesTab
@onready var class_list_tab = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/SpeciesAndClass/ClassListTab
@onready var battle_statistics = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/BattleStatistics
signal on_equipment_picked(Array)
func accept_creator(creator):
	picked_battle_statistics = creator.picked_statistics
	picked_class_design = creator.picked_class.design
	picked_species_design = creator.picked_species.design
	picked_class = creator.picked_class

func refresh():
	species_tab.species = picked_species_design
	class_list_tab.class_design = picked_class_design
	battle_statistics.statistics = picked_battle_statistics
	refresh_choices()
func refresh_choices():
	NodeTools.clear_children(options)
	for choice in picked_class_design.equiment_choices:
		var display = choice.get_display()
		display.show_title = false
		display.on_choice_made.connect(handle_equiment_choice_made)
		options.add_child(display)
func handle_equiment_choice_made(choice:CreationChoice):
	choices[choice.design] = choice
	if len(choices) == picked_class_design.equiment_choices.size():
		on_equipment_picked.emit(choices.values())
		var equipements:Array[CreationChoice] = []
		equipements.assign(choices.values())
		picked_class.set_equipment_choices(equipements)
func _set_picked_species_design(value:SpeciesDesign):
	picked_species_design = value
	if not is_node_ready():
		await ready
	refresh()
func _set_picked_class_design(value:GameClassDesign):
	picked_class_design = value
	if not is_node_ready():
		await ready
	refresh()
func _set_picked_battle_statistics(value:CreatureBattleStatistics):
	picked_battle_statistics = value
	if not is_node_ready():
		await ready
	refresh()
