@tool
extends Control
var LEVEL_ICON = preload("res://scenes/creature/creation/spell_level_icon.tscn")
@export var class_level : GameClassLevelDesign:set=_set_class_level

@onready var level_counter = $GridContainer/LevelCounter
@onready var proficiency = $GridContainer/Proficiency
@onready var spell_available_list = $Details/Spells/SpellAvailableList
@onready var spells = $Details/Spells
@onready var spell_slot_list = $Details/SpellSlot/SpellSlotList
@onready var template_spell_slot_icon = $TemplateSpellSlotIcon
@onready var template_slot_level_label = $Details/SpellSlot/TemplateSlotLevelLabel
@onready var template_slot_icon_container = $Details/SpellSlot/TemplateSlotIconContainer
@onready var spell_slot_container = $Details/SpellSlot
@onready var ability_icon_template = $Details/Abilities/AbilityIconTemplate
@onready var abilities_list = $Details/Abilities/AbilitiesList
@onready var options = $Options

var _level_value:GameClassLevel
var _option_values:Dictionary = {}

signal on_level_update(GameClassLevel,GameClassLevelDesign)

func refresh():
	refresh_header()
	refresh_details()
	refresh_options()


func 	refresh_header():
	level_counter.text = "Level  %s"%[class_level.level]
	proficiency.text = "Proficiency %+d"%[CreatureBattleStatistics.proficiency_for_level(class_level.level)]
func 	refresh_details():
	refresh_spells()
	refresh_spell_slots()
	refresh_abilities()
func refresh_abilities():
	NodeTools.clear_children(abilities_list)
	for action in class_level.actions:
		var icon = ability_icon_template.duplicate()
		icon.texture = action.icon
		icon.show()
		abilities_list.add_child(icon)
func refresh_spell_slots():
	NodeTools.clear_children(spell_slot_list)
	var slot_count = range(0,6).map(func(_c):return 0)
	for resource in class_level.resources:
		if resource is SpellSlot:
			slot_count[resource.level]+=1
	if slot_count.all(func(c):return c==0):
		spell_slot_container.modulate = Color.TRANSPARENT
	else:
		spell_slot_container.modulate = Color.WHITE
	for level in range(len(slot_count)):
		if slot_count[level]==0:
			continue
		var label = template_slot_level_label.duplicate()
		label.text = "LVL %s"%[level]
		label.show()
		
		var icons = template_slot_icon_container.duplicate()
		for i in range(slot_count[level]):
			var icon = template_spell_slot_icon.duplicate()
			icon.show()
			icons.add_child(icon)
		icons.show()
		spell_slot_list.add_child(label)
		spell_slot_list.add_child(icons)
		
func refresh_spells():
	NodeTools.clear_children(spell_available_list)	
	if not class_level.spell_choices:
		spells.modulate=Color.TRANSPARENT
		return
	spells.modulate=Color.WHITE
	for spell_choice in class_level.spell_choices:
		var icon = LEVEL_ICON.instantiate()
		icon.level = spell_choice.level
		icon.count = spell_choice.choice_count
		spell_available_list.add_child(icon)
		
func 	refresh_options():
	if Engine.is_editor_hint():
		return
	NodeTools.clear_children(options)
	for choice in class_level.spell_choices:
		add_option(choice.get_display())
	for choice in class_level.options:
		add_option(choice.get_display())
func add_option(display:Node):
	display.on_choice_made.connect(handle_option_picked)
	options.add_child(display)
		

func update_level():
	if _level_value and _level_value.get_parent()==null:
		_level_value.queue_free()
	_level_value = class_level.get_game_class_level()
	for choice in _option_values.values():
		if choice.get_parent()!=null:
			choice.get_parent().remove_child(choice)
		_level_value.add_choice(choice)
	if _level_value.is_fully_configured():
		on_level_update.emit(_level_value,class_level)
func handle_option_picked(option:CreationChoice):
	_option_values[option.design]=option
	update_level()



func _set_class_level(value:GameClassLevelDesign):
	class_level = value
	if not is_node_ready():
		await ready
	refresh()
