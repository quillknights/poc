class_name ConditionOptionDisplay
extends Control

var option_design:CreationOptionDesign:set=_set_option_design

@onready var icon = $VBoxContainer/TitleHolder/Icon
@onready var title = $VBoxContainer/TitleHolder/VBoxContainer/Title
@onready var description = $VBoxContainer/TitleHolder/VBoxContainer/Description
@onready var pick_one = $"VBoxContainer/Pick One"
@onready var details_popup = $DetailsPopup

var _buttons = {}
@onready var popup_icon = $DetailsPopup/VBoxContainer/TitleHolder/Icon
@onready var popup_title = $DetailsPopup/VBoxContainer/TitleHolder/VBoxContainer/Title
@onready var popup_description = $DetailsPopup/VBoxContainer/TitleHolder/VBoxContainer/Description
@onready var option_template = $DetailsPopup/OptionTemplate
@onready var possibilities = $DetailsPopup/VBoxContainer/ScrollContainer/Possibilities

var _picked:Array[Resource] = []
var _creation_option:CreationChoice

signal on_choice_made(CreationChoice)

func _ready():
	pick_one.pressed.connect(handle_pick_on_pressed)

func handle_pick_on_pressed():
	details_popup.popup_centered_ratio()
	
func _set_option_design(value:CreationOptionDesign):
	option_design = value
	if not is_node_ready():
		await ready
	refresh()

func refresh():
	icon.texture = option_design.icon
	title.text = option_design.game_name
	description.text = option_design.game_description
	refresh_popup()
func refresh_popup():
	popup_icon.texture = option_design.icon
	popup_title.text = option_design.game_name
	popup_description.text = option_design.game_description
	NodeTools.clear_children(possibilities)
	_buttons.clear()
	for possibility in option_design.possibilities:
		var display= option_template.duplicate()
		display.name = "%s-option"%[possibility.name]
		display.get_node("Button").toggled.connect(handle_picked.bind(possibility))
		_buttons[possibility] = display.get_node("Button")
		display.get_node("Content/Vbox/Name").text = possibility.name
		display.get_node("Content/Vbox/Description").text = possibility.description
		display.show()
		possibilities.add_child(display)
func update_choice():
	if len(_picked)==option_design.choice_count:
		_creation_option = option_design.get_creation_option()
		_creation_option.choose(_picked)
		on_choice_made.emit(_creation_option)
		details_popup.hide()
func handle_picked(pressed:bool,possibility):
	if pressed:
		if len(_picked)>=option_design.choice_count:
			_buttons[_picked[-1]].button_pressed = false
		_picked.append(possibility)
		pick_one.text = " ".join(_picked.map(func(p):return p.name))
	else:
		_picked.erase(possibility)
		pick_one.text = "Pick"
	update_choice()
