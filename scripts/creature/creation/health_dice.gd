@tool
extends Control
@onready var dice_name = $HealthDice/HBoxContainer/DiceName
@onready var dice_icon = $HealthDice/HBoxContainer/DiceName/DiceIcon

var class_design:GameClassDesign:set=_set_class_design


func refresh():
	dice_name.text = class_design.health_dice.name
	dice_icon.texture = class_design.health_dice.icon

func _set_class_design(value:GameClassDesign):
	class_design = value
	if not is_node_ready():
		await ready
	refresh()
