extends Control

var option_design:CreationOptionDesign:set=_set_option_design
@onready var possibilities = $Holder/Possibilities
@onready var title_holder = $Holder/TitleHolder
@onready var icon = $Holder/TitleHolder/Icon
@onready var title = $Holder/TitleHolder/VBoxContainer/Title
@onready var description = $Holder/TitleHolder/VBoxContainer/Description

@export var show_title	:bool = true

signal on_choice_made(CreationChoice)
var _picked:Array[Resource] = []
var _buttons:Dictionary = {}
var _creation_option:CreationChoice
func _set_option_design(value:CreationOptionDesign):
	option_design = value
	if not is_node_ready():
		await ready
	refresh()

func refresh():
	NodeTools.clear_children(possibilities)
	for possibility in option_design.possibilities:
		if "icon" in possibility:
			var button = get_button(possibility.icon)
			button.toggled.connect(handle_pressed.bind(possibility))
			_buttons[possibility] = button
			possibilities.add_child(button)
	if not show_title:
		title_holder.hide()
	else:
		title_holder.show()
		icon.texture=option_design.icon
		title.text = option_design.game_name
		description.text = option_design.game_description
func update_choice():
	if len(_picked)==option_design.choice_count:
		_creation_option = option_design.get_creation_option()
		_creation_option.choose(_picked)
		on_choice_made.emit(_creation_option)
func handle_pressed(pressed:bool,possibility):
	if pressed:
		if len(_picked)>=option_design.choice_count:
			_buttons[_picked[-1]].button_pressed = false
		_picked.append(possibility)
	else:
		_picked.erase(possibility)
	update_choice()
func get_button(button_icon:Texture2D):
	var button = Button.new()
	button.icon = button_icon
	button.icon_alignment = HORIZONTAL_ALIGNMENT_CENTER
	button.theme_type_variation="ToggleButton"
	button.toggle_mode = true
	button.expand_icon = true
	button.custom_minimum_size = Vector2(50,50)
	return button
