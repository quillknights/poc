class_name SpeciesVariantOption
extends CreationOptionDesign
@export var variants:Array[SpeciesDesign]
func _get_possibilities() -> Array[Resource]:
	possibilities= []
	possibilities.append_array(variants)
	return possibilities
