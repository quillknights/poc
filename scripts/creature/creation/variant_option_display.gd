extends ScrollContainer
var VARIANT_PANE = preload("res://scenes/creature/creation/species_tab.tscn")
var option_design:CreationOptionDesign:set=_set_option_design
@onready var possibilities = $Possibilities
signal on_variant_picked(SpeciesDesign)
func _set_option_design(value:CreationOptionDesign):
	option_design = value
	if not is_node_ready():
		await ready
	refresh()

func refresh():
	NodeTools.clear_children(possibilities)
	for possibility in option_design.possibilities:
		var variant_pane = VARIANT_PANE.instantiate()
		variant_pane.species = possibility
		variant_pane.is_toggle = true
		variant_pane.on_toggle.connect(handle_variant_clicked.bind(variant_pane))
		possibilities.add_child(variant_pane)
func handle_variant_clicked(variant,toggled:bool,pane):
	if pane.toggled:
		on_variant_picked.emit(variant)
		for other in possibilities.get_children():
			if other!=pane and other.toggled:
				other.toggle()
	
