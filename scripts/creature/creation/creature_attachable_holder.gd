class_name CreatureAttachableHolder
extends Node

func add_attachable(attachable):
	if not InterfaceCheckTool.is_attachable(attachable):
		push_error("%s is not attachable"%[attachable])
		return
	add_child(attachable)
func clear():
	NodeTools.clear_children(self)
func attach(creature):
	for attachable in get_children():
		attachable.attach(creature)
func reattach(creature):
	for attachable in get_children():
		attachable.reattach(creature)

func get_class_save_id() -> String:
	return "CreatureAttachableHolder"

func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"attachables":saver.to_save(get_children())
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	for attachable in saver.from_save(save["attachables"]):
		add_child(attachable)
