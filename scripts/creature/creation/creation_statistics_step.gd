class_name CreationStatisticsStep
extends Control
@export var picked_class_design:GameClassDesign:set=_set_picked_class_design
@export var picked_species_design:SpeciesDesign:set=_set_picked_species_design
@export var next_step:Node
var base_statistics:CreatureBattleStatistics = CreatureBattleStatistics.new()
var complete_statistics:CreatureBattleStatistics = CreatureBattleStatistics.new()
@export var point_remaining_text_template:String
@export var remaining_point_texture		:Texture2D
@export var spent_point_texture 		:Texture2D
@onready var species_tab = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/SpeciesAndClass/SpeciesTab
@onready var class_tab = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/SpeciesAndClass/ClassListTab

@onready var point_remaining_label = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/StatsPointRemaining/PointRemainingLabel
@onready var stat_point_container = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/StatsPointRemaining/StatPointContainer

@onready var final_chip_str = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/FinalStats/STR
@onready var final_chip_dex = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/FinalStats/DEX
@onready var final_chip_con = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/FinalStats/CON
@onready var final_chip_int = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/FinalStats/INT
@onready var final_chip_wis = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/FinalStats/WIS
@onready var final_chip_cha = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/FinalStats/CHA

@onready var str_add_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/STRAddButton
@onready var dex_add_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/DEXAddButton
@onready var con_add_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/CONAddButton
@onready var int_add_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/INTAddButton
@onready var wis_add_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/WISAddButton
@onready var cha_add_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/CHAAddButton

@onready var chip_str = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/STR
@onready var chip_dex = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/DEX
@onready var chip_con = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/CON
@onready var chip_int = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/INT
@onready var chip_wis = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/WIS
@onready var chip_cha = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/CHA

@onready var str_less_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/STRLessButton
@onready var dex_less_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/DEXLessButton
@onready var con_less_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/CONLessButton
@onready var int_less_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/INTLessButton
@onready var wis_less_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/WISLessButton
@onready var cha_less_button = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/AbilityDistributor/CHALessButton
signal on_statistics_valid(bool)
# Called when the node enters the scene tree for the first time.
func _ready():
	base_statistics = CreatureBattleStatistics.new()
	complete_statistics = CreatureBattleStatistics.new()
	bind_buttons()
	refresh()
func accept_creator(creator):
	picked_class_design = creator.picked_class.design
	picked_species_design = creator.picked_species.design
func refresh():
	refresh_remaining_points()
	refresh_ability_chips()
	refresh_buttons()
func refresh_remaining_points():
	var total_points = get_remaining_points()
	var i = 0
	while i < total_points:
		stat_point_container.get_child(i).texture = remaining_point_texture
		i+=1
	while i < stat_point_container.get_child_count():
		stat_point_container.get_child(i).texture = spent_point_texture
		i+=1
		
	point_remaining_label.text = point_remaining_text_template%[total_points]
func refresh_ability_chips():
	chip_str.value = base_statistics.get_base_value(CreatureBattleStatistics.Type.STR)
	chip_dex.value = base_statistics.get_base_value(CreatureBattleStatistics.Type.DEX)
	chip_con.value = base_statistics.get_base_value(CreatureBattleStatistics.Type.CON)
	chip_int.value = base_statistics.get_base_value(CreatureBattleStatistics.Type.INT)
	chip_wis.value = base_statistics.get_base_value(CreatureBattleStatistics.Type.WIS)
	chip_cha.value = base_statistics.get_base_value(CreatureBattleStatistics.Type.CHA)
	
	chip_str.modifier = base_statistics.get_modifier(CreatureBattleStatistics.Type.STR)
	chip_dex.modifier = base_statistics.get_modifier(CreatureBattleStatistics.Type.DEX)
	chip_con.modifier = base_statistics.get_modifier(CreatureBattleStatistics.Type.CON)
	chip_int.modifier = base_statistics.get_modifier(CreatureBattleStatistics.Type.INT)
	chip_wis.modifier = base_statistics.get_modifier(CreatureBattleStatistics.Type.WIS)
	chip_cha.modifier = base_statistics.get_modifier(CreatureBattleStatistics.Type.CHA)
	
	final_chip_str.value = complete_statistics.get_value(CreatureBattleStatistics.Type.STR)
	final_chip_dex.value = complete_statistics.get_value(CreatureBattleStatistics.Type.DEX)
	final_chip_con.value = complete_statistics.get_value(CreatureBattleStatistics.Type.CON)
	final_chip_int.value = complete_statistics.get_value(CreatureBattleStatistics.Type.INT)
	final_chip_wis.value = complete_statistics.get_value(CreatureBattleStatistics.Type.WIS)
	final_chip_cha.value = complete_statistics.get_value(CreatureBattleStatistics.Type.CHA)
	
	final_chip_str.modifier = complete_statistics.get_modifier(CreatureBattleStatistics.Type.STR)
	final_chip_dex.modifier = complete_statistics.get_modifier(CreatureBattleStatistics.Type.DEX)
	final_chip_con.modifier = complete_statistics.get_modifier(CreatureBattleStatistics.Type.CON)
	final_chip_int.modifier = complete_statistics.get_modifier(CreatureBattleStatistics.Type.INT)
	final_chip_wis.modifier = complete_statistics.get_modifier(CreatureBattleStatistics.Type.WIS)
	final_chip_cha.modifier = complete_statistics.get_modifier(CreatureBattleStatistics.Type.CHA)
func refresh_buttons():
	var remaining_points = get_remaining_points()
	str_add_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.STR)>=15 or remaining_points == 0
	dex_add_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.DEX)>=15 or remaining_points == 0
	con_add_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.CON)>=15 or remaining_points == 0
	int_add_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.INT)>=15 or remaining_points == 0
	wis_add_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.WIS)>=15 or remaining_points == 0
	cha_add_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.CHA)>=15 or remaining_points == 0
	str_less_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.STR)<=8
	dex_less_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.DEX)<=8
	con_less_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.CON)<=8
	int_less_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.INT)<=8
	wis_less_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.WIS)<=8
	cha_less_button.disabled = base_statistics.get_base_value(CreatureBattleStatistics.Type.CHA)<=8
func get_value_cost(value:int):
	if value == 14:
		return 7
	if value == 15:
		return 9
	return value-8
	
func bind_buttons():
	str_add_button.pressed.connect(handle_more_pressed.bind(CreatureBattleStatistics.Type.STR))
	dex_add_button.pressed.connect(handle_more_pressed.bind(CreatureBattleStatistics.Type.DEX))
	con_add_button.pressed.connect(handle_more_pressed.bind(CreatureBattleStatistics.Type.CON))
	int_add_button.pressed.connect(handle_more_pressed.bind(CreatureBattleStatistics.Type.INT))
	wis_add_button.pressed.connect(handle_more_pressed.bind(CreatureBattleStatistics.Type.WIS))
	cha_add_button.pressed.connect(handle_more_pressed.bind(CreatureBattleStatistics.Type.CHA))
	
	str_less_button.pressed.connect(handle_less_pressed.bind(CreatureBattleStatistics.Type.STR))
	dex_less_button.pressed.connect(handle_less_pressed.bind(CreatureBattleStatistics.Type.DEX))
	con_less_button.pressed.connect(handle_less_pressed.bind(CreatureBattleStatistics.Type.CON))
	int_less_button.pressed.connect(handle_less_pressed.bind(CreatureBattleStatistics.Type.INT))
	wis_less_button.pressed.connect(handle_less_pressed.bind(CreatureBattleStatistics.Type.WIS))
	cha_less_button.pressed.connect(handle_less_pressed.bind(CreatureBattleStatistics.Type.CHA))
func handle_less_pressed(target_ability:CreatureBattleStatistics.Type):
	if get_remaining_points()==0:
		on_statistics_valid.emit(false)
	base_statistics.update_base_value(target_ability,-1)
	complete_statistics.update_base_value(target_ability,-1)
	
	refresh()
func handle_more_pressed(target_ability:CreatureBattleStatistics.Type):
	base_statistics.update_base_value(target_ability,1)
	complete_statistics.update_base_value(target_ability,1)
	if get_remaining_points()==0:
		on_statistics_valid.emit(true)
		
	refresh()
func get_remaining_points():
	var total_points = 27
	for stat in CreatureBattleStatistics.BASIC_CREATURE_STATS:
		total_points -=get_value_cost(base_statistics.get_base_value(stat))
	return total_points
func _set_picked_class_design(value):
	picked_class_design = value
	if not is_node_ready():
		await ready
	base_statistics.queue_free()
	base_statistics = CreatureBattleStatistics.new()
	class_tab.class_design = picked_class_design
	refresh()
func _set_picked_species_design(value):
	picked_species_design = value
	if not is_node_ready():
		await ready
	complete_statistics.buffs.clear()
	species_tab.species = picked_species_design
	for buff in picked_species_design.ability_buffs:
		complete_statistics.add_buff(buff)
	refresh()
