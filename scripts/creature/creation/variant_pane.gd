extends PanelContainer
@onready var portrait = $VBoxContainer/HBoxContainer/Portrait
@onready var name_label = $VBoxContainer/HBoxContainer/VBoxContainer/Name
@onready var statistics = $VBoxContainer/HBoxContainer/VBoxContainer/Statistics

@export var variant:SpeciesDesign:set=_set_variant

func _set_variant(value:SpeciesDesign):
	variant = value
	if not is_node_ready():
		await ready
	refresh()
func refresh():
	portrait.texture = variant.icon
	name_label.text = variant.game_name
	statistics.species = variant
