class_name CreationChoice
extends Node

var design 		:CreationOptionDesign
var condition_designs	:Array[CreatureConditionDesign]
var actions				:Array[CreatureActionDesign]
var spells				:Array[Spell]
var _choices			:Array[Resource]
var items				:Array[GameItem]
var ids_conditions		:Dictionary		={}
var _attached			:bool=false
var creature:Creature

func reattach(target:Creature):
	creature = target
	check_conditions()
	add_spells()
	add_actions()
	
func attach(target:Creature):
	creature = target	
	attach_conditions()
	add_spells()
	add_actions()
	add_items()
	_attached = true
func attach_conditions():
	if multiplayer and not multiplayer.is_server():
		return
	for condition_design in condition_designs:
		attach_condition(condition_design)
func check_conditions():
	if multiplayer and not multiplayer.is_server():
		return
	for id in ids_conditions:
		if not creature.has_condition_with_id(id):
			attach_condition(ids_conditions[id])
			ids_conditions.erase(id)
func attach_condition(condition_design:CreatureConditionDesign):
	var id = MultiplayerService.generate_unique_id()
	ids_conditions[id] = condition_design
	creature.attach_new_condition.rpc(condition_design.resource_path,SI.s(id))	
func add_spells():
	for spell in spells:
		var spell_cast = creature.add_action(spell)
		spell_cast.casting_ability = design.spell_casting_ability
func add_actions():
	for action in actions:
		creature.add_action(action)
func add_items():
	for item in items:
		creature.add_item(item)
func choose(values:Array[Resource]):
	if _attached:
		push_error("Can't choose an alreay attached choice")
		return
	_choices = []
	condition_designs.clear()
	actions.clear()
	spells.clear()
	items.clear()
	for choice in values:
		if choice not in design.possibilities:
			push_error("Bad choice")
			return
		_add_choice(choice)
func _add_choice(value:Resource):
	if value is CreatureConditionDesign:
		condition_designs.append(value)
	elif value is Spell:
		spells.append(value)
	elif value is CreatureActionDesign:
		actions.append(value)	
	elif value is GameItem:
		items.append(value)
	_choices.append(value)
	
func get_class_save_id()->String:
	return "CreationChoice"
func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"design":saver.to_save(design),
		"condition_designs":saver.to_save(condition_designs),
		"actions":saver.to_save(actions),
		"spells":saver.to_save(spells),
		"_choices":saver.to_save(_choices),
		"ids_conditions":saver.to_save(ids_conditions),
		"_attached":_attached,
		"creature":saver.to_save(creature)
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	design  = saver.from_save(save["design"])

	var new_condition_designs :Array[CreatureConditionDesign] = []
	new_condition_designs.append_array(saver.from_save(save["condition_designs"]))
	condition_designs = new_condition_designs

	var new_actions :Array[CreatureActionDesign] = []
	new_actions.append_array(saver.from_save(save["actions"]))
	actions = new_actions

	var new_spells :Array[Spell] = []
	new_spells.append_array(saver.from_save(save["spells"])) 
	spells = new_spells

	var new__choices :Array[Resource] = []
	new__choices.append_array(saver.from_save(save["_choices"])) 
	_choices = new__choices
	
	ids_conditions  = saver.from_save(save["ids_conditions"])
	
	_attached = save["_attached"]
	
	creature  = saver.from_save(save["creature"])
