@tool
extends Control


@export var count:int:set=_set_count
@export var level:int:set=_set_level
@onready var count_label = $HBoxContainer/CountLabel
@onready var level_label = $HBoxContainer/Icon/LevelLabel
@onready var level_icon = $HBoxContainer/Icon

func refresh():
	count_label.text = str(count)
	level_icon.level = level
func _set_count(value:int):
	count = value
	if not is_node_ready():
		await ready
	refresh()
func _set_level(value:int):
	level = value
	if not is_node_ready():
		await ready
	refresh()
