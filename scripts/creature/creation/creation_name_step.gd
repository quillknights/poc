class_name CreationNameStep
extends Control
@export var next_step:Node
@export var picked_species_design	:SpeciesDesign:set= _set_picked_species_design
@export var picked_class_design		:GameClassDesign:set= _set_picked_class_design
var picked_class					:GameClass
var picked_battle_statistics		:CreatureBattleStatistics:set=_set_picked_battle_statistics
var creature_name					:String

@onready var name_field = $ChoiceContainer/Panel/MarginContainer/VBoxContainer/Name
@onready var suggestions = $ChoiceContainer/Panel/MarginContainer/VBoxContainer/Suggestions

@onready var species_tab = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/SpeciesAndClass/SpeciesTab
@onready var class_list_tab = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/SpeciesAndClass/ClassListTab
@onready var battle_statistics = $MarginContainer/DetailsContainer/MarginContainer/VBoxContainer/BattleStatistics

signal on_name_picked(String)
func _ready():
	name_field.text_changed.connect(handle_name_change)
	
func accept_creator(creator):
	picked_battle_statistics = creator.picked_statistics
	picked_class_design = creator.picked_class.design
	picked_species_design = creator.picked_species.design
	picked_class = creator.picked_class

func refresh():
	species_tab.species = picked_species_design
	class_list_tab.class_design = picked_class_design
	battle_statistics.statistics = picked_battle_statistics
	refresh_suggestions()
func refresh_suggestions():
	NodeTools.clear_children(suggestions)
	for name_suggestion in picked_species_design.typical_names:
		var button = Button.new()
		button.text = name_suggestion
		button.pressed.connect(func():name_field.text = name_suggestion)
		suggestions.add_child(button)
	name_field.text = picked_species_design.typical_names[0]
	handle_name_change(name_field.text)
func _set_picked_species_design(value:SpeciesDesign):
	picked_species_design = value
	if not is_node_ready():
		await ready
	refresh()
func _set_picked_class_design(value:GameClassDesign):
	picked_class_design = value
	if not is_node_ready():
		await ready
	refresh()
func _set_picked_battle_statistics(value:CreatureBattleStatistics):
	picked_battle_statistics = value
	if not is_node_ready():
		await ready
	refresh()
func handle_name_change(value):
	creature_name = value
	on_name_picked.emit(value)
