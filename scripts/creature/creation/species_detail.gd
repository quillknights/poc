extends Control


@onready var portrait = $Header/Portrait
@onready var name_label = $Header/NameLabel

@export var species_design		:SpeciesDesign:set=_set_species_design
@export var variant 	:SpeciesDesign

@onready var options = $Options
@onready var speed_label = $Stats/VBoxContainer/Speed/HBoxContainer/SpeedLabel
@onready var sight_label = $Stats/VBoxContainer/Sight/HBoxContainer/SightLabel
@onready var sight = $Stats/VBoxContainer/Sight
@onready var statistics = $Stats/Statistics
@onready var condition_template = $ConditionTemplate

var variants:Control
var choices:Dictionary = {}
var species:Species

signal on_species_configured(Species)
func configure_species():
	if species:
		species.queue_free()
	species = species_design.get_species()
	if variant:
		species.set_variant(variant.get_species())
	for choice in choices.values():
		species.add_choice(choice)
	on_species_configured.emit(species)
func refresh():
	refresh_portrait()
	statistics.species = species_design
	refresh_speed_and_sight()
	refresh_bonuses()
	refresh_variants()
	
func refresh_portrait():
	portrait.texture = species_design.icon
	name_label.text = species_design.game_name

func refresh_speed_and_sight():
	speed_label.text = "%sm"%[species_design.move_speed]
	sight_label.text = "%sm"%[species_design.night_sight]
	if not species_design.night_sight:
		sight.hide()
	else:
		sight.show()
func refresh_bonuses():
	NodeTools.clear_children(options)
	for condition in species_design.conditions:
		var display = condition_template.duplicate()
		display.show()
		display.get_node("ConditionIcon").texture = condition.character_summary_icon
		display.get_node("Text/Title").text = condition.name
		display.get_node("Text/Description").text = condition.description
		options.add_child(display)
	for option in species_design.options:
		var display = option.get_display()
		display.on_choice_made.connect(handle_choice_made)
		options.add_child(display)
		
func refresh_variants():
	if variants:
		variants.get_parent().remove_child(variants)
		if is_instance_valid(variants):
			variants.queue_free()
	if species_design.variants:
		variants = species_design.variants.get_display()
		variants.on_variant_picked.connect(handle_variant_picked)
		add_child(variants)
		
func handle_variant_picked(picked):
	variant = picked
	configure_species()
func handle_choice_made(choice):
	choices[choice.design] = choice
	configure_species()

func _set_species_design(value:SpeciesDesign):
	species_design = value
	if not is_node_ready():
		await ready
	refresh()
	
	
