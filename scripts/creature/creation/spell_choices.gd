class_name SpellChoice
extends CreationOptionDesign
@export_group("class source")
@export var class_spell_list:GameClassSpells
@export var level:int = 0
@export_group("Raw List")
@export var spells:Array[Spell]

func _get_possibilities()->Array[Resource]:
	possibilities = []
	if class_spell_list:
		possibilities.append_array(class_spell_list.get_level(level))
	elif spells:
		possibilities.append_array(spells)		
	return super._get_possibilities()
