extends PanelContainer

signal on_click(SpeciesDesign)
signal on_toggle(SpeciesDesign,bool)
@export var species:SpeciesDesign	:set=_set_species
@export var is_toggle					:bool = false
@export var toggled					:bool = false
@onready var species_abilities = $HBoxContainer/SpeciesSummary/SpeciesAbilities

@onready var species_icon = $HBoxContainer/Portrait/AspectRatioContainer/SpeciesIcon
@onready var species_name = $HBoxContainer/Portrait/SpeciesName
@onready var statistics = $HBoxContainer/SpeciesSummary/Statistics
@onready var speed_label = $HBoxContainer/SpeciesSummary/SpeedAndSight/Speed/HBoxContainer/SpeedLabel
@onready var sight_label = $HBoxContainer/SpeciesSummary/SpeedAndSight/Sight/HBoxContainer/SightLabel
@onready var sight = $HBoxContainer/SpeciesSummary/SpeedAndSight/Sight

func _ready():
	gui_input.connect(handle_gui_input)
func handle_gui_input(event:InputEvent):
	if event is InputEventMouseButton:
		if not event.is_pressed() and event.button_index == MOUSE_BUTTON_LEFT:
			handle_clicked()
func handle_clicked():
	on_click.emit(species)
	toggle()
func toggle():
	if not is_toggle:
		return
	toggled = not toggled
	on_toggle.emit(species,toggled)
	refresh_display()
func _set_species(value:SpeciesDesign):
	species = value
	if not is_node_ready():
		await ready
	refresh_display()
func refresh_display():
	refresh_background()
	refresh_portrait()
	refresh_stat()
	refresh_speed_and_sight()
	refresh_bonuses()
func refresh_background():
	if not toggled:
		theme_type_variation = ""
	else:
		theme_type_variation = "LightContainer"
func refresh_portrait():
	species_icon.texture = species.icon
	species_name.text = species.game_name
func refresh_stat():
	statistics.species = species
func refresh_speed_and_sight():
	speed_label.text = "%sm"%[species.move_speed]
	if species.night_sight!=-1:
		sight_label.text = "%sm"%[species.night_sight]
		sight.show()
	else:
		sight.hide()
func refresh_bonuses():
	for child in species_abilities.get_children():
		species_abilities.remove_child(child)
		child.queue_free()
	for option in species.options:
		var bonus_view = TextureRect.new()
		bonus_view.texture = option.icon
		species_abilities.add_child(bonus_view)
		
