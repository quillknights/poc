@tool
class_name CreationClassStep
extends Control

var CLASS_TAB_SCENE	=preload("res://scenes/creature/creation/class_list_tab.tscn")
var CLASS_DETAILS_SCENE = preload("res://scenes/creature/creation/creation_class_details.tscn")
@onready var class_container = $MarginContainer/DetailsContainer/MarginContainer/ScrollContainer/ClassContainer
@export var class_registry:GameClassRegistry:set=_set_class_registry
@export var next_step:Node
@onready var class_details_container = $MarginContainer/DetailsContainer/MarginContainer
@onready var previous_button = $HBoxContainer/PreviousButton
@onready var next_button = $HBoxContainer/NextButton

signal on_class_picked()
var current_class_design:GameClassDesign
var game_class
var class_details

func _ready():
	next_button.pressed.connect(handle_next_pressed)
	previous_button.pressed.connect(handle_previous_pressed)
func handle_next_pressed():
	var next_class_index = (class_registry.game_classes.find(current_class_design)+1)%class_registry.game_classes.size()
	focus_class_design(class_registry.game_classes[next_class_index])
func handle_previous_pressed():
	var previous_index = class_registry.game_classes.find(current_class_design)-1	
	focus_class_design(class_registry.game_classes[previous_index])
func _set_class_registry(value:GameClassRegistry):
	class_registry = value
	if not is_node_ready():
		await ready
	NodeTools.clear_children(class_container)
	for class_element in class_registry.game_classes:
		var class_view = CLASS_TAB_SCENE.instantiate()
		class_view.class_design = class_element
		class_container.add_child(class_view)
		class_view.on_clicked.connect(handle_class_tab_clicked)
func handle_class_tab_clicked(design:GameClassDesign):
	focus_class_design(design)
func refresh_previous_button():
	if not current_class_design:
		previous_button.hide()
	else:
		previous_button.show()
		var previous_index = class_registry.game_classes.find(current_class_design)-1
		previous_button.icon = class_registry.game_classes[previous_index].icon
func refresh_next_button():
	if not current_class_design:
		next_button.hide()
	else:		
		next_button.show()
		var next_class_index = (class_registry.game_classes.find(current_class_design)+1)%class_registry.game_classes.size()
		next_button.icon = class_registry.game_classes[next_class_index].icon
func handle_class_update(new_class:GameClass):
	game_class = new_class
	on_class_picked.emit(game_class)
	
func focus_class_design(design:GameClassDesign):
	current_class_design = design
	if game_class:
		game_class.queue_free()
	class_container.hide()
	if class_details:
		class_details_container.remove_child(class_details)
		class_details.queue_free()
	class_details = CLASS_DETAILS_SCENE.instantiate()
	class_details.class_design = design
	class_details.on_class_update.connect(handle_class_update)
	class_details_container.add_child(class_details)
	refresh_previous_button()
	refresh_next_button()
