class_name CreatureAIState

extends Node

@export var creature:Creature
var entered:bool = false
func _ready():
	pass
func enter_state():
	print("Entering %s"%[name])
	entered = true
func leave_state():
	entered=false

func is_idle()->bool:
	push_error("Child should have overiden is_idle")
	return true
func update()->CreatureAIState:
	push_error("Child should have overiden update")	
	return self
func end_turn():
	if not ResourceService.get_battle().turn_manager.is_turn_of(creature):
		return
	push_warning("ending turn of %s"%[creature.game_name])
	print("ending turn")
	
	ResourceService.get_battle().turn_manager.end_turn.rpc()
