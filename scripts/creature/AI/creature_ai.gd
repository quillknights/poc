class_name CreatureAI
extends Node
@export var creature 	:Creature
@export var start_state	:CreatureAIState
@export var state		:CreatureAIState
var is_active			:bool = false
var turn_manager		:TurnManager
# Called when the node enters the scene tree for the first time.
func _ready():
	state = start_state
func check_turn_manager():
	if GameEventService.battle_running and ResourceService.get_battle():
		turn_manager = ResourceService.get_battle().turn_manager
func _process(_delta):
	if not is_active:
		return
	if not multiplayer.is_server():
		return
	elif not turn_manager:
		check_turn_manager()	
	elif not turn_manager.has_battle_started():
		return
	elif turn_manager.is_turn_of(creature):
		if not state.is_idle():
			update_state()
func update_state():
	var next_state = state.update()
	if next_state==null:
		print("State null !")
	if next_state!=state:
		print("Next state %s"%[next_state.name])
		state.leave_state()
		state = next_state
		state.enter_state()
