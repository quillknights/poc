class_name RoamingAIState
extends ActState

var waiting =  false

func _ready():
	super._ready()
	creature.on_turn_start.connect(handle_turn_start)
func handle_turn_start(_c):
	action = creature.actions.get_children().filter(func(a):return a is Move)[0]
	waiting = false
func enter_state():
	super.enter_state()
	waiting = false
func is_idle()->bool:
	return super.is_idle() 
func handle_acted(_action):
	acting = false
	if creature.battle_resources.get_resource(CreatureBattleResources.MOUVEMENT)>=1:
		return
	acted = true
func update()->CreatureAIState:
	if acting:
		return self
	if acted :
		return super.update()
	if waiting:
		return self
	if not action.is_affordable(creature) and ResourceService.get_battle().turn_manager.is_turn_of(creature):
		waiting = true
		end_turn()
		return self
	var possibles = HexCoordTool.neightbors(creature.hex_coord)
	var terrain:Terrain = ResourceService.get_battle().terrain
	var start = creature.hex_coord
	for p in possibles:
		var path = terrain.find_path(start,p)
		if path:
			action.set_path.rpc_id(1,ShapeTools.make_smooth_path(path))
			return super.update()
	waiting = true
	end_turn()
	return self
