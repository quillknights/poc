class_name PickActionState
extends CreatureAIState

@export var walk_to_range_state 	:CreatureAIState
@export var act_state				:CreatureAIState
var target							:Creature
var attack_action					:Attack
var waiting							:bool=false

func _ready():
	super._ready()
	creature.on_turn_start.connect(handle_turn_start)
func handle_turn_start(_c):
	waiting = false
	attack_action = creature.actions.get_children().filter(func(a):return a is Attack)[0]
func is_idle()->bool:
	return creature.is_acting or waiting or not ResourceService.get_battle().turn_manager.is_turn_of(creature)
	
func update()->CreatureAIState:
	if waiting:
		return self
	if attack_action.can_attack_creature(target):
		act_state.action = attack_action
		attack_action.set_target(SI.s(target.unique_id))
		return act_state
	else:
		walk_to_range_state.target_hex = target.hex_coord
		walk_to_range_state.target_range = attack_action.attack_range
		return walk_to_range_state
