class_name PickingTargetState
extends CreatureAIState

@export var roaming_state 	:CreatureAIState
@export var acting_state	:CreatureAIState

func is_idle()->bool:
	return false
	
func update()->CreatureAIState:
	if not creature.fov:
		return self
	var possibles = creature.fov.get_creature_in_los()
	for p in possibles:
		if p==creature or TeamService.are_creature_allied(creature,p):
			continue
		acting_state.target=p
		return acting_state
	return roaming_state
