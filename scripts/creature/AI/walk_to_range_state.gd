class_name WalkToRangeState
extends ActState
@export var pick_action_state	:CreatureAIState

var waiting =  false
var target_hex			:Vector2	=Vector2.INF
var target_range		:float		=INF
var configured 			:bool		= false
func _ready():
	super._ready()
	creature.on_turn_start.connect(handle_turn_start)
func handle_turn_start(_c):
	action = creature.actions.get_children().filter(func(a):return a is Move)[0]
	waiting = false
func enter_state():
	super.enter_state()
	waiting = false
	configure_move()

func leave_state():
	target_hex=Vector2.INF
	target_range=INF
func is_idle()->bool:
	return super.is_idle() or waiting or not action.is_affordable(creature)	
func configure_move():
	var neightbors = HexCoordTool.neightbors(creature.hex_coord)
	neightbors.sort_custom(func (a,b):return HexCoordTool.distance(target_hex,a)<HexCoordTool.distance(target_hex,b))
	for n in neightbors:
		var path = ResourceService.get_battle().terrain.find_path(creature.hex_coord,n)
		if path:
			action.set_path.rpc_id(1,ShapeTools.make_smooth_path(path))
			configured = true
			return
func update()->CreatureAIState:
	if acting:
		return self
	if acted :
		return super.update()
	if waiting:
		return self
	if not configured:
		return picking_target_state
	if not action.is_affordable(creature) and ResourceService.get_battle().turn_manager.is_turn_of(creature):
		waiting = true
		end_turn()
		return self
	return super.update()
