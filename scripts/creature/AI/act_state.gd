class_name ActState
extends CreatureAIState

@export var picking_target_state 	:CreatureAIState
var action			:CreatureAction:set=_set_action
var acting			:bool=false
var acted			:bool=false
func _ready():
	super._ready()
	
func is_idle()->bool:
	return acting or creature.is_acting or not ResourceService.get_battle().turn_manager.is_turn_of(creature)
func enter_state():
	super.enter_state()
	acting = false
	acted = false
func leave_state():
	super.leave_state()
	acting = false
	acted = false


func update()->CreatureAIState:
	if acting:
		return self
	if acted:
		end_turn()
		return picking_target_state
	acting = true
	action.act.rpc_id(1)
	return self

func handle_acted(_action):
	acted=true
	acting = false

func _set_action(value:CreatureAction):
	if not multiplayer.is_server():
		return
	if action:
		action.on_acted.disconnect(handle_acted)
	action = value
	action.on_acted.connect(handle_acted)
	
