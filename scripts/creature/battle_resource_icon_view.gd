class_name BattleResourceIconView
extends Node2D

signal on_toggle(BattleResourceIconView,BattleResource,bool)

@export var resource	:BattleResource
@export var amount		:int
@export var depletted	:int
@export var GRID_OFFSET=75
@export var togglable	:bool
var GRID_COLS=3
var GRID_ROWS=3
@onready var summary_sprite = $Summary/summary_sprite
@onready var summary_label = $Summary/summary_label
@onready var summary = $Summary
@onready var grid = $Grid
@onready var resource_level_indicator = $ResourceLevelIndicator
@onready var button:Button = $Button

var roman_values = ["0","I","II","III","IV","V","VI"]

func _ready():
	refresh_sprites()
	
func refresh_sprites():
	grid.hide()
	summary.hide()
	button.toggle_mode = togglable
	button.pressed.connect(handle_button_pressed)
	button.toggled.connect(handle_button_toggled)
	if (amount +depletted)> GRID_COLS*GRID_ROWS:
		summary_display()
	else:
		grid_display()
	if resource is SpellSlot:
		resource_level_indicator.show()
		resource_level_indicator.text = str(resource.level)
func summary_display():
	summary_label.text = str(amount)
	summary_sprite.texture = resource.icon
	summary.show()
func grid_display():
	for child in grid.get_children():
		grid.remove_child(child)
		child.queue_free()
	var grid_scale = Vector2.ONE*1.5
	var total = amount+depletted
	var offset = GRID_OFFSET*1.5
	var cols = 2
	var rows = cols
	if total == 1:
		cols = 1
		rows = 1
		grid_scale = Vector2.ONE*2
	if total ==2:
		cols = 2
		rows = 1
	if total >4:
		grid_scale = Vector2.ONE
		offset = GRID_OFFSET
		cols = 3
		rows = 3
	for i in range(amount):
		var sprite = Sprite2D.new()
		sprite.scale = grid_scale
		sprite.texture = resource.icon
		sprite.position = get_grid_position(i,offset,cols,rows)
		grid.add_child(sprite)
	if resource.spent_icon:
		for i in range(depletted):
			var sprite = Sprite2D.new()
			sprite.texture = resource.spent_icon
			sprite.position = get_grid_position(amount+i,offset,cols,rows)
			sprite.scale = grid_scale
			grid.add_child(sprite)
	grid.show()
	
func get_grid_position(index,offset,cols,rows)->Vector2:
	var offset_center_x = offset/2*(cols/2)
	if cols%2==1:
		offset_center_x = offset*(cols/2)
	var offset_center_y = offset_center_x
	if rows==1:
		offset_center_y=0
	@warning_ignore("integer_division")
	var x = offset*(index%cols)-offset_center_x
	@warning_ignore("integer_division")
	var y = offset*(index/cols)-offset_center_y
	return Vector2(x,y)
func handle_button_pressed():
	pass
func handle_button_toggled(state:bool):
	on_toggle.emit(self,resource,state)
func toggle(state:bool):
	if button.button_pressed!=state:
		button.button_pressed=state
