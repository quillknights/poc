extends PanelContainer

const CLASS_LEVEL_LIST_ENTRY = preload("res://scenes/creature/character_sheet_element/class_level_list_entry.tscn")
var creature			:Creature:set=_set_creature
@onready var str_stat = $VBoxContainer2/VBoxContainer/BattleStatistics/STR
@onready var dex_stat = $VBoxContainer2/VBoxContainer/BattleStatistics/DEX
@onready var con_stat = $VBoxContainer2/VBoxContainer/BattleStatistics/CON
@onready var int_stat = $VBoxContainer2/VBoxContainer/BattleStatistics/INT
@onready var wis_stat = $VBoxContainer2/VBoxContainer/BattleStatistics/WIS
@onready var cha_stat = $VBoxContainer2/VBoxContainer/BattleStatistics/CHA
@onready var class_list_container = $VBoxContainer2/MarginContainer/ScrollContainer/ClassList
@onready var battle_statistics = $VBoxContainer2/VBoxContainer/BattleStatistics

# Called when the node enters the scene tree for the first time.
func _set_creature(value:Creature):
	creature = value
	refresh_statistics()
	refresh_class_list()
	
	
func refresh_statistics():
	battle_statistics.statistics = creature.statistics
func refresh_class_list():
	
	for child in class_list_container.get_children():
		class_list_container.remove_child(child)
		child.queue_free()
	var classes = creature.game_classes.get_children()
	for game_class in classes:
		for level in game_class.get_levels():
			var margin_container = MarginContainer.new()
			margin_container.offset_top = 10
			margin_container.offset_bottom = 10
			var class_level_entry = CLASS_LEVEL_LIST_ENTRY.instantiate()
			class_level_entry.class_level = level
			margin_container.add_child(class_level_entry)
			class_list_container.add_child(margin_container)
		

