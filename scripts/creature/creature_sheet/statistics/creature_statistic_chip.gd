@tool
class_name StatisticsChip
extends Control


@export var short_name		:String
@onready var short_name_label = $Bgpanel/VBoxSTR/MarginContainer/ShortNameLabel
@onready var value_label = $Bgpanel/VBoxSTR/Control/ValueLabel
@onready var mod_label = $Bgpanel/VBoxSTR/MarginContainer2/ModLabel

var value		:int	=18	:set=_set_value
var value_text	:String	="18"	:set=_set_value_text
var modifier	:int	=4	:set=_set_modifier
var modifier_text:String="+4"	:set=_set_modifier_text
# Called when the node enters the scene tree for the first time.
func _ready():
	refresh()
func refresh():
	short_name_label.text = short_name
	value_label.text = value_text
	mod_label.text = modifier_text
	if modifier_text=="":
		mod_label.hide()
	else:
		mod_label.show()

func _set_value(v:int):
	value = v
	value_text = str(v)
func _set_modifier(v:int):
	modifier = v
	modifier_text = "%+d"%[modifier]
	
func _set_value_text(v:String):
	value_text = v
	refresh()
func _set_modifier_text(v:String):
	modifier_text = v
	refresh()
