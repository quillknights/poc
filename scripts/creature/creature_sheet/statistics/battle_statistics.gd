extends Control
var statistics:CreatureBattleStatistics:set=_set_statistics
@onready var str_stat = $STR
@onready var dex_stat = $DEX
@onready var con_stat = $CON
@onready var int_stat = $INT
@onready var wis_stat = $WIS
@onready var cha_stat = $CHA

func refresh():
	if not statistics:
		str_stat.value = 0
		dex_stat.value = 0
		con_stat.value = 0
		int_stat.value = 0
		wis_stat.value = 0
		cha_stat.value = 0
	else:
		str_stat.value = statistics.get_base_value(CreatureBattleStatistics.Type.STR)
		str_stat.modifier = statistics.get_modifier(CreatureBattleStatistics.Type.STR)
		dex_stat.value = statistics.get_base_value(CreatureBattleStatistics.Type.DEX)
		dex_stat.modifier = statistics.get_modifier(CreatureBattleStatistics.Type.DEX)
		con_stat.value = statistics.get_base_value(CreatureBattleStatistics.Type.CON)
		con_stat.modifier = statistics.get_modifier(CreatureBattleStatistics.Type.CON)
		int_stat.value = statistics.get_base_value(CreatureBattleStatistics.Type.INT)
		int_stat.modifier = statistics.get_modifier(CreatureBattleStatistics.Type.INT)
		wis_stat.value = statistics.get_base_value(CreatureBattleStatistics.Type.WIS)
		wis_stat.modifier = statistics.get_modifier(CreatureBattleStatistics.Type.WIS)
		cha_stat.value = statistics.get_base_value(CreatureBattleStatistics.Type.CHA)
		cha_stat.modifier = statistics.get_modifier(CreatureBattleStatistics.Type.CHA)

func _set_statistics(value:CreatureBattleStatistics):
	statistics=value
	if not is_node_ready():
		await ready
	refresh()
