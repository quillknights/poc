extends PanelContainer

@onready var class_icon = $HBoxContainer/ClassIcon
@onready var class_name_label = $HBoxContainer/ClassName
@onready var class_level_label = $HBoxContainer/ClassLevel

var class_level	:GameClassLevel:set=_set_class_level


func _set_class_level(value:GameClassLevel):
	if not is_node_ready():
		await ready
	class_level = value
	class_icon.texture = class_level.design.game_class.icon
	class_name_label.text = class_level.design.game_class.name
	class_level_label = "LVL %d"%[class_level.level]

