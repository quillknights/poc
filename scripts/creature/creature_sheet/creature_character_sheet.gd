extends Control

@export var creature:Creature:set=_set_creature
@onready var class_pane_container = $RootPanel/VBoxContainer/MarginContainer/ClassPaneContainer

# Called when the node enters the scene tree for the first time.
func _set_creature(value:Creature):
	creature = value
	if not is_node_ready():
		await ready
	class_pane_container.creature = creature
