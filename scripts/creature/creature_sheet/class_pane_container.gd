extends VBoxContainer

@export var creature:Creature : set=_set_creature

@onready var creature_icon = $CharacterPortrait/CreatureIcon
@onready var name_label = $CharacterPortrait/VBoxContainer/Name
@onready var class_and_species = $CharacterPortrait/VBoxContainer/ClassAndSpecies
@onready var class_detail_block = $ClassDetailBlock

# Called when the node enters the scene tree for the first time.
func _set_creature(value:Creature):
	creature = value
	refresh_header()
	class_detail_block.creature = creature
func refresh_header():
	var class_label = ""
	for game_class in creature.get_game_classes():
		class_label += game_class.name+" "
	creature_icon.texture = creature.icon
	name_label.text = creature.game_name
	class_and_species.text = "Species %s"%[class_label]
	
