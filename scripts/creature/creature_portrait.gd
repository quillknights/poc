extends Node2D
class_name CreaturePortrait
const TAB_UNSELECTED = preload("res://assets/UI/Tab-Unselected.png")
const TAB_SELECTED = preload("res://assets/UI/Tab-Selected.png")
const CREATURE_PORTRAIT_ENEMY = preload("res://shaders/creature_portrait_enemy.tres")
const CREATURE_PORTRAIT_ALLY = preload("res://shaders/creature_portrait_ally.tres")
@export var CONDITIONS_OFFSET=40
@export var creature:Creature:set=_set_creature
@onready var icon = $Icon
@onready var health_bar = $HealthBar
@onready var animation_player = $AnimationPlayer
@onready var background = $Background
@onready var dice_roll:DiceRollView = $DiceRollView
@onready var conditions = $Conditions
@onready var concentration_icon = $ConcentrationIcon

var displayed_health = INF
func _ready():
	MultiplayerService.on_creature_assignement.connect(handle_creature_assignement)
	
func _set_creature(value:Creature):
	if creature:
		creature.on_health_change.disconnect(handle_health_change)
		creature.on_rolling.disconnect(handle_creature_rolling)
		creature.on_death.disconnect(handle_death)
		creature.on_turn_start.disconnect(handle_turn_start)
		creature.on_turn_end.disconnect(handle_turn_end)
		creature.on_conditions_changed.disconnect(handle_conditions_changed)
		creature.on_concentrating.disconnect(handle_creature_concentrating)
		
	creature = value
	if not is_node_ready():
		await ready
	
	creature.on_health_change.connect(handle_health_change)
	creature.on_rolling.connect(handle_creature_rolling)
	creature.on_death.connect(handle_death)
	creature.on_turn_start.connect(handle_turn_start)
	creature.on_turn_resume.connect(handle_turn_resume)
	creature.on_turn_end.connect(handle_turn_end)
	creature.on_conditions_changed.connect(handle_conditions_changed)
	creature.on_concentrating.connect(handle_creature_concentrating)
	refresh_textures()
	refresh_health_bar()
	refresh_condition_list()
func handle_creature_assignement(c:Creature):
	if c == creature:
		refresh_textures()
		refresh_health_bar()
func handle_health_change(_mini):
	refresh_health_bar()
func refresh_textures():
	icon.texture=creature.icon
	if MultiplayerService.is_spectator():
		return
	elif not MultiplayerService.is_my_creature(creature):
		background.material = CREATURE_PORTRAIT_ENEMY
	else:
		background.material = CREATURE_PORTRAIT_ALLY
func refresh_health_bar():
	if displayed_health>creature.health and displayed_health!=INF:
		animation_player.play("take_damage")
	if displayed_health<creature.health :
		animation_player.play("heal")
	displayed_health = creature.health
	var max_health = creature.max_health
	health_bar.text="%d/%d"%[displayed_health,max_health]
func refresh_condition_list():
	for child in conditions.get_children():
		conditions.remove_child(child)
		child.queue_free()
	for condition in creature.conditions.get_children():
		if condition.design.portrait_icon != null:
			var sprite = Sprite2D.new()
			sprite.texture = condition.design.portrait_icon
			sprite.position.x = CONDITIONS_OFFSET*conditions.get_child_count()		
			conditions.add_child(sprite)
func handle_creature_concentrating(_c:Creature):
	if creature.concentration:
		concentration_icon.texture = creature.concentration.spell.icon
		concentration_icon.show()
	else:
		concentration_icon.hide()
func handle_creature_rolling(_creature,rolled:DiceRoll):
	dice_roll.rolled(rolled)
func handle_death(_creature:Creature):
	animation_player.play("die")
func handle_turn_start(_creature:Creature):
	animation_player.play("playing")
func handle_turn_resume(_c:Creature):
	animation_player.play("playing")	
func handle_turn_end(_creature:Creature):
	if animation_player.current_animation=="playing":
		animation_player.stop()
func handle_conditions_changed(_creature:Creature):
	refresh_condition_list()
