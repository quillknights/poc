class_name ValueGetter
extends Object

var state		:Dictionary = {}
var design		:ValueGetterDesign
func get_value(from:Variant):
	return design.get_value(from,state)

func get_class_save_id()->String:
	return "ValueGetter"

func to_save(_saver:GameElementSaver):
	return {
		"state":_saver.to_save(state),
		"design":_saver.to_save(design)
	}
	
func from_save(data:Dictionary,_saver:GameElementSaver):
	state = _saver.from_save(data["state"])
	design = _saver.from_save(data["design"])
