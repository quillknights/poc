class_name SpellSaveDCGetter
extends ValueGetterDesign


func get_value(_from:Variant,getter_state:Dictionary = {}):
	if not getter_state:
		return null 
	return getter_state["caster"].get_spell_difficulty_saving_class(getter_state["spell"])
