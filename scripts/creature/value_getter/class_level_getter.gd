class_name ClassLevelGetter
extends ValueGetterDesign


@export var target_class	:GameClassDesign


func get_value(creature:Variant,_getter_config:Dictionary = {}):
	var class_level = 0
	var game_class = creature.get_game_class(target_class)
	if game_class==null:
		return 0
	for level in game_class.get_levels():
		class_level = max(class_level,level.level)
	return class_level
