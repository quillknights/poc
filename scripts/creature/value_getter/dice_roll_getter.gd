class_name DiceRollGetter
extends ValueGetterDesign
@export var dice:Dice

func get_value(creature:Variant,_getter_config:Dictionary = {}):
	var dice_roll:DiceRoll = creature.get_dice_roll(dice)
	var value = dice_roll.roll()
	GameEventService.register_child(dice_roll.get_game_event())
	return value
