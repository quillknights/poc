class_name DivideGetterDesign
extends CompositeValueGetterDesign

@export var divisor		:float = 2
@export var round_up	:bool = false
@export var round_down	:bool = false

func get_value(from:Variant,getter_state:Dictionary = {}):
	var value = float(child_getter.get_value(from,getter_state))
	var result = value/divisor
	if round_up:
		result = ceil(result)
	if round_down:
		result=floor(result)
	return result
