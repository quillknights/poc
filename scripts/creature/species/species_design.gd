class_name SpeciesDesign
extends Resource

@export var game_name			:String
@export var typical_names		:Array[String]
@export var variants			:SpeciesVariantOption
@export var icon				:Texture2D
@export var conditions			:Array[CreatureConditionDesign]
@export var options				:Array[CreationOptionDesign]
@export var move_speed			:float
@export var night_sight			:float=-1
@export var ability_buffs		:Array[BattleStatisticBuff]


func get_species():
	var species = Species.new()
	species.design = self
	species.name = game_name
	return species
	
