class_name Species
extends Node

@export var design 		:SpeciesDesign
@export var creature	:Creature
@export var variant 	:Species
var ids_conditions	:Dictionary
var choices = CreatureAttachableHolder.new()
func _init():
	choices.name = "Choices"
	add_child(choices)

func attach(target:Creature):
	creature = target
	creature.icon = design.icon
	creature.game_name = design.game_name
	creature.attack_range=1
	if not design.night_sight and not creature.range_of_sight:
		creature.range_of_sight = 3
	else:
		creature.range_of_sight=design.night_sight
	attach_statistics()
	attach_conditions()
	attach_resources()
	attach_choices()
	if variant:
		variant.attach(target)
func reattach(target:Creature):
	creature = target
	reattach_conditions()
	reattach_choices()
func attach_conditions():
	if multiplayer and not multiplayer.is_server():
		return
	for condition_design in design.conditions:
		attach_condition(condition_design)
func reattach_conditions():
	if multiplayer and not multiplayer.is_server():
		return
	for id in ids_conditions:
		if not creature.has_condition_with_id(id):
			attach_condition(ids_conditions[id])
			ids_conditions.erase(id)
	for condition_design in design.conditions:
		if not condition_design in ids_conditions.values():
			attach_condition(condition_design)
func attach_condition(condition_design:CreatureConditionDesign):
	var id = MultiplayerService.generate_unique_id()
	ids_conditions[id]=condition_design
	creature.attach_new_condition.rpc(condition_design.resource_path,SI.s(id))
func attach_statistics():
	for buff in design.ability_buffs:
		creature.statistics.add_buff(buff)
func attach_resources():
	creature.start_mouvements = design.move_speed
func attach_choices():
	for choice in choices.get_children():
		choice.attach(creature)
func reattach_choices():
	for choice in choices.get_children():
		choice.reattach(creature)
func set_variant(value:Species):
	variant = value
	add_child(variant)
func add_choice(choice:CreationChoice):
	choices.add_child(choice)
	
# Called when the node enters the scene tree for the first time.
func _ready():
	if creature == null:
		push_error("Species node must belong to a creature")

func get_class_save_id()->String:
	return "Species"

func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"design":saver.to_save(design),
		"creature":saver.to_save(creature),
		"choices":saver.to_save(choices.get_children()),
		"ids_conditions"	:saver.to_save(ids_conditions),
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	design  = saver.from_save(save["design"])
	creature  = saver.from_save(save["creature"])
	if "choices" in save:
		if save["choices"]["type"]=="Array":
			for choice in saver.from_save(save["choices"]):
				choices.add_attachable(choice)
		else:
			choices = NodeTools.replace_child(self,choices,saver.from_save(save["choices"]))
	if "ids_conditions" in save:
		ids_conditions = saver.from_save(save["ids_conditions"])
