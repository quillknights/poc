class_name HasteFatigueCondition
extends CreatureConditionDesign


func handle_turn_start(condition:CreatureCondition):
	
	var resources = condition.creature.battle_resources
	resources.update_resource(CreatureBattleResources.ACTION,func (c):return c/2)
	resources.update_resource(CreatureBattleResources.MOUVEMENT,func(c):return c/2)
	

