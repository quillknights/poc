class_name DiceRollBuffCondition
extends CreatureConditionDesign
@export var creature_predicates	:Array[CreaturePredicate] = []
@export var modifier			:int = 0
@export var advantage			:bool=false
@export var disadvantage		:bool = false
@export var dice_roll_type		:DiceRoll.Type



func affect_roll(roll:DiceRoll,condition:CreatureCondition):
	var creature = condition.creature
	if creature_predicates.all(func (p):return p.get_value(creature)) and dice_roll_type==roll.type:
		if modifier:
			roll.add_modifier(modifier)
		if advantage:
			roll.add_advantage()
		if disadvantage:
			roll.add_disadvantage()
