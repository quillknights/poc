class_name PoisonedConditionDesign
extends CreatureConditionDesign



func affect_roll(roll:DiceRoll,_condition :CreatureCondition):
	if roll is AttackRoll or roll is AbilityCheck:
		roll.add_disadvantage()
