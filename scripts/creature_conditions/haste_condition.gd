class_name HasteCondition
extends CreatureConditionDesign

@export var haste_fatigue_condition:HasteFatigueCondition

func configure(condition:CreatureCondition,_creature:Creature):
	super.configure(condition,_creature)
	condition.configuration = haste_fatigue_condition
func attach(target_creature:Creature,_condition:CreatureCondition):
	super.attach(target_creature,_condition)
	var resources = target_creature.battle_resources
	resources.update_resource(CreatureBattleResources.ACTION,func (c):return c*2)
	resources.update_resource(CreatureBattleResources.MOUVEMENT,func(c):return c*2)
func check_stacking(creature:Creature,condition:CreatureCondition):
	super.check_stacking(creature,condition)
	for other in creature.conditions.get_children():
		if other.design == haste_fatigue_condition:
			creature.detach_condition.rpc(SI.s(other.instance_id))
			
func handle_turn_start(condition:CreatureCondition):
	
	var resources = condition.creature.battle_resources
	resources.update_resource(CreatureBattleResources.ACTION,func (c):return c*2)
	resources.update_resource(CreatureBattleResources.MOUVEMENT,func(c):return c*2)
	
func detach(_creature:Creature,condition:CreatureCondition):
	var creature = condition.creature
	if MultiplayerService.is_server():
		creature.attach_new_condition.rpc(condition.configuration.resource_path,
		SI.s(MultiplayerService.generate_unique_id())
		)	
	var resources = creature.battle_resources
	resources.update_resource(CreatureBattleResources.ACTION,func (c):return c/2)
	resources.update_resource(CreatureBattleResources.MOUVEMENT,func(c):return c/2)
