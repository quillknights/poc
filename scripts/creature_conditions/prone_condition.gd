class_name ProneCondition
extends CreatureConditionDesign

func handle_turn_start(_condition:CreatureCondition):
	
	var creature = _condition.creature
	var mouvement = creature.battle_resources.get_resource(CreatureBattleResources.MOUVEMENT)
	creature.battle_resources.set_resource_value(CreatureBattleResources.MOUVEMENT,floor(mouvement/2))

func affect_roll(dice_roll:DiceRoll,condition:CreatureCondition):
	if dice_roll is AttackRoll:
		if dice_roll.target_creature==condition.creature:
			dice_roll.add_advantage()
	
