class_name DamageResistanceCondition
extends CreatureConditionDesign

@export var damage_type		:DamageType
@export var resistance		:bool
@export var immunity		:bool
@export var vulnerability	:bool

func configure(condition:CreatureCondition,_creature:Creature):
	super.configure(condition,_creature)
	var config = Configuration.new()
	config.damage_type =damage_type
	config.resistance = resistance
	config.immunity = immunity
	config.vulnerability=vulnerability
	condition.configuration = config

func affect_damage(damage:Damage,condition:CreatureCondition):
	var config = condition.configuration
	if damage.type==config.damage_type:
		if config.resistance:
			damage.resistance 		=true
		elif config.immunity:
			damage.immunity			=true
		elif config.vulnerability:
			damage.vulnerability	=true
			
class Configuration:
	var damage_type		:DamageType
	var resistance		:bool
	var immunity		:bool
	var vulnerability	:bool
