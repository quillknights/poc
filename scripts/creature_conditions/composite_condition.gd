class_name CompositeCondition
extends CreatureConditionDesign


@export var sub_conditions	 				:Array[CreatureConditionDesign]
@export_group("saving_conditions")
@export var saving_throw_ability			:CreatureBattleStatistics.Type = CreatureBattleStatistics.Type.NONE
@export var saving_throw_dc_getter			:ValueGetterDesign
@export var saves_on_damage					:bool
@export var saves_with_advantage_on_damage	:bool
@export var saves_on_start_turn				:bool
@export var saves_on_end_turn				:bool
@export var log_saving_throws				:bool 

func configure(condition:CreatureCondition,_creature:Creature):
	super.configure(condition,_creature)
	var config = Configuration.new()
	config.ability = saving_throw_ability
	config.saves_on_damage = saves_on_damage
	config.saves_with_advantage_on_damage=saves_with_advantage_on_damage
	config.saves_on_start_turn = saves_on_start_turn
	config.saves_on_end_turn = saves_on_end_turn
	config.log_saving_throws	=log_saving_throws
	if not "condition_ids" in condition.state:
		condition.state["condition_ids"]= [] as Array[GameUniqueId]
	if not "dc_getter" in condition.state and saving_throw_dc_getter:
		condition.state["dc_getter"] = saving_throw_dc_getter.init_getter()
	condition.configuration = config
func attach(_creature:Creature,condition:CreatureCondition):
	if MultiplayerService.is_server() and not _get_condition_ids(condition):
		var ids:Array[GameUniqueId] = []
		for sub in sub_conditions:
			var instance_id = MultiplayerService.generate_unique_id()
			ids.append(instance_id)
			_creature.attach_new_condition.rpc(sub.resource_path,SI.s(instance_id))
		_set_condition_ids(condition, ids)
func detach(_creature:Creature,condition:CreatureCondition):
	if MultiplayerService.is_server():
		for id in _get_condition_ids(condition):
			_creature.detach_condition.rpc(SI.s(id))
func handle_turn_start(condition:CreatureCondition):
	if condition.configuration.saves_on_start_turn:
		saving_throw(condition,false)
func handle_turn_end(condition:CreatureCondition):
	if condition.configuration.saves_on_end_turn:
		saving_throw(condition,false)
func affect_damage(_damage:Damage,condition:CreatureCondition):
	if condition.configuration.saves_on_damage:
		saving_throw(condition,saves_with_advantage_on_damage)
func saving_throw(condition:CreatureCondition,advantage:bool):
	if not MultiplayerService.is_server():
		return
	if condition.configuration.ability == CreatureBattleStatistics.Type.NONE:
		return
	var throw = condition.creature.get_saving_throw(condition.configuration.ability)
	var dc = get_dc(condition)
	if advantage:
		throw.add_advantage()
	throw.roll(dc)
	if throw.result>=dc.value:
		condition.creature.detach_condition.rpc(SI.s(condition.instance_id))
		log_event(condition,true,throw)
	else:
		log_event(condition,false,throw)
func _get_condition_ids(condition:CreatureCondition):
	return condition.state["condition_ids"]
func _set_condition_ids(condition,ids):
	condition.state["condition_ids"] = ids

func log_event(condition:CreatureCondition,success:bool,throw:SavingThrow):
	if not condition.configuration.log_saving_throws:
		return 
	var event = GameEvent.new()
	event.type = GameEvent.Type.ConditionSavingThrow
	if success:
		event.sentence = "%s saved against %s (rolled %d)"%[
			condition.creature.game_name,
			name,
			throw.result
		]
	else:
		event.sentence = "%s failed saving throw against %s (rolled %d)"%[
			condition.creature.game_name,
			name,
			throw.result
		]
	event.children = [throw.get_game_event()] as Array[GameEvent]
	GameEventService.instant_event(event)
func get_dc(condition:CreatureCondition):
	var getter =  condition.state["dc_getter"]
	return getter.get_value(condition.creature)
class Configuration:
	var ability	:CreatureBattleStatistics.Type
	var saves_on_damage					:bool
	var saves_with_advantage_on_damage	:bool
	var saves_on_start_turn				:bool
	var saves_on_end_turn				:bool
	var log_saving_throws				:bool
