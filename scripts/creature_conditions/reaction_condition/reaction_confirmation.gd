extends Node2D
@onready var text = $Text
@onready var yes = $HBoxContainer/Yes
@onready var no = $HBoxContainer/No

var reaction:CreatureCondition:set=_set_reaction_condition

signal on_done()

func _ready():
	yes.pressed.connect(handle_yes)
	no.pressed.connect(handle_no)
func _set_reaction_condition(value:CreatureCondition):
	reaction = value
	text.text = reaction.design.get_confirmation_text(reaction)
	
func handle_yes():
	reaction.act()
	on_done.emit()
func handle_no():
	reaction.cancel()
	on_done.emit()
