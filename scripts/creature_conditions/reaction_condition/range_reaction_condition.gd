class_name RangeReactionCondition
extends ReactionCondition

@export var activate_on_range_enter:bool
@export var activate_on_range_left :bool
@export var when_owner_is_moving:bool = false
@export var reaction_range:float

const RANGE_REACTION_DETECTOR = preload("res://scenes/conditions/range_reaction_detector.tscn")

func configure(condition:CreatureCondition,_c:Creature):
	await super.configure(condition,_c)
	var config = Configuration.new()
	config.range_detector = RANGE_REACTION_DETECTOR.instantiate()
	config.range_detector.detection_range = reaction_range	
	config.range_detector.on_creature_entered.connect(handle_creature_enter_range.bind(condition))
	config.range_detector.on_creature_exited.connect(handle_creature_leave_range.bind(condition))
	config.action_node = condition.configuration.action_node
	condition.configuration = config
	
func attach(c:Creature,condition:CreatureCondition):
	await super.attach(c,condition)	
	c.on_mini_change.connect(handle_mini_change.bind(condition))
	if c.creature_mini:
		c.creature_mini.add_child(condition.configuration)
		
func detach(c:Creature,condition:CreatureCondition):
	super.detach(c,condition)
	condition.configuration.on_creature_entered.disconnect(handle_creature_enter_range.bind(condition))
	condition.configuration.on_creature_exited.disconnect(handle_creature_enter_range.bind(condition))
	c.on_mini_change.disconnect(handle_mini_change.bind(condition))
	condition.configuration.detach()
func handle_creature_enter_range(c:Creature,condition:CreatureCondition):
	if c==condition.creature:
		return
	if condition.creature.is_moving and not when_owner_is_moving:
		return
	if activate_on_range_enter:
		condition.configuration.target_creature = c
		configure_action(condition)
		activate(condition)
func handle_creature_leave_range(c:Creature,condition:CreatureCondition):
	if c == condition.creature:
		return
	if condition.creature.is_moving and not when_owner_is_moving:
		return
	if activate_on_range_left:
		condition.configuration.target_creature = c
		configure_action(condition)
		activate(condition)
func handle_mini_change(creature_mini:CreatureMini,condition:CreatureCondition):
	if condition.configuration.range_detector.get_parent()!=null:
		condition.configuration.range_detector.get_parent().remove_child(condition.configuration.range_detector)
	creature_mini.add_child(condition.configuration)

func configure_action(condition:CreatureCondition):
	condition.configuration.action_node.set_target_creature.rpc_id(1,condition.configuration.target_creature.unique_id)
	
func get_confirmation_text(condition:CreatureCondition)->String:
	var config = condition.configuration
	return "Do you want to %s on %s"%[config.action_node.action.name,config.target_creature.game_name]
class Configuration:
	var range_detector	:RangeReactionDetector
	var target_creature	:Creature
	var action_node		:CreatureAction
