class_name ReactionCondition
extends CreatureConditionDesign

@export var ask_confirmation:bool
@export var action:CreatureActionDesign

func attach(c:Creature,condition:CreatureCondition):
	var config = Configuration.new()
	config.action_node =  await c.add_action(action)
	condition.configuration= config
func configure_action(_c:CreatureCondition):
	pass
func activate(condition:CreatureCondition):
	if not MultiplayerService.is_server():
		return
	if condition.creature.battle_resources.reaction<=0:
		return
	if ask_confirmation:
		ask_for_confirmation(condition)
	else:
		act(condition)
func act(condition:CreatureCondition):
	condition.configuration.action_node.act.rpc_id(1)
func cancel():
	pass
func ask_for_confirmation(condition:CreatureCondition):
	CreaturesEventBus.on_reaction_confirmation.emit(condition.creature,condition)
func get_confirmation_text(condition:CreatureCondition)->String:
	return "Do you want to do %s"%[condition.configuration.action_node.name]
class Configuration:
	var action_node:Node2D
