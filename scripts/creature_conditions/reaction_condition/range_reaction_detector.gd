class_name RangeReactionDetector
extends Area2D

@onready var shape = $Shape

signal on_creature_entered(Creature)
signal on_creature_exited(Creature)

var detection_range:float

func _ready():
	area_entered.connect(handle_area_entered)
	area_exited.connect(handle_area_exited)
	shape.shape.radius = detection_range*HexCoordTool.TILE_PIXEL_HEIGHT
func handle_area_entered(other:Area2D):
	if other.get_parent() is CreatureMini:
		on_creature_entered.emit(other.get_parent().creature)
func handle_area_exited(other:Area2D):
	if other.get_parent() is CreatureMini:
		on_creature_exited.emit(other.get_parent().creature)
	
