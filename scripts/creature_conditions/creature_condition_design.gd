class_name CreatureConditionDesign
extends Resource

@export var portrait_icon				:Texture2D:get=_get_portrait_icon
@export var mini_icon					:Texture2D:get=_get_mini_icon
@export var character_summary_icon		:Texture2D:get=_get_character_summary_icon
@export var name						:String
@export var turn_count					:int = -1
@export var condition_script			:GDScript	=preload("res://scripts/creature_conditions/creature_condition.gd")
@export var stacks						:bool	=false
@export var savable						:bool	=false
@export_multiline var description:String

func configure(condition:CreatureCondition,_creature:Creature):
	condition.name			= name
	if condition.turn_count ==-1:
		condition.turn_count 	= turn_count
	condition.description 	= description
	
func _get_portrait_icon()->Texture2D:
	return portrait_icon
func _get_mini_icon()->Texture2D:
	return mini_icon
func _get_character_summary_icon()->Texture2D:
	return character_summary_icon
	
func attach(creature:Creature,condition:CreatureCondition):
	if MultiplayerService.is_server():
		check_stacking(creature,condition)
			
func check_stacking(creature:Creature,condition:CreatureCondition):
	if not stacks:
		for other in creature.conditions.get_children():
			if other != condition and other.design == self:
				creature.detach_condition.rpc(SI.s(other.instance_id))
func detach(_creature:Creature,_condition:CreatureCondition):
	pass
func handle_turn_start(_condition:CreatureCondition):
	pass
func handle_turn_end(_condition:CreatureCondition):
	pass
func handle_damage_taken(_condition:CreatureCondition):
	pass
func affect_roll(_dice_roll:DiceRoll,_condition :CreatureCondition):
	pass
func affect_difficulty_class(_dc:DifficultyClass,_condition:CreatureCondition):
	pass
func affect_action(_action:CreatureAction,_condition:CreatureCondition):
	pass
func affect_damage(_damage:Damage,_condition:CreatureCondition):
	pass

func set_save_dc_getter(condition:CreatureCondition,save_dc):
	condition.state["dc_getter"] = save_dc
