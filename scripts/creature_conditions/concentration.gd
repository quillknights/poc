class_name Concentration
extends Object

var spell:Spell
var configuration

func get_saving_dc(damage:Damage):
	var dc = DifficultyClass.new()
	dc.base_value = max(floor(float(damage.value)/2),10)
	return dc
func fail():
	if MultiplayerService.is_server():
		spell.handle_concentration_failed(self)
func end():
	if MultiplayerService.is_server():
		spell.handle_concentration_end(self)
	
func get_class_save_id()->String:
	return "Concentration"
func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
	"spell":saver.to_save(spell),
	"configuration":saver.to_save(configuration)
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	spell  = saver.from_save(save["spell"])
	configuration  = saver.from_save(save["configuration"])
