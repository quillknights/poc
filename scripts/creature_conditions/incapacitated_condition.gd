class_name IncapacitatedConditionDesign
extends CreatureConditionDesign

func handle_turn_start(_condition:CreatureCondition):
	_condition.creature.battle_resources.set_resource_value(CreatureBattleResources.ACTION,0)
	_condition.creature.battle_resources.set_resource_value(CreatureBattleResources.BONUS_ACTION,0)
	_condition.creature.battle_resources.set_resource_value(CreatureBattleResources.REACTION,0)
func handle_turn_end(_condition:CreatureCondition):
	_condition.creature.battle_resources.set_resource_value(CreatureBattleResources.ACTION,0)
	_condition.creature.battle_resources.set_resource_value(CreatureBattleResources.BONUS_ACTION,0)
	_condition.creature.battle_resources.set_resource_value(CreatureBattleResources.REACTION,0)	
func affect_action(action:CreatureAction,_condition):
	action.block()
