class_name StatisticsBuffCondition
extends CreatureConditionDesign

@export var buffs 		:Array[BattleStatisticBuff]
func configure(condition:CreatureCondition,_creature:Creature):
	super.configure(condition,_creature)
	condition.configuration = []
func attach(new_creature:Creature,condition:CreatureCondition):
	super.attach(new_creature,condition)
	for buff in buffs:
		new_creature.statistics.add_buff(buff)
		condition.configuration.append(buff)
func detach(creature:Creature,_condition:CreatureCondition):
	for buff in buffs:
		creature.statistics.remove_buff(buff)
