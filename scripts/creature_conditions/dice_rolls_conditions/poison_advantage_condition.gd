class_name PoisonAdvantageCondition
extends CreatureConditionDesign


func affect_roll(_dice_roll:DiceRoll,_condition :CreatureCondition):
	if _dice_roll is SavingThrow:
		var against = _dice_roll.against
		if against is PoisonedConditionDesign:
			_dice_roll.add_advantage()
		elif against is CreatureCondition and against.design is PoisonedConditionDesign:
			_dice_roll.add_advantage()
		elif against is CompositeCondition and against.sub_conditions.any(func(c):return c is PoisonedConditionDesign):
			_dice_roll.add_advantage()
