class_name CreatureCondition
extends Node
#Unique generated id used by the system to remotely keep track of a condition
var instance_id				:GameUniqueId
var design					:CreatureConditionDesign
var creature				:Creature
var turn_count				:int	=-1
var description				:String
var state					:Dictionary

var _attached				:bool
# design specific configuration
var configuration			

signal on_detach(CreatureCondition)

func _ready():
	if not _attached and creature:
		creature.attach_condition.call_deferred(self)
	name = "%s-%s"%[design.name,instance_id.id]
func attach(c:Creature):
	design.configure(self,c)
	creature = c
	attach_turn_delay()
	design.attach(c,self)
	_attached = true
func detach(c:Creature):
	design.detach(c,self)
	if not creature == c:
		push_error("Condition not attached to creature %s"%[c.game_name])
		return
	creature = c
	detach_turn_delay()
	_attached = false
	on_detach.emit(self)
	
func attach_turn_delay():
	creature.on_turn_start.connect(handle_turn_start)
	creature.on_turn_end.connect(handle_turn_end)
func detach_turn_delay():
	if creature.on_turn_start.is_connected(handle_turn_start):
		creature.on_turn_start.disconnect(handle_turn_start)
		creature.on_turn_end.disconnect(handle_turn_end)
		
func handle_turn_start(_c:Creature):
	turn_count -=1
	design.handle_turn_start(self)	
func handle_turn_end(_c:Creature):
	design.handle_turn_end(self)
	if turn_count==0:
		detach(creature)
	

func affect_roll(dice_roll:DiceRoll):
	design.affect_roll(dice_roll,self)
func affect_difficulty_class(_dc:DifficultyClass):
	design.affect_difficulty_class(_dc,self)
func affect_damage(_damage:Damage):
	design.affect_damage(_damage,self )
func affect_action(action:CreatureAction):
	design.affect_action(action,self)
func extend_by(_condition:CreatureCondition):
	self.turn_count += _condition.turn_count

func get_class_save_id():
	return "CreatureCondition"

func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
	"instance_id":saver.to_save(instance_id),
	"design":saver.to_save(design),
	"creature":saver.to_save(creature),
	"turn_count":turn_count,
	"description":description,
	"state"		:saver.to_save(state)
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	instance_id = saver.from_save(save["instance_id"])
	design  = saver.from_save(save["design"])
	creature  = saver.from_save(save["creature"])
	turn_count = save["turn_count"]
	description = save["description"]
	if "state" in save:
		state = saver.from_save(save["state"])
	BaseResourceService.register_resource(self,instance_id)
