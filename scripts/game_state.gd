class_name GameState
extends Node
# Represent a global game state, from the involved creature, to the teams, 
# to the battle if one is in progress

@export var creatures:Node = Node.new()
@export var teams	:Node = Node.new()
var battle	:MultiplayerBattle = null
var event_log :Array[GameEvent]
var last_event_id :int=0

func _ready():
	if creatures.get_parent()==null:
		add_child(creatures)
	creatures.name = "Creatures"
	if teams.get_parent()==null:
		add_child(teams)
	teams.name = "Teams"
@rpc("any_peer","reliable","call_local")
func add_creature(creature_data:String):
	var creature = SI.p(creature_data)
	creature.name = creature.game_name
	creatures.add_child(creature)

func get_class_save_id()->String:
	return "GameState"
func to_save(saver:GameElementSaver)->Dictionary:
	return {
		"creatures":creatures.get_children().map(func (c):return saver.to_save(c)),
		"teams":	teams.get_children().map(func(c):return saver.to_save(c)),
		"battle"	:saver.to_save(battle),
		"event_log"	:saver.to_save(event_log),
		"last_event_id":last_event_id
	}
func from_save(data:Dictionary,saver:GameElementSaver):
	for child in creatures.get_children():
		creatures.remove_child(child)
		child.queue_free()
	for child in teams.get_children():
		teams.remove_child(child)
		child.queue_free()
	for creature_data in data["creatures"]:
		var creature = saver.from_save(creature_data)
		creature.name = creature.game_name
		creatures.add_child(creature)
		
	for team_data in data["teams"]:
		teams.add_child(saver.from_save(team_data))
	battle = saver.from_save(data["battle"])
	if "event_log" in data:
		event_log = []
		event_log.append_array(saver.from_save(data["event_log"]))
		last_event_id	= data["last_event_id"]
func register_battle(new_battle:MultiplayerBattle):
	battle = new_battle
