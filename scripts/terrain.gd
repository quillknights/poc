extends TileMap
class_name Terrain

var astar : AStar2D = AStar2D.new()
var tiles:Array[Vector2i]:set=_set_tiles
var minis:Array[CreatureMini] = []
var creature_positions:Dictionary= {}
@export var effects :Node
@export var objects :Node2D
@export var spawn_points	:Array[Vector2]

var spell_effect_client_visuals: Dictionary = {}

@onready var minis_views = $Minis
@onready var overlays = $Overlays
@onready var effect_shapes = $EffectShapes
@onready var spell_layer = $SpellLayer


func _ready():
	HexCoordTool.TILE_PIXEL_HEIGHT 	=325
	HexCoordTool.TILE_PIXEL_WIDTH	=297
	GameEventService.on_game_event.connect(handle_game_event)
	init_from_map()
	
func init_from_map():
	var hex_tiles:Array[Vector2i] = []
	for tile in get_used_cells(0):
		hex_tiles.append(HexCoordTool.oddr_to_axial(tile))
		
	_set_tiles(hex_tiles)
	for object in objects.get_children():
		if object is TerrainObject:
			object.mouvement.on_changed_position.connect(func(_m):update_object_position(object))
			object.on_destroy.connect(func(o):astar.set_point_disabled(get_tile_id(o.hex_coord),false))
			update_object_position(object)
			
func update_object_position(object:TerrainObject):
	if object.hex_coord == Vector2.INF:
		var hex_position = HexCoordTool.hex_round(HexCoordTool.pixel_to_hex(object.position))
		object.hex_coord=hex_position
	else:
		object.position = HexCoordTool.hex_to_pixel(object.hex_coord)
	if get_tile_id(object.hex_coord) != -1:
		astar.set_point_disabled(get_tile_id(object.hex_coord),true)
	else:
		print(object.hex_coord)
		
@warning_ignore("shadowed_global_identifier")
func add_mini(creature_mini:CreatureMini):
	minis_views.add_child(creature_mini)
	if creature_mini.creature.hex_coord ==Vector2.INF:
		place_creature(creature_mini.creature)
	creature_mini.position = HexCoordTool.hex_to_pixel(creature_mini.creature.hex_coord)
	creature_mini.visible=true
	creature_mini.terrain_container=minis_views
	var spawn = GameEventService.get_tween()
	spawn.tween_property(creature_mini,"scale",Vector2.ONE,0.5).set_trans(Tween.TRANS_BOUNCE)
	spawn.play()
	creature_mini.scale=Vector2.ONE*1.5
	astar.set_point_disabled(get_tile_id(creature_mini.creature.hex_coord),true)
	minis.append(creature_mini)
	creature_positions[creature_mini.creature] = creature_mini.creature.hex_coord
	creature_mini.mouvement.on_path_step.connect(handle_mini_stepped)
	creature_mini.creature.on_death.connect(handle_creature_died)
	return spawn.finished
func place_creature(creature:Creature):
	for point in spawn_points:
		if not is_occupied(point):
			creature.hex_coord = point
			return
	
func is_occupied(coord:Vector2):
	for object in objects.get_children():
		if HexCoordTool.pixel_to_hex(object.position) == coord:
			return true
	for creature_mini in minis:
		if creature_mini.creature.hex_coord == coord:
			return true
	return false
func add_spell_visual(visual:Node2D):
	spell_layer.add_child(visual)
	visual.name = visual.visual_instance_id.id
@rpc("any_peer","call_remote","reliable")
func add_spell_effect_client_visual(visual_unique_id:String,visual_configuration:Dictionary):
	var effect = BaseResourceService.get_resource(visual_configuration["effect"])
	var visual = effect.visual.instantiate()
	visual.from_config(visual_configuration)
	visual.visual_instance_id=visual_unique_id
	add_spell_visual(visual)
	spell_effect_client_visuals[visual_unique_id] = visual
	
@rpc("any_peer","call_remote","reliable")
func dissmiss_spell_effect_client_visual(visual_unique_id:String):
	var visual = spell_effect_client_visuals[visual_unique_id]
	visual.get_parent().remove_child(visual)
	visual.queue_free()
func update_tiles():
	astar.clear()
	for tile in tiles:
		if ( get_cell_source_id(0,HexCoordTool.axial_to_oddr(tile))==0):
			astar.add_point(get_tile_id(tile),HexCoordTool.hex_to_pixel(tile))
	for tile in tiles:
		var tile_id = get_tile_id(tile)
		if not astar.has_point(tile_id):
			continue
		for n in HexCoordTool.neightbors(tile):
			var n_id = get_tile_id(n)
			
			if n_id!=-1 and astar.has_point(n_id) and get_cell_source_id(0,HexCoordTool.axial_to_oddr(tile))==0:
				astar.connect_points(tile_id, n_id)
				
func add_overlay(overlay:Node2D):
	overlays.add_child(overlay)
func add_effect(effect:TerrainEffect):
	effect.terrain = self
	effects.add_child(effect)
	
func add_effect_shape(effect_view):
	if not is_node_ready():
		await ready
	effect_shapes.add_child(effect_view)
	effect_view.position = HexCoordTool.hex_to_pixel(effect_view.center)
func find_path(start:Vector2,end:Vector2)->Array[Vector2]:
	var result:Array[Vector2] = []
	var start_id = get_tile_id(start)
	var end_id = get_tile_id(end)
	if start_id==-1 or end_id==-1:
		return []
	if not (astar.has_point(start_id) and astar.has_point(end_id)):
		return []
	result.append_array(astar.get_point_path(start_id,end_id))
	return result
@warning_ignore("shadowed_global_identifier")
func handle_mini_stepped(mouvement:TerrainMouvement):
	var creature = mouvement.terrain_item.creature
	var new_position = HexCoordTool.hex_round(HexCoordTool.pixel_to_hex(mouvement.terrain_item.position))
	var old_position = creature_positions[creature]
	creature_positions[creature] = new_position
	astar.set_point_disabled(get_tile_id(new_position),true)
	astar.set_point_disabled(get_tile_id(old_position),false)
func handle_creature_died(creature):
	astar.set_point_disabled(get_tile_id(creature.hex_coord),false)
	
func handle_game_event(_gameEvent):
	pass

#============Getters=============
func get_tile_id(tile:Vector2i)->int:
	return tiles.find(tile)

func _set_tiles(value:Array[Vector2i]):
	tiles = value
	update_tiles()
func get_minis_in_range(center:Vector2,target_range:int)->Array[CreatureMini]:
	center = HexCoordTool.hex_round(center)
	var in_range:Array[CreatureMini] = []
	for m in minis:
		if HexCoordTool.distance(m.creature.hex_coord,center) <= target_range:
			in_range.append(m)
	return in_range
		


func _on_area_2d_mouse_entered():
	pass


func get_class_save_id()->String:
	return "Terrain"
func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"effects":saver.to_save(effects.get_children()),
		"objects":saver.to_save(objects.get_children()),
		"spawn_points":saver.to_save(spawn_points),
	}
	return data

func from_save(save:Dictionary,saver:GameElementSaver):
	for child in effects.get_children():
		effects.remove_child(child)
		child.queue_free()
	for effect in saver.from_save(save["effects"]):
		effects.add_child(effect)
	if "objects" in save:
		for child in objects.get_children():
			objects.remove_child(child)
			child.queue_free()
		for object in saver.from_save(save["objects"]):
			if object != null:
				objects.add_child(object)
	if "spawn_points" in save:
		spawn_points = []
		spawn_points.assign(saver.from_save(save["spawn_points"]))
