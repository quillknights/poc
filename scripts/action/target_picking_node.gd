class_name TargetPickingNode
extends Node

signal  on_target_clicked(Variant)
signal on_target_hovered(Variant)
signal on_target_unhovered(Variant)

var targetable_parent : Node

func _ready():
	targetable_parent = get_parent()
	if not InterfaceCheckTool.check_targetable(targetable_parent):
		get_parent().remove_child(self)
		queue_free()
		return
	targetable_parent.highlightable(true)
	var target_area:Area2D = targetable_parent.get_node("TargetArea")
	target_area.mouse_entered.connect(handle_mouse_entered)
	target_area.mouse_exited.connect(handle_mouse_exited)
	target_area.input_event.connect(handle_input_event)

func handle_mouse_entered():
	targetable_parent.highlight(true)
	on_target_hovered.emit(targetable_parent)
func handle_mouse_exited():
	targetable_parent.highlight(false)
	on_target_unhovered.emit(targetable_parent)
func handle_input_event(_vp,input_event:InputEvent,_shp):
	if input_event is InputEventMouseButton and not input_event.is_pressed():
		on_target_clicked.emit(targetable_parent)
func _exit_tree():
	targetable_parent.highlightable(false)
	
