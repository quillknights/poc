class_name CreatureAction
extends Node

signal on_acted(CreatureAction)
signal on_acting(CreatureAction)

var design	:CreatureActionDesign :set=_set_configuration
var actor			:Creature			:set=_set_actor
# Current Cost in battle resource
var cost 			:Array[BattleResource]
var blocked			:bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func is_affordable(creature:Creature) -> bool:
	var cost_counters = {}
	for r in design.cost:
		if not r in cost_counters:
			cost_counters[r]=0
		cost_counters[r]+=1
	for r in cost_counters.keys():
		if creature.battle_resources.get_resource(r)<cost_counters[r]:
			return false
	return true
@rpc("any_peer","call_local","reliable")
func act():
	if not MultiplayerService.is_server():
		push_error("Actions should not execute on clients !!!!")
		return
	acting.rpc()
	
	actor.apply_conditions(self)	
	if not is_affordable(actor):
		push_error("%s cannot afford this action"%[actor.game_name])
		return
	if blocked:
		blocked= false
		return
	cost = design.cost.duplicate()
@rpc("authority","call_local","reliable")
func acting():
	on_acting.emit(self)
@rpc("any_peer","call_local","reliable")
func acted():
	on_acted.emit(self)
func can_select()->bool:
	if not is_affordable(actor):
		print("%s cannot afford this action"%[actor.game_name])
		return false
	return true
func select()->bool:
	if not can_select():
		return false
	return true 
func deselect()->void:
	pass
func get_game_event()->GameEvent:
	push_error("Creature Action should return a game event")
	return null
func reset()->void:
	pass
func block():
	blocked = true
func _set_actor(value:Creature):
	actor = value
func spend_cost():
	_spend_cost.rpc(cost.map(func(r):return r.resource_path))
@rpc("authority","call_local","reliable")	
func _spend_cost(cost_id:Array):
	for id in cost_id:
		actor.battle_resources.spend_resource(BaseResourceService.get_resource(id))
func _set_configuration(value:CreatureActionDesign):
	design = value
func accept_condition(condition:CreatureCondition):
	condition.affect_action(self)
