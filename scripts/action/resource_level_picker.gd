class_name ResourceLevelPicker
extends Node2D
const RESOURCE_ICON_VIEW = preload("res://scenes/resource_icon_view.tscn")
@export var RESOURCE_OFFSET	=150
@export var levels			:Array[BattleResource]
@export var action			:CreatureAction
@export var tooltip			:String
@export var current_level	:BattleResource
@onready var action_icon = $ActionIcon
@onready var tooltip_label = $Tooltip
@onready var resource_levels = $ResourceLevels
@onready var back = $Back

signal on_level_picked(BattleResource)

func _ready():
	back.pressed.connect(
		func():
			get_parent().remove_child(self)
			self.queue_free())
	action_icon.texture = action.design.icon
	tooltip_label.text = tooltip
	for level in levels:
		var view = RESOURCE_ICON_VIEW.instantiate()
		view.resource = level
		view.amount = 1
		view.depletted=0
		view.togglable = true
		
		view.position = Vector2(-300+RESOURCE_OFFSET*resource_levels.get_child_count(),0)
		resource_levels.add_child(view)
		if level == current_level:
			view.toggle(true)
		view.on_toggle.connect(handle_level_toggled)
func handle_level_toggled(_resource_view:BattleResourceIconView,resource:BattleResource,state:bool):
	if state : 
		for view in resource_levels.get_children():
			if view !=_resource_view:
				view.toggle(false)
		on_level_picked.emit(resource)
		
