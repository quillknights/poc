extends CreatureAction
class_name Move

var target_path:Array[Array]=[]
var start_position:Vector2=Vector2.INF

@rpc("any_peer","call_local","reliable")
func set_path(path:Array):
	target_path = []
	for element in path:
		target_path.append(element)

func is_affordable(creature:Creature):
	var mouvement = creature.battle_resources.get_resource(CreatureBattleResources.MOUVEMENT)
	return mouvement>=1 and target_path.size()-1<=mouvement
@rpc("any_peer","call_local","reliable")
func act():
	super.act()
	GameEventService.enter_event()
	start_position = actor.hex_coord
	actor.move_along_path.rpc(target_path)
	actor.on_moved.connect(handle_move_done)
	
func handle_move_done(_creature):
	actor.update_hex_coord.rpc(HexCoordTool.hex_round(HexCoordTool.pixel_to_hex(actor.creature_mini.position)))
	actor.on_moved.disconnect(handle_move_done)
	var game_event = get_game_event()
	acted.rpc()
	GameEventService.leave_event(game_event)
	

func get_game_event()->GameEvent:
	var event = GameEvent.new()
	event.type = GameEvent.Type.Move
	event.sentence	= "%s moved %d tiles"%[actor.game_name,target_path.size()-1]
	event.payload ={
		"start_position":start_position,
		"path":target_path
	}
	return event
