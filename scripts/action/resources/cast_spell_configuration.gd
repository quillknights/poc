class_name CastSpellConfiguration
extends CreatureActionDesign

@export var spell	:Spell	:set=_set_spell



func _set_spell(value:Spell):
	spell = value
	name="Cast Spell %s"%[spell.name]
	icon = spell.icon
func _get_configurator()->PackedScene:
	if spell.configurator:
		return spell.configurator
	return configurator
