extends Resource
class_name CreatureActionDesign
@export_group("Interface")
@export var icon:			Texture2D
@export var configurator	:PackedScene:get=_get_configurator
@export var name			:String
@export var in_action_bar	:bool 			=	 true
@export_group("Behavior")
@export var cost			:Array[BattleResource]
@export var action			:GDScript		= preload("res://scripts/action/creature_action.gd")
func _get_configurator()->PackedScene:
	return configurator
