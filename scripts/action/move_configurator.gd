extends ActionConfigurator
@onready var range_indicator = $RangeInterface/RangeIndicator
@onready var range_interface = $RangeInterface
@onready var range_shape = $RangeInterface/RangeShape
@onready var path_indicator:Line2D = $PathGroup/PathIndicator
@onready var path_group = $PathGroup
@onready var path_trace = $PathGroup/PathTrace

var _is_mouse_hovering=false
var move_range:float
var center:Vector2
var target:Vector2
func _ready():
	if not action:
		push_error("Configurator must have action set before ready")
		detach()
	range_interface.mouse_entered.connect(handle_mouse_entered)
	range_interface.input_event.connect(handle_input_event)
	if action == null:
		return 
	move_range = HexCoordTool.hex_distance_to_pixel(action.actor.battle_resources.get_resource(CreatureBattleResources.MOUVEMENT))+HexCoordTool.TILE_PIXEL_HEIGHT*0.10
	center = HexCoordTool.hex_to_pixel(action.actor.hex_coord)
	
	init_range_circle()
func handle_mouse_entered():
	_is_mouse_hovering=true
func handle_mouse_exited():
	_is_mouse_hovering=false
func handle_input_event(_vp,event:InputEvent,_p):
	if _is_mouse_hovering:
		var terrain = ResourceService.get_battle().terrain		
		if event is InputEventMouseMotion:
			update_displayed_path(HexCoordTool.hex_round(HexCoordTool.pixel_to_hex(terrain.to_local(event.position))))
		if event is InputEventMouseButton:
			if event.button_index==MOUSE_BUTTON_LEFT and event.is_pressed():
				if 	ResourceService.get_battle().terrain.find_path(action.actor.hex_coord,target):
					pick_target(HexCoordTool.hex_round(HexCoordTool.pixel_to_hex(terrain.to_local(event.position))))
func update_displayed_path(to_hex:Vector2):
	var battle:MultiplayerBattle = ResourceService.get_battle()
	target = HexCoordTool.hex_round(to_hex)
	var path = battle.terrain.find_path(action.actor.hex_coord,target)
	path_trace.curve = make_smooth_curve(path)
	path_indicator.clear_points()
	for point in path_trace.curve.tessellate():
		path_indicator.add_point(point)
	if path_group.get_parent()==self:
		remove_child(path_group)
		battle.terrain.add_overlay(path_group)
func pick_target(target_hex:Vector2):
	target = HexCoordTool.hex_to_pixel(target_hex)
	var path = ShapeTools.make_smooth_path(ResourceService.get_battle().terrain.find_path(action.actor.hex_coord,HexCoordTool.hex_round(HexCoordTool.pixel_to_hex(target))))
	action.set_path.rpc_id(1,path)
	on_configured.emit()
func init_range_circle():
	remove_child(range_interface)
	ResourceService.get_battle().terrain.add_overlay(range_interface)
	range_interface.position = action.actor.creature_mini.position
	var points = generate_circle_polygon(move_range,1000,Vector2.ZERO)	
	range_indicator.polygon = points

	var shape = CircleShape2D.new()
	shape.radius = move_range
	range_shape.shape = shape
	
func detach():
	range_interface.get_parent().remove_child(range_interface)
	range_interface.queue_free()
	path_group.get_parent().remove_child(path_group)
	path_group.queue_free()
	get_parent().remove_child(self)
	queue_free()
func make_smooth_curve(points:Array[Vector2])->Curve2D:
	var curve = Curve2D.new()
	for step in ShapeTools.make_smooth_path(points):
		if step.size()==1:
			curve.add_point(step[0])
		else:
			curve.add_point(step[0],step[1],step[2])
	return curve


func generate_circle_polygon(radius: float, num_sides: int, circle_center: Vector2) -> PackedVector2Array:
	var angle_delta: float = (PI * 2) / num_sides
	var vector: Vector2 = Vector2(radius, 0)
	var polygon= PackedVector2Array()

	for _i in num_sides:
		polygon.append(vector + circle_center)
		vector = vector.rotated(angle_delta)

	return polygon
