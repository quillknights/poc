class_name TargetPicker
extends Node

var targets: Array[Node]
var target_picking_nodes =[]
signal on_target_picked(Variant)
signal on_target_hovered(Variant)
signal on_target_unhovered(Variant)


func _ready():
	for target in targets:
		target.add_child(get_picking_node(target))
func get_picking_node(_target:Variant) -> TargetPickingNode:
	var node = TargetPickingNode.new()
	node.on_target_clicked.connect(handle_target_clicked)
	node.on_target_hovered.connect(handle_target_hovered)
	node.on_target_unhovered.connect(handle_target_unhovered)
	target_picking_nodes.append(node)
	return node
	
func handle_target_clicked(target:Variant):
	on_target_picked.emit(target)
func handle_target_hovered(target:Variant):
	on_target_hovered.emit(target)
func handle_target_unhovered(target:Variant):
	on_target_unhovered.emit(target)

func _exit_tree():
	for picking in target_picking_nodes:
		picking.get_parent().remove_child(picking)
		picking.queue_free()
