extends ActionConfigurator
@onready var range_click_detector = $TerrainOverlay/RangeClickDetector
@onready var range_shape = $TerrainOverlay/RangeClickDetector/RangeShape
@onready var terrain_overlay = $TerrainOverlay
var target_picker			:TargetPicker
var attack_range			:int
var center					:Vector2
var target_location			:Vector2 = Vector2.INF
var target					#targetable variant


func _ready():
	if not action:
		push_error("Configurator must have action set before ready")
		detach()
	attack_range = action.actor.attack_range
	center = action.actor.hex_coord
	init_target_selection()
	range_click_detector.input_event.connect(handle_range_detector_input_event)
	
func init_target_selection():
	range_shape.shape.radius=HexCoordTool.hex_distance_to_pixel(attack_range)
	remove_child(terrain_overlay)
	ResourceService.get_battle().terrain.add_overlay(terrain_overlay)
	range_click_detector.transform.origin =HexCoordTool.hex_to_pixel(center)
	var targetables:Array[Node] = []
	target_picker = TargetPicker.new()
	for targetable in get_tree().get_nodes_in_group("targetables"):
		if targetable == action.actor.creature_mini:
			continue
		if HexCoordTool.distance(targetable.hex_coord,center) <= attack_range:
			targetables.append(targetable)
	target_picker.targets = targetables
	target_picker.on_target_hovered.connect(handle_target_hovered)
	target_picker.on_target_unhovered.connect(handle_target_unhovered)
	target_picker.on_target_picked.connect(handle_target_picked)
	add_child(target_picker)
	
func handle_target_picked(picked):
	if InterfaceCheckTool.is_hittable(picked):
		target=picked
	elif InterfaceCheckTool.is_proxy(picked) and InterfaceCheckTool.is_hittable(picked.proxy_for):
		target = picked.proxy_for
	else:
		return
	configured()
func handle_target_hovered(_target):
	pass
	
func configured():
	action.set_target.rpc_id(1,SI.s(target.unique_id))
	on_configured.emit()
func handle_range_detector_input_event(_vp,event:InputEvent,_i):
	if event is InputEventMouseButton and event.is_pressed() and event.button_index==MOUSE_BUTTON_LEFT:
		target_location=get_global_mouse_position()
		configured()

func handle_target_unhovered(_target):
	pass
	#GameEventService.on_game_event_considered.emit(null)
	
func detach():
	if terrain_overlay.get_parent():
		terrain_overlay.get_parent().remove_child(terrain_overlay)
	terrain_overlay.queue_free()
	if get_parent():
		get_parent().remove_child(self)	
	queue_free()

