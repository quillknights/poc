class_name CastSpell
extends CreatureAction

var terrain	:Terrain
var _acting	:bool=false
var target_creatures	:Array[Creature]
var targets				:Array[Node] # 
var target_positions	:Array[Vector2]
var casting_level		:int =-1
var casting_ability		:CreatureBattleStatistics.Type = CreatureBattleStatistics.Type.NONE
var effect_configuration
var target_count		:int=-1
var additional_targets	:int=0
var spell_state	:Dictionary = {}

func is_affordable(creature:Creature) -> bool:
	if super.is_affordable(creature):
		if design.level==0:
			return true
		var scaling = design.scaling
		if not design.scaling:
			return creature.battle_resources.has_unspent_slot_level(design.level)
		for i in range(scaling.floor_level,scaling.ceil_level):
			if creature.battle_resources.has_unspent_slot_level(i):
				return true
	return false
func act():
	super.act()
	GameEventService.enter_event()
	terrain =ResourceService.get_battle().terrain
	_acting = true
	design.cast(terrain,self)
	if effect_configuration.require_concentration:
		actor.concentrate(self)
func spell_casted(_s:Spell):
	if _acting:
		spend_cost()
		await GameEventService.leave_event(get_game_event())		
		on_acted.emit(self)
		_acting = false
@rpc("any_peer","call_local","reliable")
func update_configuration():
	effect_configuration = design.effect.get_configuration()
	design.configure(self)	
	if design.scaling:
		design.scaling.apply(self,effect_configuration,casting_level)
	
func select():
	if super.select():
		update_configuration()
		return true
	return false
@rpc("any_peer","call_local","reliable")
func set_target_creatures(ids:Array):
	target_creatures = []
	for id in ids:
		target_creatures.append(BaseResourceService.get_resource(id))
@rpc("any_peer","call_local","reliable")
func set_target_locations(positions:Array):
	target_positions = []
	for pos in positions:
		target_positions.append(pos)
@rpc("any_peer","call_local","reliable")
func set_targets(target_ids:Array):
	if len(target_ids)>get_total_target_count():
		push_error("To many targets")
		return
	targets = []
	for id in target_ids:
		var target = BaseResourceService.get_resource(id)
		if not target is Node or not InterfaceCheckTool.check_targetable(target):
			push_error("target with id %s is not targetable"%[id])
			continue
		targets.append(target)
@rpc("any_peer","call_local","reliable")
func set_casting_level(level:int):
	casting_level = level
func _set_configuration(value:CreatureActionDesign):
	design = value
	casting_level = design.level
	update_configuration()
	
	
func get_game_event():
	var event = GameEvent.new()
	event.type = GameEvent.Type.SpellCasted
	event.sentence = "%s casted %s "%[actor.game_name,design.name]

	if target_creatures:
		event.sentence = "%s at %s"%[event.sentence,target_creatures[0].game_name]
		
	return event
func spend_cost():
	super.spend_cost()
	if casting_level!=0:
		_spend_cost.rpc([Spell.SPELL_SLOTS[casting_level-1].resource_path])
func configure_concentration(concentration:Concentration):
	design.configure_concentration(concentration,self)
func get_total_target_count():
	if target_count==-1:
		return -1
	return target_count+additional_targets
