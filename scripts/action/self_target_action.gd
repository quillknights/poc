class_name SelfTargetAction
extends CreatureAction


func act() -> void :
	super.act()
	GameEventService.enter_event()
	for effect in design.effects:
		var _effect_result = await effect.apply(actor)
	spend_cost()
	GameEventService.leave_event(get_game_event())
	on_acted.emit(self)
func get_game_event() -> GameEvent:
	if not design.event_type:
		return null 
	var event = GameEvent.new()
	event.type = design.event_type
	event.sentence	= "%s used %s"%[actor.game_name,name]
	return event
