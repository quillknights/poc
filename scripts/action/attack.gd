extends CreatureAction
class_name Attack
var target	=null#Targetable
var target_location:Vector2=Vector2.INF
var is_successfull=false
var ac:DifficultyClass
var roll:DiceRoll
var damage_roll:DiceRoll
var used_weapon:Weapon
var attack_range:
	get:
		return actor.attack_range
func is_affordable(creature:Creature) -> bool:
	return creature.battle_resources.action>0

func act():
	super.act()
	GameEventService.enter_event()
	if InterfaceCheckTool.is_hittable(target):
		attack(target)	
	elif InterfaceCheckTool.is_proxy(target) and InterfaceCheckTool.is_hittable(target.proxy_for):
		attack(target.proxy_for)
	elif target_location!=Vector2.INF:
		attack_location(target_location)
	else:
		push_error("Can't attack this")
		return
	await GameEventService.leave_event(get_game_event())	
	on_acted.emit(self)
func can_attack_creature(potential:Creature)->bool:
	return attack_range>=HexCoordTool.distance(potential.hex_coord,actor.hex_coord)
func attack(on):
	if not attack_range>=HexCoordTool.distance(on.hex_coord,actor.hex_coord):
		return
	ac = target.get_ac()
	roll = get_attack_roll(on)
	used_weapon = actor.weapon
	roll.roll(ac)
	GameEventService.register_child(roll.get_game_event())			
	if roll.result >=ac.value:
		actor.attack.rpc(SI.s(on.unique_id),roll.result,ac.value)
		var damages = actor.weapon.get_damages(on)
		for damage in damages:
			on.take_damage(damage)
			GameEventService.register_child(damage.get_game_event())
		is_successfull = true
	else:
		actor.fail_attack.rpc(on.hex_coord)
		is_successfull=false
	spend_cost()
func attack_location(_target_location):
	actor.animate_failed_attack(_target_location)
	actor.spend_action()
	
func get_game_event()->GameEvent:
	var event = GameEvent.new()
	event.type = GameEvent.Type.Attack
	event.sentence	=  "%s attacked %s "%[actor.game_name,target.game_name]
	
	event.payload = {
		
	}
	return event
func get_attack_roll(on)->DiceRoll:
	if roll:
		return roll
	var temp_roll= actor.get_attack_roll(on)
	if on:
		on.apply_conditions(temp_roll)
	return temp_roll
func reset()->void:
	target_location=Vector2.INF
	target=null
	is_successfull=false
	ac=null
	roll=null
	damage_roll = null 
func _set_actor(value:Creature):
	super._set_actor(value)
@rpc("call_local","any_peer","reliable")
func set_target(target_id_data:String):
	var target_id = SI.p(target_id_data)
	if not target_id:# nothing was parsed, the id is a pure string
		target_id = target_id_data
	target = BaseResourceService.get_resource(target_id)
