class_name OpportunityAttack
extends Attack

func is_affordable(creature:Creature) -> bool:
	return creature.battle_resources.reaction>0
func get_game_event()->GameEvent:
	var event = super.get_game_event()
	event.type = GameEvent.Type.OpportunityAttack
	event.sentence = "%s took and opportunity attack at %s"%[actor.game_name,target.game_name]
	return event

	
