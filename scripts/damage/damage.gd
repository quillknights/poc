class_name Damage
extends Object

var base_value	:int
var type		:DamageType
var value 		:int:get=_get_value
var modifiers	:Array[int] = []
var vulnerability	:bool
var resistance		:bool
var immunity		:bool
var target			:Variant
func accept_condition(condition:CreatureCondition):
	condition.affect_damage(self)

func _get_value()->int:
	if immunity:
		return 0
	var final_value = base_value
	if modifiers:
		final_value = final_value + modifiers.reduce(func (a,b):return a+b)
	if resistance:
		final_value=floor(final_value/2)
	if vulnerability:
		final_value=final_value*2
	
	return final_value
func get_game_event()->GameEvent:
	var event = GameEvent.new()
	event.type = GameEvent.Type.Damage
	event.sentence = "%s took %d %s damage "%[target.game_name,value,type.name]
	return event

func get_class_save_id()->String:
	return "Damage"
	

func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"base_value":base_value,
		"type":saver.to_save(type),
		"value":value,
		"modifiers":saver.to_save(modifiers),
		"vulnerability":vulnerability,
		"resistance":resistance,
		"immunity":immunity,
		"target":saver.to_save(target)
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	base_value = save["base_value"]
	type  = saver.from_save(save["type"])
	value = save["value"]

	var new_modifiers :Array[int] = []
	new_modifiers.append_array(saver.from_save(save["modifiers"]))
	modifiers = new_modifiers
	vulnerability = save["vulnerability"]
	resistance = save["resistance"]
	immunity = save["immunity"]
	target  = saver.from_save(save["target"])

	
