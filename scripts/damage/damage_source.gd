class_name DamageSource
extends Resource

@export var dices		:Array[Dice]
@export var flat_value	:int
@export var type		:DamageType


func get_damages(source,target,_context) -> Damage:
	var amount = get_flat_value(_context)
	for dice in dices:
		var roll = _get_roll(dice,source,target,_context)
		target.apply_conditions(roll)
		amount += roll.roll()
		GameEventService.register_child(roll.get_game_event())
	var damage = _get_damage(amount,source,target,_context)
	return damage

func _get_roll(_dice:Dice,_source,_target,_context):
	push_error("Damage source is not supposed to be used raw, this method must be overiden by a child")
func _get_damage(amount:int,_source,target,_context):
	if not InterfaceCheckTool.is_damageable(target):
		return null
	return target.get_damage_taken(amount,type)
func get_flat_value(_context) -> int:
	return flat_value
	
func get_printable_str(context=null)->String:
	var dice_counts = {}
	for dice in dices:
		if not dice in dice_counts:
			dice_counts[dice]=0
		dice_counts[dice]+=1
	var result = ""
	if get_flat_value(context)!=0:
		result+="%d+"%[get_flat_value(context)]
	result+="+".join(dice_counts.keys().map(func(dice):return "%d%s"%[dice_counts[dice],dice.name]))
	return result
