class_name SpellDamageSource
extends DamageSource
func _get_roll(dice:Dice,source:Creature,target,context):
	return source.get_spell_damage_roll(target,dice,context)
