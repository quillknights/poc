class_name WeaponDamageSource
extends DamageSource
func _get_roll(dice:Dice,source:Creature,target,_context):
	return source.get_damage_roll(dice,target)
func get_flat_value(context)->int:
	if not context:
		return super.get_flat_value(context)
	return super.get_flat_value(context)+context.get_modifier()
