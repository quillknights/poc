class_name DamageType
extends Resource

enum Type{
	Acid,Bludgeoning,Cold,Fire,Force,Lightning,Necrotic,Piercing,Poison, Psychic,Radiant,Slashing,Thunder
}
@export var unique_id:String:set=_set_unique_id
@export var type:Type
@export var icon:Texture2D
@export var name:String

func _set_unique_id(value:String):
	unique_id = value
	unique_id = resource_path
