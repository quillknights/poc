extends Node2D
class_name Battle
const END_GAME = preload("res://scenes/end_game_screen.tscn")
const CREATURE_MINI = preload("res://scenes/creature_mini.tscn")
@export var TILE_PIXEL_HEIGHT:int
@export var turn_manager:TurnManager
@onready var dwarf = $Dwarf
@onready var goblin = $Goblin
@onready var goblin_2 = $Goblin2
@onready var initiative_bar:InitiativeBar = $InitiativeBar
@onready var action_bar = $ActionBar
@onready var terrain:Terrain = $terrain

signal on_initiative_roll()

@onready var creatures:Array[Creature] = [dwarf,goblin]


func _enter_tree():
	HexCoordTool.TILE_PIXEL_HEIGHT = TILE_PIXEL_HEIGHT
	
func _ready():
	CreaturesEventBus.on_creature_died.connect(handle_creature_death)
	start()
	
func start():
	var timer = Timer.new()
	timer.set_wait_time(1)
	add_child(timer)
	timer.start()
	await timer.timeout
	roll_initiative()
	
func roll_initiative():
	var init_table_roll = TableInitiativeRolls.new()
	init_table_roll.creatures = creatures
	add_child(init_table_roll)
	init_table_roll.on_done.connect(handle_init_rolled)	
	init_table_roll.roll()
func handle_init_rolled(init_rolled:TableInitiativeRolls):
	turn_manager.initiative = init_rolled.initiative_order
	
	remove_child(init_rolled)
	init_rolled.queue_free()
	on_initiative_roll.emit()
	await place_minis()
	#turn_manager.start_battle(turn_manager.initiative.map(func(c):return c.unique_id))
func handle_creature_death(_creature:Creature):
	var survivors =  creatures.filter(func (c):return not c.is_dead)
	if survivors.size()==1:
		game_over(survivors[0])
func game_over(winner):
	var end_game_screen = END_GAME.instantiate()
	end_game_screen.winner = winner
	end_game_screen.losers = creatures.filter(func(c):return c!=winner)
	add_child(end_game_screen)
	
func place_minis():
	for creature in turn_manager.initiative:
		var creature_mini = CREATURE_MINI.instantiate()
		creature_mini.creature = creature
		await terrain.add_mini(creature_mini)
	return null
