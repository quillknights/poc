class_name BaseResourceService
extends Node
static var resource_node_instanciated :bool = false

# A autoload node used to retrieve registered resources based on their Unique resource ID
static var _resources:Dictionary={}
#Should be called only once, by the resource itself
# Resources IDS should be unique and coherent from instance to instance
static func register_resource(resource,overide_id=null):
	var unique_id = overide_id
	if unique_id == null:
		unique_id = resource.unique_id
		
	_resources[unique_id] = resource	
static func get_resource(unique_id):
	if unique_id is String and ResourceLoader.exists(unique_id):
		return ResourceLoader.load(unique_id)
	assert(unique_id in _resources)
	return _resources[unique_id]
static func unregister_resource(resource_id):
	
	if resource_id in _resources:
		_resources.erase(resource_id)
func get_battle() -> MultiplayerBattle:
	var battle =  get_tree().current_scene
	if not battle is MultiplayerBattle:
		push_error("Node not in battle scene")
		return null
	return battle
