class_name GameUniqueId
extends Object

var id:String
func _init(p_id=null):
	if p_id !=null:
		id=p_id
	if MultiplayerService.id_exists(self):
		assert(false,"A Game id should not aready exist")
		id="INVALID ID"
func _to_string():
	return id
func get_class_save_id():
	return "GameUniqueId"
func _from_save(_save,_saver=null):
	assert(false, "A game unique id must be deserialized by the MultiplayerService to maintain the registry")
func to_save(_saver=null):
	return {"id":id}
