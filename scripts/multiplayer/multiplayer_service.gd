extends Node
signal on_creature_assignement(Creature)
static var next_id = 0
var _at_the_gate=0
signal _distributed_gate()
var player_peers 			:Dictionary	={}
var spectating_peers 		:Dictionary={}

var player_team			:Dictionary = {}
var unique_ids			:Dictionary	= {}
func is_spectator():
	return multiplayer.get_unique_id() in spectating_peers
func is_creature_ally(creature:Creature)->bool:
	if is_spectator():
		return true
	var peer_id = multiplayer.get_unique_id()
	if not peer_id:
		return false
	var creature_team = TeamService.get_creature_team(creature)
	var p_team = player_team[player_peers[peer_id]]
	return TeamService.are_allied(p_team,creature_team)
	
func is_my_creature(creature:Creature) -> bool:
	if is_spectator():
		return false
	var peer_id = multiplayer.get_unique_id()
	var creature_team = TeamService.get_creature_team(creature)
	var p_team = player_team[player_peers[peer_id]]
	return creature_team == p_team

@rpc("authority","reliable","call_local")
func register_player(peer_id,player_id):
	player_peers[peer_id]=player_id
@rpc("authority","reliable","call_local")
func register_spectator(peer_id,peer_name):
	spectating_peers[peer_id] = peer_name
@rpc("authority","reliable","call_local")
func assign_team(team_id,player_id):
	player_team[player_id]=team_id
func get_team():
	var peer_id = multiplayer.get_unique_id()
	if not player_peers.has(peer_id):
		return "Spectator"
	var player_id = player_peers[peer_id]
	return player_team[player_id]

func sync_clients(gate_id:String):
	if multiplayer.multiplayer_peer is OfflineMultiplayerPeer:
		return
	print("%d arriving at gate %s"%[multiplayer.get_unique_id(),gate_id])
	arrived_gate.rpc_id(1,gate_id)
	return await _distributed_gate
@rpc("any_peer","call_local","reliable")
func arrived_gate(_gate_id:String):
	if not multiplayer.is_server():
		return
	_at_the_gate+=1
	print("%d is %d at_the_gate "%[multiplayer.get_remote_sender_id(),_at_the_gate])		
	if _at_the_gate==multiplayer.get_peers().size()+1:
		_at_the_gate=0		
		open_gate.rpc()
		print("Opened gate %s"%[_gate_id])
@rpc("authority","call_local","reliable")
func open_gate():
	_distributed_gate.emit()
	

func is_server()->bool:
	return multiplayer.is_server()
func register_id(id:GameUniqueId):
	unique_ids[id.id]=id
func id_exists(id:GameUniqueId):
	return id.id in unique_ids
func get_custom_unique_id(custom_id:String):
	var id = GameUniqueId.new("Custom-%s"%[custom_id])
	if id_exists(id):
		return null
	register_id(id)
	return id
func generate_unique_id()->GameUniqueId:
	next_id +=1
	var peer_id = multiplayer.get_unique_id()
	var id_string = "%d-%d"%[peer_id,next_id]
	while id_string in unique_ids:
		next_id+=1
		id_string = "%d-%d"%[peer_id,next_id]
	var id = GameUniqueId.new(id_string)
	register_id(id)
	return id

func game_unique_id_from_save(save:Dictionary,_saver:GameElementSaver):
	if save["id"] in unique_ids:
		return unique_ids[save["id"]]
	var id = GameUniqueId.new(save["id"])
	register_id(id)
	return id
