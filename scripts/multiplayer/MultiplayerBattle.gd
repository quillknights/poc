class_name MultiplayerBattle
extends Node2D

const CREATURE_MINI = preload("res://scenes/creature_mini.tscn")
const END_GAME_SCREEN = preload("res://scenes/end_game_screen.tscn")
const CREATURE = preload("res://scenes/creature.tscn")
@export var IP_ADDRESS:String
@export var PORT:int
@export var MAX_CLIENTS:int
@export var AUTOSTART:bool=false
@export var turn_manager:TurnManager
@export var center_tile_effect:TerrainEffectDesign
@export var creatures:Array[Creature]
@export var terrain:Terrain
var is_started:bool=false


@onready var camera = $Camera2D
@onready var connection_status = $CanvasLayer/ConnectionStatus
@onready var ping_button = $CanvasLayer/Ping
@onready var ping_delay = $CanvasLayer/ping_delay
@onready var initiative_bar:InitiativeBar = $InitiativeBar
@onready var save_button = $CanvasLayer/SaveButton
@onready var action_bar = $ActionBar


var game_state:GameState:set=_set_game_state

func _process(_delta):
	if Input.is_action_pressed("keyboard_quit"):
		quit.rpc()
func _ready():
	action_bar.turn_manager = turn_manager
	save_button.pressed.connect(handle_save_pressed)
	ping_button.pressed.connect(send_ping)
	terrain.visible=false
	for creature in creatures:
		var w = creature.weapon
		if w:
			creature.weapon=null
			creature.equip_weapon(w)
	# Temp code to disable saving after a turn has started
	turn_manager.on_turn_start.connect(handle_turn_start)
	GameEventService.on_game_event.connect(func(_e):save_button.disabled = true)
	# End temp code
	
	if not is_started:
		center_tile_effect.attach(terrain,Vector2(2,2))
	if multiplayer.is_server():
		CreaturesEventBus.on_creature_died.connect(handle_creature_died)
func handle_turn_start(_c):
	save_button.disabled=false
	if OS.is_debug_build() and multiplayer.is_server():
		await GameEventService.tween_settled()
		print("autosaving")
		var save_string =GameElementSaver.to_save_string(game_state)
		GameElementSaver.write_save_string("res://dev_saves/autosave.save",save_string)
@rpc("any_peer")
func ping(ping_ts):
	pong.rpc_id(multiplayer.get_remote_sender_id(),Time.get_ticks_msec()-ping_ts)
@rpc()
func pong(delta):
	ping_delay.text = "%d ms"%[delta]
func handle_save_pressed():
	request_save.rpc_id(1)
@rpc("any_peer","reliable","call_local")
func request_save():
	var peer_id = multiplayer.get_remote_sender_id()
	var save_string = GameElementSaver.to_save_string(game_state)
	save_to_file.rpc_id(peer_id,save_string)
@rpc("any_peer","reliable","call_local")
func save_to_file(save_string:String):
	var file_dialog =FileDialog.new()
	file_dialog.add_filter("*.save")
	file_dialog.access = FileDialog.ACCESS_RESOURCES
	file_dialog.file_mode = FileDialog.FILE_MODE_SAVE_FILE
	file_dialog.min_size = Vector2(280,280)
	add_child(file_dialog)
	file_dialog.popup_centered()
	var file =await file_dialog.file_selected
	GameElementSaver.write_save_string(file,save_string)
func send_ping():
	var other_id = multiplayer.get_peers()[0]
	ping.rpc_id(other_id,Time.get_ticks_msec())
@rpc("authority","call_local","reliable")
func resume_battle():
	GameEventService.battle_running = true 	
	initiative_bar.battle=self
	if MultiplayerService.is_spectator():
		connection_status.text = "Spectator"
	else:
		connection_status.text = "Team %s"%[MultiplayerService.get_team()]
	place_minis()
	turn_manager.resume_battle()
@rpc("authority","call_local","reliable")
func start_battle():
	is_started=true
	GameEventService.battle_running = true 	
	initiative_bar.battle=self
	print("sincing")
	await MultiplayerService.sync_clients("MultiplayerBattle:StartBattle")
	
	if MultiplayerService.is_spectator():
		connection_status.text = "Spectator"
	else:
		connection_status.text = "Team %s"%[MultiplayerService.get_team()]
	if multiplayer.is_server():
		roll_initiative()
func roll_initiative():
	var init_table_roll = TableInitiativeRolls.new()
	init_table_roll.creatures = creatures
	add_child(init_table_roll)
	init_table_roll.on_done.connect(handle_init_table_roll_done)
	init_table_roll.roll()

func handle_init_table_roll_done(init_table_roll:TableInitiativeRolls):
	var init_ids:Array[GameUniqueId]=[]
	init_ids.assign(init_table_roll.initiative_order.map(func(c):return c.unique_id))
	turn_manager.start_battle.rpc(SI.s(init_ids))
	place_minis.rpc()
	remove_child(init_table_roll)
	init_table_roll.queue_free()
@rpc("authority","call_local","reliable")
func place_minis():
	terrain.visible=true
	for creature in turn_manager.initiative:
		var creature_mini = CREATURE_MINI.instantiate()
		creature_mini.creature = creature
		creature.creature_mini=creature_mini
		await terrain.add_mini(creature_mini)
	return null

func handle_creature_died(_creature:Creature):
	var survivors =  creatures.filter(func (c):return not c.is_dead)
	if survivors.size()==1:
		game_over.rpc(SI.s(survivors[0].unique_id))
@rpc("authority","call_local","reliable")
func game_over(winner_id_data:String):
	var winner_id = SI.p(winner_id_data)
	if GameEventService.is_event_running:
		await GameEventService.on_no_event_running
	var winner = BaseResourceService.get_resource(winner_id)
	var end_game_screen = END_GAME_SCREEN.instantiate()
	end_game_screen.position = camera.position
	end_game_screen.winner = winner
	end_game_screen.losers = creatures.filter(func(c):return c!=winner)
	add_child(end_game_screen)
@rpc("authority","call_local","reliable")
func restart_scene():
	get_tree().reload_current_scene()
@rpc("any_peer","call_local","reliable")
func quit():
	print("quiting")
	get_tree().quit()


func log_mouse_entered():
	print("mouse_enetered_connected")
func _set_game_state(value:GameState):
	game_state=value
	game_state.register_battle(self)





func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
		"turn_manager":saver.to_save(turn_manager),
		"center_tile_effect":saver.to_save(center_tile_effect),
		"creatures":saver.to_save(creatures),
		"is_started":is_started,
		"game_state":saver.to_save(game_state),
		"terrain":saver.to_save(terrain)
	}
	return data
func get_class_save_id()->String:
	return "MultiplayerBattle"
func from_save(save:Dictionary,saver:GameElementSaver):
	remove_child(turn_manager)
	turn_manager.queue_free()
	turn_manager  = saver.from_save(save["turn_manager"])
	add_child(turn_manager)
	center_tile_effect  = saver.from_save(save["center_tile_effect"])
	var new_creatures :Array[Creature] = []
	new_creatures.append_array(save["creatures"]["data"].map(func(d):return saver.from_save(d)))
	creatures = new_creatures
	is_started = save["is_started"]
	game_state = saver.from_save(save["game_state"])	
	var terrain_parent = terrain.get_parent()
	var	terrain_transform = terrain.get_transform()
	terrain_parent.remove_child(terrain)
	terrain.queue_free()
	terrain = saver.from_save(save["terrain"])
	terrain_parent.add_child(terrain)
	terrain.transform = terrain_transform
	terrain.name = "Loaded terrain"
