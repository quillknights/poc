class_name TestClass
extends Node
var failed = false
var fail_message = ""
var any_failed = false
func _ready():
	get_window().title = get_scene_file_path()
	Engine.time_scale = 10
	run_tests.call_deferred()
	
func run_tests():
	for method in get_method_list():
		if method["name"].begins_with("test_"):
			var method_name =method["name"]
			await run_test(method_name)
	var code = 0 
	if any_failed:
		code = 1
	get_tree().quit(code)
func run_test(method_name:String):
	failed = false
	setup()
	print("running %s"%[method_name])
	await call(method_name)
	if failed:
		print("Failed : %s"%[method_name])
		if fail_message:
			print("Error: %s"%[fail_message])
		any_failed = true
	else:
		print("Done: %s"%[method_name])
	tear_down()
	
func capture_signal(target:Signal):
	var capture = SignalCapture.new()
	capture.capture(target)
	return capture
func capture_bisignal(target:Signal):
	var capture = BiSignalCapture.new()
	capture.capture(target)
	return capture
func capture_trisignal(target:Signal):
	var capture = TriSignalCapture.new()
	capture.capture(target)
	return capture
func find_by_text(text,root=null)->Array[Node]:
	return find_by_attribute("text",text,root)
func find_by_name(value,root=null)->Array[Node]:
	var finder = func finder(node):
		return node.name == value
	return find(finder,root)
func find_by_texture(texture:Texture2D,root=null)->Array[Node]:
	return find_by_attribute("texture",texture,root)
func find_by_icon(icon:Texture2D,root=null)->Array[Node]:
	return find_by_attribute("icon",icon,root)
func find_by_attribute(attribute_name,value,root=null):
	return find(by_attribute(attribute_name,value),root)
func find(predicate,root=null) -> Array[Node]:
	if not root:
		root = self
	var matched:Array[Node] = []
	for child in root.get_children():
		if "visible" in child and not child.visible:
			continue
		
		if predicate.call(child):
			matched.append(child)
		else:
			matched.append_array(find(predicate,child))
	return matched
func by_has_attribute(attribute_name:String):
	var finder = func finder(node):
		return attribute_name in node
	return finder
func by_attribute(attribute_name:String,value):
	var has_attribute_predicate = by_has_attribute(attribute_name)
	var finder = func finder(node):
		return has_attribute_predicate.call(node) and node.get(attribute_name)==value
	return finder
func get_by_text(text:String,root=null)->Node:
	return get_by_attribute("text",text,root)
func get_by_name(node_name,root=null)->Node:
	var finder = func(n):return n.name == node_name
	return get_tested_node(finder,root)
func get_by_texture(texture,root=null)->Node:
	return get_by_attribute("texture",texture,root)
func get_by_icon(icon,root=null)->Node:
	return get_by_attribute("icon",icon,root)
func get_by_attribute(attribute,value,root=null)->Node:
	var finder = func(n):return attribute in n and n.get(attribute) == value
	return get_tested_node(finder,root)
func get_tested_node(predicate,root=null)->Node:
	var found = find(predicate,root)
	assert(len(found)!=0,"No value found")
	assert(len(found)==1,"To many value found")
	return found[0]
func tween_settle():
	for tween in get_tree().get_processed_tweens():
		if is_instance_valid(tween) and tween.is_running():
			await tween.finished
	return 
func send_text(target_node, text: String, typing_delay: float = 0.05):
	await click(target_node)
	await get_tree().create_timer(0.1).timeout  # Short pause after clicking

	for character in text:
		# Create key press event
		var key_press_event = InputEventKey.new()
		key_press_event.pressed = true
		key_press_event.keycode = OS.find_keycode_from_string(character)
		key_press_event.unicode = character.unicode_at(0)
		
		# Create key release event
		var key_release_event = InputEventKey.new()
		key_release_event.pressed = false
		key_release_event.keycode = OS.find_keycode_from_string(character)
		key_release_event.unicode = character.unicode_at(0)

		# Send key press event
		Input.parse_input_event(key_press_event)
		await get_tree().create_timer(typing_delay * 0.5).timeout

		# Send key release event
		Input.parse_input_event(key_release_event)
		await get_tree().create_timer(typing_delay * 0.5).timeout

	await get_tree().create_timer(0.2).timeout  # Short pause after typing

	# Simulate pressing Enter
	var enter_press = InputEventKey.new()
	enter_press.pressed = true
	enter_press.keycode = KEY_ENTER
	Input.parse_input_event(enter_press)

	await get_tree().create_timer(0.05).timeout

	var enter_release = InputEventKey.new()
	enter_release.pressed = false
	enter_release.keycode = KEY_ENTER
	Input.parse_input_event(enter_release)

func click(node):
	assert_visible([node],"Node Must Be visible to be clicked")
	var event_lmb = InputEventMouseButton.new()
	var position:Vector2=Vector2.ZERO
	if node is Control:
		position = node.get_global_rect().get_center()+node.get_viewport().get_canvas_transform().get_origin()
	if node is Node2D:
		position= node.global_position
	event_lmb.position =position
	event_lmb.button_index = MOUSE_BUTTON_LEFT
	event_lmb.pressed = true
	node.get_viewport().push_input(event_lmb)
	await frame()
	event_lmb = InputEventMouseButton.new()
	event_lmb.position = position
	event_lmb.button_index = MOUSE_BUTTON_LEFT
	event_lmb.pressed = false
	node.get_viewport().push_input(event_lmb)
	
func fail(message=""):
	failed = true
	fail_message = message
func assert_false(value:bool,message:String):
	return assert_true(not value, message)
func assert_equals(value,expected,message):
	assert_true(value==expected,message)
func assert_true(value:bool,message:String):
	if not value:
		fail(message)
	assert(value,message)	
func assert_found(find_result:Array,count:int, message:String):
	assert_true(find_result.size()>=count,message)
func assert_visible(find_result:Array,message):
	var visible = find_result.size()>0
	visible = visible && find_result.all(func(n):return n.modulate!=Color.TRANSPARENT and n.visible)
	assert_true(visible,message)
func assert_exists(find_result:Array,message):
	assert_true(find_result.size()>0,message)
func assert_not_exist(find_result:Array,message):
	assert_true(find_result.size()==0,message)
func frame(frame_count = 1):
	for i in range(frame_count):
		await get_tree().process_frame
func wait_for_find(by_finder,root=null,timeout_frames=10):
	while find(by_finder,root).size()==0 and timeout_frames:
		await frame()
		timeout_frames-=1
	print("found it")
	return find(by_finder,root).size()!=0
func setup():
	pass
func tear_down():
	pass



class SignalCapture:
	var target
	var captured_values =[]
	func capture(value:Signal):
		target = value
		target.connect(handle_signal)
	func handle_signal(value):
		captured_values.append(value)
class BiSignalCapture:
	var target
	var captured_values =[]
	func capture(value:Signal):
		target = value
		target.connect(handle_signal)
	func handle_signal(a,b):
		captured_values.append([a,b])
class TriSignalCapture:
	var target
	var captured_values =[]
	func capture(value:Signal):
		target = value
		target.connect(handle_signal)
	func handle_signal(a,b,c):
		captured_values.append([a,b,c])
