class_name InGameTest
extends TestClass

const LOBBY_SCENE = preload("res://scenes/lobby.tscn")
# This is a endtoend test
# or at least my best shot at an endtoend test
var lobby

@export_file("*.save") var game_save_path
@export var game_save_string:String

func setup():
	lobby = LOBBY_SCENE.instantiate()
	add_child(lobby)
	if not lobby.is_node_ready():
		await lobby.ready
	if game_save_path:
		lobby.load_save(game_save_path)
func start_game():
	lobby.start_game()
	await get_battle().turn_manager.on_turn_resume
func tear_down():
	
	if lobby:
		remove_child(lobby)
		lobby.queue_free()
func no_event():
	if GameEventService.is_event_running:
		await GameEventService.on_no_event_running
func get_battle():
	return lobby.game_state.battle
func get_game_state():
	return lobby.game_state
func get_creature_by_name(value:String,root=null):
	var creature_finder=func(n):
		return n is Creature and n.game_name == value
	return get_tested_node(creature_finder,root)
func get_by_hex_coord(value:Vector2,root=null):
	return get_by_attribute("hex_coord",value,root)
func get_action(design:CreatureActionDesign,root=null):
	var finder = func(n):
		return n is CreatureAction and n.design == design
	return get_tested_node(finder,root)
func wait_act(action:CreatureAction):
	get_tree().create_timer(0.2).timeout.connect(func():action.act())
	var acted = await action.on_acted
	return acted
