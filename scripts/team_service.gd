extends Node
enum RelationStatus{
	ALLIED=1, FRIENDLY=2, NEUTRAL=3, SUSPICIOUS=4, HOSTILE=5
}
# Dictionary of dictionaries, keys are team_ids, values are dicts of other_id:relationstatus, 
# reprensenting the teams oppinion of the other_id
# for now only teams should be in theses dictionaries
var point_of_views = {}
var members			={}
var creature_teams	={}
var teams_visions	={}

# Two creature area allied, if and only if they both see each other as allies
func are_allied(a:String,b:String)->bool:
	if a==b:
		return true
	return point_of_views[a][b]==RelationStatus.ALLIED and point_of_views[b][a]==RelationStatus.ALLIED
# Two creature are friendly if both of them see each other as at least a friend
func are_friendly(a:String,b:String)->bool:
	var a_of_b = point_of_views[a][b]
	var b_of_a = point_of_views[b][a]
	return a_of_b<=2 and b_of_a <=2
func are_neutral(a:String,b:String) -> bool:
	var a_of_b = point_of_views[a][b]
	var b_of_a = point_of_views[b][a]
	return a_of_b<=RelationStatus.NEUTRAL and b_of_a<=RelationStatus.NEUTRAL
func is_suspicious(a:String,b:String)->bool:
	return point_of_views[a][b]==RelationStatus.SUSPICIOUS
func is_hostile(a:String,b:String)->bool:
	return point_of_views[a][b]==RelationStatus.HOSTILE

func are_creature_allied(a:Creature,b:Creature)->bool:
	var team_a = get_creature_team(a)
	var team_b = get_creature_team(b)
	return are_allied(team_a,team_b)

func make_allies(a:String,b:String):
	point_of_views[a][b]=RelationStatus.ALLIED
	point_of_views[b][a]=RelationStatus.ALLIED
func make_friends(a:String,b:String):
	if point_of_views[a][b]!=RelationStatus.ALLIED:
		point_of_views[a][b]=RelationStatus.FRIENDLY
	if point_of_views[b][a]!=RelationStatus.ALLIED:	
		point_of_views[b][a]=RelationStatus.FRIENDLY
func make_hostile(a:String,b:String):
	point_of_views[a][b]=RelationStatus.HOSTILE

func get_allies(team:String):
	var allies = []
	for other in point_of_views[team].keys():
		if are_allied(team,other):
			allies.append(other)
	return allies
	
func get_creature_team(creature:Creature) -> String:
	return creature_teams[creature]
func register_team(team_id:String,_size:int):
	var team_pov = {}
	for other in point_of_views.keys():
		team_pov[other]=RelationStatus.NEUTRAL
		point_of_views[other][team_id]=RelationStatus.NEUTRAL
	point_of_views[team_id] = team_pov
	members[team_id]=[]
	teams_visions[team_id] = {}
func add_to_team(team_id:String,creature:Creature):
	members[team_id].append(creature)
	creature_teams[creature] = team_id


func set_member_sees(creature:Creature,element:Node2D,sees:bool):
	var team = get_creature_team(creature)
	var team_vision = teams_visions[team]
	if sees:
		if not team_vision.has(element):
			team_vision[element] = {}
		team_vision[element][creature] = true
	else:
		if team_vision.has(element):
			team_vision[element].erase(creature)
			if team_vision[element].size()==0:
				team_vision.erase(element)
func get_creature_team_sees(creature:Creature,element):
	var team = get_creature_team(creature)
	if has_vision(team,element):
		return true
	for ally in get_allies(team):
		if has_vision(ally,element):
			return true
	return false
func has_vision(team:String,element:Node2D)-> bool:
	if team == "Spectator":
		return true
	return teams_visions[team].has(element) and teams_visions[team][element].size()!=0

	
	
