class_name GameElementSerializer
extends GameElementSaver
var mutex:Mutex
func _init(p_instance_ids:Dictionary,p_instance_registry:Dictionary,p_mutex:Mutex):
	self.instance_ids = p_instance_ids
	self.instance_registry= p_instance_registry
	self.mutex = p_mutex

func register_new_variant(variant:Variant):
	mutex.lock()
	if variant not in instance_ids:
		super.register_new_variant(variant)
	mutex.unlock()

func _load(data:Dictionary)->Variant:
	mutex.lock()
	instance_registry.merge(data["instance_registry"])
	mutex.unlock()
	var root_id = data["root_id"]
	return from_save(root_id)
