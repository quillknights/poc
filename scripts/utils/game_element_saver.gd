class_name GameElementSaver
extends Object
var instance_count = 0
var instance_registry = {}
var instance_dictionaries = {}
var instance_ids = {}
var CREATURE = load("res://scenes/creature.tscn")
var MULTIPLAYER_BATTLE = load("res://scenes/multiplayer_battle.tscn")
var TERRAIN	= load("res://scenes/terrain.tscn")
var TERRAIN_OBJECT	= load("res://scenes/terrain_object/terrain_object.tscn")
var class_loaders = {
	"Creature":CREATURE.instantiate,
	"TurnManager":TurnManager.new,
	"TerrainEffect":TerrainEffect.new,
	"GameState":GameState.new,
	"Texture2D":Texture2D.new,
	"CreatureBattleStatistics":CreatureBattleStatistics.new,
	"CreatureBattleResources":CreatureBattleResources.new,
	"TeamConfig":TeamConfig.new,
	"MultiplayerBattle":MULTIPLAYER_BATTLE.instantiate,
	"Terrain"	:TERRAIN.instantiate,
	"TerrainObject"	:TERRAIN_OBJECT.instantiate,
	"CreatureCondition":CreatureCondition.new,
	"Concentration"		:Concentration.new,
	"ValueGetter"		:ValueGetter.new,
	"GameEvent"			:GameEvent.new,
	"Species"			:Species.new,
	"GameClass"			:GameClass.new,
	"GameClassLevel"	:GameClassLevel.new,
	"CreationOptions"	:CreationChoice.new,#Legacy 
	"CreationChoice"	:CreationChoice.new,
	"CreatureAttachableHolder":CreatureAttachableHolder.new,
	"Damage"					:Damage.new,	
	"DiceRoll"					:DiceRoll.new,
	"SavingThrow"				:SavingThrow.new,
	"AttackRoll"				:AttackRoll.new,
	"DamageRoll"				:DamageRoll.new,
	"SpellAttackRoll"			:SpellAttackRoll.new,
	"AbilityCheck"				:AbilityCheck.new,
}
func create_instance_id(instance:Variant):
	instance_count +=1
	var peer_id = MultiplayerService.multiplayer.get_unique_id()
	var id =  "%s:%d:%d"%[instance.get_class_save_id(),instance_count,peer_id]
	while id in instance_dictionaries:
		instance_count+=1
		id =  "%s:%d:%d"%[instance.get_class_save_id(),instance_count,peer_id]
	return id
func to_save(variant:Variant):
	if variant == null:
		return null
	var data = null
	var type = null
	if variant is Vector2:
		type = "Vector2"
		data= var_to_str(variant)
	elif not variant is Variant:
		type = "Base"
		data = variant
	elif variant is String:
		type = "String"
		data = variant
	elif variant is Array:
		type="Array"
		data = variant.map(func(v):return to_save(v))
	elif variant is Dictionary:
		type="Dictionary"
		data = []
		for key in variant:
			data.append([to_save(key),to_save(variant[key])])
	elif typeof(variant)!=TYPE_OBJECT:
		type="Base"
		data = variant
	elif variant.has_method("to_save"):
		type = variant.get_class_save_id()
		if not variant in instance_ids:
			register_new_variant(variant)
		data = instance_ids[variant]
	elif variant is Resource:
		type ="Resource"
		data = variant.resource_path
	
	return {"type":type,"data":data}
func register_new_variant(variant:Variant):
	var instance_id = create_instance_id(variant) 
	instance_ids[variant] =instance_id
	instance_dictionaries[instance_id] = variant.to_save(self)
func from_save(save_data) -> Variant:
	
	if save_data==null:
		return null
	if not save_data is Dictionary:
		return save_data
	var data = save_data["data"]
	var class_instance_id = save_data["type"]
	
	match(class_instance_id):
		null:
			return null
		"Vector2":
			return str_to_var(data)
		"String":
			return data
		"Base":
			return data
		"Resource":
			return BaseResourceService.get_resource(data)
		"Array":
			var array = []
			for value in data:
				array.append(from_save(value))
			return array
		"Dictionary":

			var value = {}
			if data is Array:
				for item in data:
					var key = from_save(item[0])
					var v = from_save(item[1])
					value[key]=v
			else:
				# Legacy
				for key in data:
					value[key]=from_save(data[key])
			return value
		"GameUniqueId":
			if instance_registry[data] is Dictionary:
				var base_instance_data = instance_registry[data]			
				instance_registry[data] = MultiplayerService.game_unique_id_from_save(base_instance_data,self)				
			return instance_registry[data]
	if class_instance_id in class_loaders:
		if instance_registry[data] is Dictionary:
			var base_instance = class_loaders[class_instance_id].call()			
			var base_instance_data = instance_registry[data]			
			instance_registry[data] = base_instance
			base_instance.from_save(base_instance_data,self)
		return instance_registry[data]
	push_error("No loader found for %s"%[class_instance_id])
	return null
func _load(data:Dictionary)->Variant:
	instance_registry = data["instance_registry"]
	var root_id = data["root_id"]
	return from_save(root_id)
func _save(root_item:Variant):
	var root_id = to_save(root_item)
	return {
		"instance_registry":instance_dictionaries,
		"root_id":root_id
	}
	

static func to_save_string(root_item):
	var saver = GameElementSaver.new()
	var save_data = saver._save(root_item)
	var json_string = JSON.stringify(save_data)
	return json_string
static func save(save_name,root_item:Variant):
	if not root_item.has_method("to_save"):
		print("Root save item must have to_save method")
		return
	var json_string = to_save_string(root_item)
	write_save_string(save_name,json_string)

static func write_save_string(save_name,json_string):
	if not ".save" in save_name:
		save_name = "user://%s.save"%[save_name]
	var save_file = FileAccess.open(save_name, FileAccess.WRITE)
	save_file.store_line(json_string)
static func get_save_string(save_name):
	var file_path = save_name
	if not ".save" in file_path:
		file_path = "user://%s.save"%[save_name]
	if not FileAccess.file_exists(file_path):
		return # Error! We don't have a save to load.
	var save_file = FileAccess.open(file_path,FileAccess.READ)
	var json_string = save_file.get_as_text()
	return json_string
	
static func load(save_name):
	var json_string = get_save_string(save_name)
	if not json_string:
		print("Error loading save ")
		return
	
	return load_string(json_string)
static func load_string(json_string:String):
	# Creates the helper class to interact with JSON
	var json = JSON.new()

	# Check if there is any error while parsing the JSON string, skip in case of failure
	var parse_result = json.parse(json_string)
	if not parse_result == OK:
		print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
		return

	# Get the data from the JSON object
	var node_data = json.get_data()
	var saver = GameElementSaver.new()
	var loaded =  saver._load(node_data)
	SI.register_instance_ids(saver)
	return loaded

static func list_saves():
	var saves = []
	var dir = DirAccess.open("user://")
	if dir:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				pass
			elif ".save" in file_name:
				saves.append(file_name.split(".save")[0])
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")
	return saves
static func is_savable(variant:Variant)->bool:
	if variant is Resource:
		return true
	if typeof(variant)==TYPE_OBJECT:
		return InterfaceCheckTool.is_savable(variant)
	if typeof(variant) == TYPE_SIGNAL or typeof(variant) == TYPE_CALLABLE:
		return false
	return true
	
