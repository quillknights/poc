class_name InterfaceCheckTool
# Checks if a node holds a specific interface (as godot has no support for them
# check_interfacename pushes errors and returns false
# is_interfacename	 just returns without errors
extends Object



# targetable_interface
const targetable_methods = [
	"highlightable","highlight"
]
const targetable_children = [
	"TargetArea"	
]
const target_able_attributes = [
	"unique_id","hex_coord"
]

# A targetable node must have:
# - Highlight method
# - higlightable method
# - target_area child node
# - A unique ID
static func check_targetable(node:Node)->bool:
	if not node.is_in_group("targetables"):
		return false
	return check_interface(node,targetable_methods,target_able_attributes,targetable_children)
static func check_interface(variant,methods,attributes,children)->bool:
	for method in methods:
		if not variant.has_method(method):
			push_error("e%s does not have %s method"%[variant,method])
			return false
	if children:
		if not variant is Node:
			push_error("%s is not a node and must be"%[variant])
			return false
		for child in children:
			if not variant.has_node(child):
				push_error("%s does not have %s child"%[variant,child])
				return false
	for attribute in attributes:
		if not attribute in variant:
			push_error("%s does not have %s attribute"%[variant,attribute])
			return false
	return true
static func is_interface(variant,methods,attributes,children)->bool:
	for method in methods:
		if not variant.has_method(method):
			return false
	if children:
		if not variant is Node:
			return false
		for child in children:
			if not variant.has_node(child):
				return false
	for attribute in attributes:
		if not attribute in variant:
			return false
	return true
	
const hittable_methods = [
	"take_damage",
	"get_ac",
	"apply_conditions",
	"can_save",
	"get_damage_taken",	
]
const hittable_attribute=[
	"on_damage_taken"
]
const hittable_children =[]
static func is_hittable(node:Node)->bool:
	if node == null:
		return false
	if not node.is_in_group("hittables"):
		return false
	return is_interface(node,hittable_methods,hittable_attribute,hittable_children)
static func is_proxy(variant:Variant):
	return "proxy_for" in variant


static func is_visible_element(node:Node2D) -> bool:
	return node.is_in_group("fov_subjects")
	
static func is_terrain_collidable(node:Node2D)->bool:
	if node is Area2D:
		print("node is area 2D ")
	return node is CreatureMini or node is TerrainObject
	
const attachable_methods = [
	"attach",
	"reattach",
]
const attachable_attribute=[
	"creature"
]
const attachable_children =[]
static func is_attachable(node:Node):
	if node == null:
		return false
	return is_interface(node,attachable_methods,attachable_attribute,attachable_children)

const damageable_methods = [
	"take_damage",
	"get_damage_taken",
]
const damageable_attribute=[
	"on_damage_taken"
]
const damageable_children =[]
static func is_damageable(node:Node):
	if node == null:
		return false
	return is_interface(node,damageable_methods,damageable_attribute,damageable_children)

const savable_methods = [
	"to_save",
	"from_save",
	"get_class_save_id"
]
const savable_attribute=[
]
const savable_children =[]
static func is_savable(node:Variant):
	if node == null:
		return true
	return is_interface(node,savable_methods,savable_attribute,savable_children)
