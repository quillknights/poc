class_name TextTools
extends Object

static func to_roman(n:int):
	var digits = [[1000, 'M'], [900, 'CM'], [500, 'D'], [400, 'CD' ],
			[100, 'C'], [90, 'XC'], [50, 'L'], [40, 'XL'],
			[10, 'X'], [9, 'IX'], [5, 'V'], [4, 'IV'], [1, 'I']]
	var result = ""
	while len(digits) > 0:
		var val = digits[0][0]
		var romn = digits[0][1]
		if n < val:
			digits.pop_front() # Removes first element
		else:
			n -= val
			result += romn
	return result
