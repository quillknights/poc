class_name GameSerializationService
extends Node
# Handles the serialization for one game instance 
var instance_ids:Dictionary = {}
var instance_registry:Dictionary={}
var serialization_mutex	:Mutex = Mutex.new()
var registry_instanciated = true
func register_instance_ids(saver:GameElementSaver):
	instance_ids = {}
	for id in saver.instance_registry:
		instance_ids[saver.instance_registry[id]] = id
	instance_registry = saver.instance_registry
	registry_instanciated = true

func s(variant:Variant)->String:
	if not registry_instanciated:
		push_error("Must initialize instance id registry before serialization")
	var serializer = GameElementSerializer.new(instance_ids,instance_registry,serialization_mutex)
	var data= serializer._save(variant)
	return JSON.stringify(data)
func p(data:String)->Variant:
	if not registry_instanciated:
		push_error("Must initialize instance id registry before serialization")
	var serializer = GameElementSerializer.new(instance_ids,instance_registry,serialization_mutex)
	var parsed = JSON.parse_string(data)
	return serializer._load(parsed)
	
