class_name ShapeTools
extends Object

static func generate_circle_polygon(radius: float, num_sides: int, circle_center: Vector2) -> PackedVector2Array:
	var angle_delta: float = (PI * 2) / num_sides
	var vector: Vector2 = Vector2(radius, 0)
	var polygon= PackedVector2Array()

	for _i in num_sides:
		polygon.append(vector + circle_center)
		vector = vector.rotated(angle_delta)

	return polygon
static func make_smooth_path(points:Array[Vector2])->Array[Array]:
	var path:Array[Array] = []
	for i in range(points.size()):
		var point = points[i]
		var in_point = Vector2(0,0)
		var out_point=Vector2(0,0)
		if i>0 and i<points.size()-1:
			var next = points[i+1]			
			var previous = points[i-1]
			in_point = (previous-next)*0.20
			out_point = -(previous-next)*0.20
			path.append([point,in_point,out_point])
		else:
			path.append([point])
	return path
