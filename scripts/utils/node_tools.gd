class_name NodeTools
extends Object

static func clear_children(parent:Node):
	for child in parent.get_children():
		parent.remove_child(child)
		child.queue_free()
static func replace_child(parent: Node, child: Node, replacement: Node) -> Node:
	# Check if parent is valid
	if not is_instance_valid(parent):
		push_error("Invalid parent node")
		return

	# Check if child is valid and is a child of parent
	if not is_instance_valid(child) or child.get_parent() != parent:
		push_error("Invalid child node or child is not a direct child of parent")
		return

	# Check if replacement is valid
	if not is_instance_valid(replacement):
		push_error("Invalid replacement node")
		return

	# Get the index of the child
	var child_index = child.get_index()

	# Remove the child
	parent.remove_child(child)

	# Add the replacement at the same index
	parent.add_child(replacement)
	parent.move_child(replacement, child_index)
	return replacement
