class_name TerrainObjectDesign
extends Resource

@export var shape		:PackedVector2Array
@export var icon		:Texture2D
@export var is_movable	:bool
@export var weight		:int
@export var animations	:SpriteFrames
@export var is_occluding:bool
@export var block_tile	:bool
@export var health		:int
@export var base_ac		:int = 0
@export var game_name	:String
