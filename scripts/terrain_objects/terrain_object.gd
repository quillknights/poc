class_name TerrainObject
extends Node2D

const HIGHLIGHT_MATERIAL = preload("res://shaders/creature_mini_sprite_hovered.tres")
const HIGHLIGHTABLE_MATERIAL = preload("res://shaders/creature_mini_sprite_outlined.tres")
var is_movable			:bool:get=_is_movable
var is_highlightable	:bool = false
var health				:int=-1

@export var hex_coord			:Vector2=Vector2.INF
@export var unique_id			:String:set =_set_unique_id
@export var game_name			:String
@export var design				:TerrainObjectDesign
@onready var shape = $TargetArea/Shape
@onready var occlusion_shape = $OcclusionShape
@onready var sprite:Sprite2D = $Sprite
@onready var mouvement:TerrainMouvement = $Mouvement
@onready var area:Area2D = $TargetArea


signal on_pushed_back(TerrainObject)
signal on_moved(TerrainObject)
signal on_colide_object(TerrainObject,Variant)
signal on_collide_terrain(TerrainObject,Variant)
signal on_damage_taken(TerrainObject,Damage)
signal on_health_change(TerrainObject)
signal on_destroy(TerrainObject)

func _ready():
	shape.polygon = design.shape
	sprite.texture = design.icon
	if design.is_occluding:
		occlusion_shape.occluder.polygon = design.shape
	mouvement.on_moved.connect(handle_on_moved)
	mouvement.on_push_done.connect(handle_on_pushed_back)
	area.area_entered.connect(handle_area_entered)
	area.body_entered.connect(handle_body_entered)
	health = design.health
	game_name = design.game_name
func handle_on_moved(_mouvement):
	on_moved.emit(self)
func handle_on_pushed_back(_mouvement):
	on_pushed_back.emit(self)
func handle_body_entered(other):
	if other is Terrain and mouvement.is_moving:
		on_collide_terrain.emit(self,other)
func handle_area_entered(other:Area2D):
	var parent = other.get_parent()
	if mouvement.is_moving:
		if InterfaceCheckTool.is_terrain_collidable(parent):
			on_colide_object.emit(self,parent)
		if InterfaceCheckTool.is_terrain_collidable(other):
			on_colide_object.emit(self,other)
	
func highlight(value:bool):
	if is_highlightable:
		if value:
			sprite.material = HIGHLIGHT_MATERIAL
		else:
			sprite.material = HIGHLIGHTABLE_MATERIAL
func highlightable(value:bool):
	is_highlightable = value 
	if is_highlightable:
		sprite.material = HIGHLIGHTABLE_MATERIAL
	else:
		sprite.material = null
func take_damage(damage:Damage):
	if not multiplayer.is_server:
		return
	if design.health==-1:
		return
	print(" object taking damage")
	_take_damage.rpc(SI.s(damage))
@rpc("any_peer","call_local","reliable")
func _take_damage(damage_data:String):
	var damage:Damage = SI.p(damage_data)
	health=max(0,health-damage.value)
	on_damage_taken.emit(self,damage)
	on_health_change.emit(self)
	if health==0:
		destroy()
func destroy():
	on_destroy.emit(self)
	if multiplayer.is_server:
		await get_tree().create_timer(0.5).timeout
	if get_parent():
		get_parent().remove_child(self)
	if is_instance_valid(self):
		queue_free()
func get_ac()->DifficultyClass:
	var ac =  DifficultyClass.new()
	ac.base_value = design.base_ac
	return ac
func apply_conditions(_target):
	pass
func can_save()->bool:
	return false
func get_damage_taken(amount:int,type:DamageType):
	var damage = Damage.new()
	damage.base_value = amount
	damage.type = type
	damage.target = self
	apply_conditions(damage)
	return damage
func _is_movable()->bool:
	return design.is_movable
@rpc("authority","reliable","call_local")
func update_hex_coord(new_coord:Vector2):
	hex_coord = new_coord
@rpc("authority","reliable","call_local")
func move_along_(path:Array):
	mouvement.move_along(path)
@rpc("authority","reliable","call_local")
func push_back(direction:Vector2,distance:int):
	mouvement.push_back(direction,distance)
	
func _set_unique_id(value:String):
	unique_id = value
	BaseResourceService.register_resource(self)
func get_class_save_id()->String:
	return "TerrainObject"
func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
	"hex_coord":saver.to_save(hex_coord),
	"unique_id":unique_id,
	"game_name":game_name,
	"design":saver.to_save(design),
	"scale":saver.to_save(scale),
	"rotation":rotation
	}
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	hex_coord  = saver.from_save(save["hex_coord"])
	unique_id = save["unique_id"]
	game_name = save["game_name"]
	design  = saver.from_save(save["design"])
	scale = saver.from_save(save["scale"])
	rotation = save["rotation"]
