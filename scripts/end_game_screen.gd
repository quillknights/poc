extends Node2D
@export var winner:Creature
@export var losers:Array[Creature]
@export var LOSER_ROW_OFFSET = 180

const CREATURE_PORTRAIT = preload("res://scenes/creature_portrait_2.tscn")

@onready var winner_spot = $WinnerSpot
@onready var loser_row = $LoserRow
@onready var button = $CenterContainer/HBoxContainer/Button

func _ready():
	var portrait = CREATURE_PORTRAIT.instantiate()
	portrait.creature = winner
	winner_spot.add_child(portrait)
	for loser in losers:
		portrait = CREATURE_PORTRAIT.instantiate()
		portrait.creature = loser
		loser_row.add_child(portrait)
		portrait.handle_death(loser)
		portrait.transform.origin.x = LOSER_ROW_OFFSET*loser_row.get_child_count()
	button.button_up.connect(handle_new_game)
func handle_new_game():
	get_tree().reload_current_scene()
