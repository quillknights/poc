class_name DifficultyClass
extends Object

var roll 		:DiceRoll
var opposition	#The spell caster, the wall to climb etc
var action		:CreatureAction
var context		#The spell casted, the trap, the rest etc
var base_value	:int
var modifiers	:Array[int] =[]

var value		:int			:get=_get_value

func _get_value()->int:
	return base_value + modifiers.reduce(func(a,b):return a+b,0)

func add_modifier(mod:int):
	modifiers.append(mod)

func accept_condition(condition):
	condition.affect_difficulty_class(self)
