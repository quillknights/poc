class_name SavingThrow
extends DiceRoll

var characteristic:CreatureBattleStatistics.Type
var against			:Variant:set=_set_against # Must Be serializable

func _set_against(value:Variant):
	if not GameElementSaver.is_savable(value):
		push_error("Only savable elements can be specified as against a saving throw")
		return
	against = value

func get_class_save_id() -> String:
	return "SavingThrow"

func to_save(saver:GameElementSaver)->Dictionary:
		var data = super.to_save(saver)
		data["characteristic"] = saver.from_save(characteristic)
		return data


func from_save(save:Dictionary,saver:GameElementSaver):
	super.from_save(save,saver)
	characteristic = saver.from_save(save["characteristic"])
