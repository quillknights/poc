extends Resource
class_name Dice
@export var unique_id:String:set=_set_unique_id
@export var icon :Texture2D
@export var roll_animation:SpriteFrames
@export var name:String
@export var maximum:int
@export var minimum:int


func _set_unique_id(value:String):
	unique_id = value
	unique_id = resource_path
