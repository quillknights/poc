class_name SpellAttackRoll
extends DiceRoll

@export var spell:Spell
var target

func _init():
	type=DiceRoll.Type.SpellAttack
	


func get_class_save_id() -> String:
	return "SpellAttackRoll"

func to_save(saver:GameElementSaver)->Dictionary:
	var data = super.to_save(saver)
	data["spell"] = saver.to_save(spell)
	data["target"] = saver.to_save(target)
	
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	super.from_save(save,saver)
	spell = saver.from_save(save["spell"])
	target = saver.from_save(save["target"])
