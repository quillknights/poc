class_name DiceRollView
extends Node2D
#When all animation are done
signal finished()
# when roll has been animated
signal roll_animated()
var frozen:bool=false


@onready var dice_roll_value = $RollValueContainer/DiceRollValue
@onready var dice_roll_animation:AnimatedSprite2D = $DiceRollAnimation
@onready var animation_player:AnimationPlayer = $AnimationPlayer

var dice_roll_queue:Array[DiceRoll]=[]
func _ready():
	dice_roll_animation.visible=false
	dice_roll_value.visible=false
	roll_animated.connect(handle_roll_animated)
func rolled(rolled:DiceRoll):
	if not frozen:
		handle_roll_animation(rolled)
	else:
		animation_player.play("DiceAnimations/dice_show_result",-1,3)
		dice_roll_value.text=str(rolled.result)
		dice_roll_animation.sprite_frames=rolled.dice.roll_animation
		dice_roll_animation.visible=true
		dice_roll_animation.play("Roll",3)
		
func handle_roll_animation(rolled:DiceRoll):
	GameEventService.register_animation(self)
	if dice_roll_animation.is_playing():
		dice_roll_queue.append(rolled)
		return
	
	dice_roll_animation.visible=true
	dice_roll_animation.sprite_frames = rolled.dice.roll_animation
	dice_roll_animation.play("Roll")
	await dice_roll_animation.animation_finished
	dice_roll_value.text=str(rolled.result)
	animation_player.play("DiceAnimations/dice_show_result")
	await animation_player.animation_finished
	animation_player.play("DiceAnimations/dice_hide_dice")
	await animation_player.animation_finished
	roll_animated.emit()
	if not is_running():
		finished.emit()
func is_running():
	return dice_roll_animation.is_playing() or animation_player.is_playing() or dice_roll_queue
func play():
	pass
func pause():
	pass
func preparing(dice:Dice):
	dice_roll_value.hide()
	dice_roll_animation.sprite_frames = dice.roll_animation
	dice_roll_animation.show()
	dice_roll_animation.play()
func handle_roll_animated():
	if dice_roll_queue:
		var roll = dice_roll_queue[0]
		dice_roll_queue.remove_at(0)
		handle_roll_animation(roll)
func is_valid():
	return true
		
