class_name AbilityCheck
extends DiceRoll

var ability :CreatureBattleStatistics.Type

func get_class_save_id() -> String:
	return "AbilityCheck"

func to_save(saver:GameElementSaver)->Dictionary:
		var data = super.to_save(saver)
		data["ability"] = saver.from_save(ability)
		return data


func from_save(save:Dictionary,saver:GameElementSaver):
	super.from_save(save,saver)
	ability = saver.from_save(save["ability"])
