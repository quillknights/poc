class_name TableInitiativeRolls
extends Node
@export var creatures:Array[Creature]
var initiative_order:Array[Creature] = []
var initiatives:Dictionary = {}
var rolls: Dictionary = {}
signal on_done(TableInitiativeRolls)
# Called when the node enters the scene tree for the first time.
func _ready():
	if not creatures:
		push_error("TableInitiativeRolls need a creature list before executing")
		get_parent().remove_child(self)
		queue_free()
func roll():
	for creature in creatures:
		var creature_roll = creature.get_initiative_roll()
		rolls[creature_roll] = creature		
		creature_roll.roll()
		initiatives[creature] = creature_roll
	assign_initiative_order()
func assign_initiative_order():
	var sorted = initiatives.keys().map(func(k):return [-initiatives[k].result,k])
	sorted.sort()
	for init in sorted:
		initiative_order.append(init[1])
	on_done.emit(self)
func get_game_event() -> GameEvent:
	var event = GameEvent.new()
	event.type = GameEvent.Type.InitiativeRoll
	event.payload ={
		"creatures":creatures.map(func(c):return c.unique_id)
	}
	return event
