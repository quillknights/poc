class_name DiceRoll
extends Object
enum Type{
	Flat,Attack,Damage,Saving,SpellAttack,SpellDamage
}

var unique_id			:GameUniqueId

@export var modifiers			:Array[int]=[]
@export var dice				:Dice
@export var advantage_level		:int
@export var type				:Type

var rolled_dc 	:DifficultyClass
var result		:int:set = _set_result
var raw_result	:int
var rng = RandomNumberGenerator.new()

signal on_rolling(DiceRoll)
signal on_result(int)


func add_modifier(mod:int):
	modifiers.append(mod)
func estimate(dc:int)->float: 
	# Using this formula adapted for a arbitrary dice range https://www.reddit.com/r/DnD/comments/802zzo/comment/dusrx2d
	# This does not handle critical success and failures
	var value_range=abs(dice.maximum+1-dice.minimum)
	var modifier = float(modifiers.reduce(func (a,b):return a+b))
	if advantage_level>0:
		return (1-pow((dc-modifier-1)/value_range,2))*100
	if advantage_level<0:
		return (pow(1-((dc-modifier-1)/20),2))*100
	return (1-(dc-1-modifier)/(value_range))*100
	
func roll(dc:DifficultyClass =null)->int:
	raw_result = rng.randi_range(dice.minimum,dice.maximum)
	if advantage_level>0:
		raw_result = max(raw_result,rng.randi_range(dice.minimum,dice.maximum))
	if advantage_level<0:
		raw_result = min(raw_result,rng.randi_range(dice.minimum,dice.maximum))
	result = raw_result
	if modifiers:
		result  += modifiers.reduce(func (a,b):return a+b)	
	if dc:
		rolled_dc = dc
	on_rolling.emit(self)
	return result
	
func add_advantage():
	advantage_level+=1
func add_disadvantage():
	advantage_level-=1
func _set_result(value:int):
	result = value

	
func printable_value():
	var mod_string = ""
	for mod in modifiers:
		mod_string+=" %+d "%[mod]
	if result:
		return " %d (%s:%d %s)"%[result,dice.name,raw_result,mod_string]
	else:
		return " ? (%s:? %s)"%[dice.name,mod_string]
func get_game_event():
	var event = GameEvent.new()
	event.type="DiceRoll"
	var modifier_sentence = modifiers.reduce(func (a,b):return "%s %+d"%[a,b],"")
	event.sentence	= "%s rolled %d (%d %s) "%[
		dice.name,
		result,
		raw_result,
		modifier_sentence
	]
	if rolled_dc:
		event.sentence = "%s against %s"%[
			event.sentence,
			rolled_dc.value
		]
	event.payload = {
		"dice":dice.unique_id,
		"result":result,
		"modifiers":modifiers,
		"advantage_level":advantage_level,
	}
	return event

func accept_condition(condition:CreatureCondition):
	condition.affect_roll(self)



func get_class_save_id() -> String:
	return "DiceRoll"

func to_save(saver:GameElementSaver)->Dictionary:
		var data = {
	"unique_id":saver.to_save(unique_id),
	"modifiers":saver.to_save(modifiers),
	"dice":saver.to_save(dice),
	"advantage_level":saver.to_save(advantage_level),
	"type":saver.to_save(type),
	"rolled_dc":saver.to_save(rolled_dc),
	"result":saver.to_save(result),
	"raw_result":saver.to_save(raw_result),

		}
		return data


func from_save(save:Dictionary,saver:GameElementSaver):
	unique_id  = saver.from_save(save["unique_id"])
	modifiers.clear()
	modifiers.assign(saver.from_save(save["modifiers"]))
	dice  = saver.from_save(save["dice"])
	advantage_level  = saver.from_save(save["advantage_level"])
	type  = saver.from_save(save["type"])
	rolled_dc  = saver.from_save(save["rolled_dc"])
	result  = saver.from_save(save["result"])
	raw_result  = saver.from_save(save["raw_result"])
