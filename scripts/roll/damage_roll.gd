class_name DamageRoll
extends DiceRoll

@export var weapon		:Weapon
var target


func _init():
	self.type = DiceRoll.Type.Damage



func get_class_save_id() -> String:
	return "DamageRoll"

func to_save(saver:GameElementSaver)->Dictionary:
	var data = super.to_save(saver)
	data["weapon"] = saver.to_save(weapon)
	data["target"] = saver.to_save(target)
	
	return data


func from_save(save:Dictionary,saver:GameElementSaver):
	super.from_save(save,saver)
	weapon = saver.from_save(save["weapon"])
	target = saver.from_save(save["target"])
