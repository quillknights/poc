extends MarginContainer
class_name ActionButton

@export var action:CreatureAction:set=_set_action
@export var UNAFFORDABLE_FILTER:Color
@export var SELECTED_FILTER:Color
@onready var resource_bar = $TextureButton/ResourceBar
@onready var button = $TextureButton

var is_selected =false
var configurator:ActionConfigurator
signal on_selected(CreatureAction)
func _ready():
	button.pressed.connect(handle_pressed)
	
func handle_pressed():
	handle_click()
func handle_click():
	if not is_selected:
		select()
	else:
		deselect()

func select():
	if 	not action.can_select():
		return
	action.select()
	is_selected=true
	refresh_texture()
	on_selected.emit(action)
	if action.design.configurator:
		attach_configurator(action)
	else:
		act()
	
func deselect():
	action.deselect()
	action.reset()
	is_selected=false 
	if configurator:
		configurator.detach()
		configurator = null
	refresh_texture()

func attach_configurator(target:CreatureAction):
	configurator = target.design.configurator.instantiate()
	configurator.action = target
	add_child(configurator)
	configurator.on_configured.connect(on_configured)
func on_configured():
	act()
	deselect()
func act():
	action.act.rpc_id(1)

func refresh_texture():
	if not action.is_affordable(action.actor):
		button.set_modulate(UNAFFORDABLE_FILTER)
	elif is_selected:
		button.set_modulate(SELECTED_FILTER)
	else:
		button.set_modulate(Color.WHITE)	
func refresh_resource_bar():
	for sprite in resource_bar.get_children():
		resource_bar.remove_child(sprite)
		sprite.queue_free()
	for resource in action.design.cost:
		var sprite = Sprite2D.new()
		sprite.texture = resource.icon
		sprite.position = Vector2(-75*resource_bar.get_child_count(),0)
		resource_bar.add_child(sprite)
func _set_action(value:CreatureAction):
	if action:
		action.on_acted.disconnect(handle_acted)
	action = value
	if not is_node_ready():
		await ready
	button.texture_normal = action.design.icon
	refresh_resource_bar()
	action.on_acted.connect(handle_acted)
	refresh_texture()
func handle_acted(_action):
	refresh_texture()


