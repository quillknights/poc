class_name TerrainMouvement
#Moves an item on the terrain
extends Node

@export var terrain_item	:Node2D
@export var terrain			:Terrain
@export var push_back_speed	:int  = 3
var move_tween		:Tween
var path_follower	:PathFollow2D
var next_path_step	:float 		=0.0
var is_moving 		:bool 		=false 
var moving_direction	:Vector2
var remaining_distance	:float
var target_hex			:Vector2
signal on_start_moving(TerrainMouvement)
signal on_moved(TerrainMouvement)
signal on_path_step(TerrainMouvement)
signal on_path_done(TerrainMouvement)
signal on_push_done(TerrainMouvement)
signal on_changed_position(TerrainMouvement)

func _ready():
	terrain_item = get_parent()
	if not terrain_item:
		push_error("A terrain mouvement must be in a parent item")
		queue_free()
	terrain_item.on_colide_object.connect(handle_colide_object)
	terrain_item.on_collide_terrain.connect(handle_collide_terrain)	
		
# Takes an array of triplets, representing a bezier curve, each triplet is the point, in vector, out vector, all Vector2D
# in and out vectors can be omited
func move_along(path:Array):
	if not terrain_item.is_movable:
		return
	var curve = Curve2D.new()
	for step in path:
		if step.size()==1:
			curve.add_point(step[0])
		else:
			curve.add_point(step[0],step[1],step[2])
	var path_2d = Path2D.new()
	terrain_item.add_sibling(path_2d)	
	path_follower = PathFollow2D.new()
	path_2d.add_child(path_follower)
	path_2d.curve = curve
	move_tween = GameEventService.get_tween()
	path_follower.progress_ratio = 0.01
	move_tween.tween_method(hanle_step_on_path.bind(path),0.01,1.0,path.size()*0.4).set_ease(Tween.EASE_IN_OUT)
	move_tween.finished.connect(handle_move_done)	
	next_path_step=1
	is_moving = true
func hanle_step_on_path(progress:float,path:Array):
	if not path_follower:
		return
	path_follower.progress_ratio=progress
	terrain_item.global_position = path_follower.global_position
	var step_count = path.size()-1
	var current_path_step = progress*step_count
	if current_path_step>=next_path_step:
		on_path_step.emit(self)
		next_path_step+=1
func handle_move_done():
	if path_follower:
		var path = path_follower.get_parent()
		path.get_parent().remove_child(path)
	path_follower = null
	is_moving = false
	on_path_done.emit(self)
	
	

func push_back(direction:Vector2,distance:float):
	if not terrain_item.is_movable:
		return
	moving_direction =direction
	remaining_distance = distance 
	target_hex = HexCoordTool.pixel_to_hex(terrain_item.position)+direction*distance
	move_tween = GameEventService.get_tween()
	move_tween.tween_property(terrain_item,"position",HexCoordTool.hex_to_pixel(target_hex),distance/push_back_speed).set_ease(Tween.EASE_OUT)
	move_tween.parallel().tween_callback(handle_push_step)
	move_tween.finished.connect(handle_push_done)
	move_tween.play()
	is_moving = true
func bounce_back(to:Vector2):
	target_hex = to
	move_tween = GameEventService.get_tween()
	move_tween.tween_property(terrain_item,"position",HexCoordTool.hex_to_pixel(target_hex),1/float(push_back_speed)).set_ease(Tween.EASE_OUT)
	move_tween.finished.connect(handle_push_done)
	move_tween.play()
func handle_push_step():
	remaining_distance = HexCoordTool.distance(HexCoordTool.pixel_to_hex(terrain_item.position),target_hex)
func handle_collide_terrain(_moving,_terrain:Terrain):
	move_tween.kill()
	bounce_back(HexCoordTool.hex_round(HexCoordTool.pixel_to_hex(terrain_item.position)))

func handle_colide_object(_moving,_object):
	move_tween.kill()
	
	bounce_back(HexCoordTool.hex_round(HexCoordTool.pixel_to_hex(terrain_item.position))-moving_direction)
	

func handle_push_done():
	is_moving = false
	on_push_done.emit(self)
	on_changed_position.emit(self)
func stop_mouving():
	move_tween.kill()
	is_moving = false
	handle_move_done()
