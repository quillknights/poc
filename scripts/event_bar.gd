extends Node2D
class_name EventBar
@export var battle:Battle
@export var event_log_views={
	GameEvent.Type.Attack:preload("res://scenes/event_log/attack_log_view.tscn"),
	GameEvent.Type.ConsideredAttack:preload("res://scenes/considered_attack_log_view.tscn"),
	GameEvent.Type.OpportunityAttack:preload("res://scenes/event_log/attack_log_view.tscn"),
}
var current_roll:DiceRoll
var events:Array[GameEvent]=[]
var event_view:Node2D
@onready var entry_tree :Tree= $Control/ScrollContainer/entryTree




func _ready():
	GameEventService.on_game_event.connect(handle_new_event)
	GameEventService.on_battle_start.connect(handle_battle_start)
	GameEventService.on_battle_resume.connect(handle_battle_resume)
	GameEventService.on_game_event_considered.connect(handle_event_considered)
	entry_tree.create_item()
	self.hide()
	if GameEventService.event_log:
		for event in GameEventService.event_log:
			handle_new_event(event)
func handle_battle_start(_game_event):
	self.show()
func handle_battle_resume(_game_event):
	self.show()
func handle_new_event(event:GameEvent):
	if event_view:
		remove_child(event_view)
	events.append(event)
	add_event(event,entry_tree.get_root().create_child(0))
func add_event(event:GameEvent,view):
	view.set_text(0,event.sentence)
	for child in event.children:
		add_event(child,view.create_child())
func handle_event_considered(event:GameEvent):
	if event:
		var new_view = get_log_view(event)
		update_event_view(new_view)
	else:
		update_event_view(null)
func update_event_view(view:Node2D):
	if event_view:
		remove_child(event_view)
	event_view = view
	if event_view:		
		add_child(view)
	
func get_log_view(event:GameEvent)->Node2D:
	if event.type in event_log_views:
		var view =  event_log_views[event.type].instantiate()
		view.event = event
		return view
	return null
