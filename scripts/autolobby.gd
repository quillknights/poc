extends Node2D



@export var IP_ADDRESS:String
@export var PORT:int
@export var MAX_CLIENTS:int
@export var renew_save	:bool = false
@export var autostart	:bool = true
@export_file("*.save") var save_file

@onready var lobby = $Lobby


var creatures :
	get:
		return lobby.game_state.creatures.get_children()
var teams	:
	get:
		return lobby.game_state.teams.get_children()
func _ready():
	var process_id = OS.get_process_id()
	await _random_timer(100)
	if not host():
		be_client()
	if multiplayer.is_server():
		print("%d is server"%[process_id])
	else:
		print("%d is client number %d"%[process_id,multiplayer.get_unique_id()])
	
		
func be_client():
	if multiplayer.multiplayer_peer is OfflineMultiplayerPeer:
		var peer = ENetMultiplayerPeer.new()
		peer.create_client(IP_ADDRESS, PORT)
		multiplayer.multiplayer_peer = peer
		multiplayer.peer_connected.connect(_place_self)
		lobby.is_client()
func _place_self(_id):
	var self_index = 0
	var peers:Array = multiplayer.get_peers()
	peers.append(multiplayer.get_unique_id())
	peers.sort()
	self_index = peers.find(multiplayer.get_unique_id())
	get_window().position = Vector2(480*(self_index),100)
	get_window().title = "QuillKnight peer %d"%[multiplayer.get_unique_id()]
	
func host():
	# Create server.
	if multiplayer.multiplayer_peer is OfflineMultiplayerPeer:
		var peer = ENetMultiplayerPeer.new()
		peer.peer_connected.connect(_place_self)
		var result = peer.create_server(PORT, MAX_CLIENTS)
		if result == ERR_CANT_CREATE:
			return false
		multiplayer.multiplayer_peer = peer
		if save_file:
			lobby.load_save(save_file)
		if renew_save:
			GameElementSaver.save(save_file,lobby.game_state)
		lobby.hosting()
		if autostart:
			get_tree().create_timer(1.5).timeout.connect(func():lobby.start_game())
	return true
	
	
@rpc("authority","call_local","reliable")
func pong():
	print("%d pong"%[multiplayer.get_unique_id()])
func ping():
	pong.rpc()
	

func _random_timer(max_time:int):
	var rng = RandomNumberGenerator.new()
	var wait_time = rng.randi_range(10,max_time)
	print("%d waiting %d"%[OS.get_process_id(),wait_time])
	return get_tree().create_timer(float(wait_time)/100).timeout
