extends Node2D
@export var TILE_PIXEL_HEIGHT:int:set=_set_tile_pixel_height
@export var TILE_PIXEL_WIDTH	:int
var TILE_PIXEL_HALF_HEIGHT:float
var hex_direction_vectors = [
	Vector2(+1, 0), Vector2(+1, -1), Vector2(0, -1), 
	Vector2(-1, 0), Vector2(-1, +1), Vector2(0, +1), 
]
func get_tilemap_offset()->Vector2:
	return Vector2(float(TILE_PIXEL_WIDTH)/2,0.5*TILE_PIXEL_HEIGHT)
func pixel_to_hex(point:Vector2)->Vector2:
	point = point -get_tilemap_offset()
	var r = ((float(2)/3)*point.y)*2/(TILE_PIXEL_HEIGHT)
	var q = (point.x/TILE_PIXEL_WIDTH)-r/2
	return Vector2(q, r)
func hex_to_pixel(hex:Vector2)->Vector2:
	@warning_ignore("integer_division")
	var x = (TILE_PIXEL_WIDTH) * ( hex.x  +  hex.y/2)
	@warning_ignore("integer_division")
	var y = (TILE_PIXEL_HEIGHT/2) * (                         3./2 * hex.y)
	return Vector2(x, y)+get_tilemap_offset()
func hex_distance_to_pixel(d:float):
	# This distance is an approximation
	# We take the flatside center to center distance as 1 unit. 
	# And get a pixel distance from here
	var hex_distance = (sqrt(3)*TILE_PIXEL_HEIGHT/2)*d
	return hex_distance
func neightbors(point:Vector2)->Array[Vector2]:
	var ns:Array[Vector2] = []
	for dir in hex_direction_vectors:
		ns.append(point+dir)
	return ns
	
func distance(a:Vector2,b:Vector2)->int:
	var ac = hex_to_cube(a)
	var bc = hex_to_cube(b)
	return cube_distance(ac, bc)
func cube_distance(a:Vector3,b:Vector3)->int:
	var vec = b-a
	return max(abs(vec.x), abs(vec.y), abs(vec.z))
func axial_to_oddr(hex:Vector2i)->Vector2i:
	@warning_ignore("integer_division")
	var col = hex.x + (hex.y - (hex.y&1)) / 2
	var row = hex.y
	return Vector2i(col, row)
func oddr_to_axial(hex:Vector2i)->Vector2i:
	@warning_ignore("integer_division")
	var q = hex.x - (hex.y - (hex.y&1)) / 2
	var r = hex.y
	return Vector2i(q, r)
func hex_round(hex:Vector2)->Vector2:
	return cube_to_hex(cube_round(hex_to_cube(hex)))

func cube_to_hex(cube:Vector3)->Vector2:
	var q = cube.x
	var r = cube.y
	return Vector2(q, r)

func hex_to_cube(hex:Vector2):
	var q = hex.x
	var r = hex.y
	var s = -q-r
	return Vector3(q, r, s)

func cube_round(frac:Vector3)->Vector3:
	var q = round(frac.x)
	var r = round(frac.y)
	var s = round(frac.z)

	var q_diff = abs(q - frac.x)
	var r_diff = abs(r - frac.y)
	var s_diff = abs(s - frac.z)

	if q_diff > r_diff and q_diff > s_diff:
		q = -r-s
	elif r_diff > s_diff:
		r = -q-s
	else:
		s = -q-r

	return Vector3(q, r, s)
func _set_tile_pixel_height(value:int):
	TILE_PIXEL_HEIGHT = value
	TILE_PIXEL_HALF_HEIGHT = float(value)/2
