class_name SPELLS
extends Object

static var LEVELS:Dictionary= {
	0:[
		preload("res://resources/spell/cantrips/firebolt.tres"),
		preload("res://resources/spell/cantrips/poison_spray.tres"),
	],
	1:[
		preload("res://resources/spell/level1/tashaslaughter.tres"),
		preload("res://resources/spell/level1/thunder_wave.tres")
	]
}
static var Spells:Dictionary = {}

static func register_spell(spell:Spell):
	if spell.unique_id in Spells:
		return 
	if not spell.level in LEVELS:
		LEVELS[spell.level] = []
	if not spell in LEVELS[spell.level]:
		LEVELS[spell.level].append(spell)
	Spells[spell.unique_id]=true
