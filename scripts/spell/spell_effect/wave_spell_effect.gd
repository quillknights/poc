class_name WaveSpellEffect
extends SpellEffect
@export var width							:int
@export var speed							:float
@export var visual							:PackedScene

var wave									:WaveSpellEffectVisual
var end_hex		:Vector2
var start_hex 	:Vector2
var animation_in_progress:int = 0
func apply(terrain:Terrain,action:CastSpell):
	animation_in_progress+=1
	end_hex = action.target_positions[0]
	start_hex = action.actor.hex_coord
	wave = visual.instantiate()
	wave.start_hex = start_hex
	wave.end_hex = end_hex
	wave.effect = self
	wave.effect_range = action.design.spell_range
	wave.width = width
	wave.on_item_hit.connect(handle_item_hit.bind(action))
	wave.on_done.connect(handle_wave_done.bind(action))
	wave.attach(terrain)
	
	
func handle_item_hit(item,cast_spell:CastSpell)->void:
	if item is CreatureMini:
		if item.creature == cast_spell.actor:
			return
		handle_creature_hit(item.creature,cast_spell)
	if item is TerrainObject:
		handle_terrain_object_hit(item,cast_spell)
func handle_terrain_object_hit(object:TerrainObject,cast_spell:CastSpell):
	if object.is_movable:
		push_back(object,cast_spell)
func handle_creature_hit(target:Creature,cast_spell:CastSpell):
	if check_saving_throw(target,cast_spell):
		wave.animate_resist(target)
		return
	if not check_attack_roll(target,cast_spell):
		wave.animate_resist(target)
		return
	var hit_damages = roll_damages(target,cast_spell)
	for damage in hit_damages:
		target.take_damage(damage)
	wave.animate_damage(target)
	if cast_spell.effect_configuration.push_back_distance:
		push_back(target,cast_spell)

func push_back(pushed_back,cast_spell:CastSpell):
	
	animation_in_progress+=1
	var direction = HexCoordTool.hex_round((end_hex-start_hex).normalized())
	var distance = cast_spell.effect_configuration.push_back_distance
	var new_hex = pushed_back.hex_coord+Vector2(direction*distance)
	pushed_back.on_pushed_back.connect(handle_pushed_back.bind(new_hex,cast_spell))
	pushed_back.push_back.rpc(direction,distance)
func handle_wave_done(_wave:WaveSpellEffectVisual,cast_spell:CastSpell):
	animation_in_progress -=1
	check_done(cast_spell)
func handle_pushed_back(pushed_back,new_hex:Vector2,cast_spell:CastSpell):
	pushed_back.on_pushed_back.disconnect(handle_pushed_back)
	pushed_back.update_hex_coord.rpc(new_hex)
	animation_in_progress-=1
	check_done(cast_spell)
	
func check_done(cast_spell:CastSpell):
	if animation_in_progress==0:
		wave.get_parent().remove_child(wave)	
		wave.queue_free()	
		cast_spell.spell_casted(cast_spell.design)
