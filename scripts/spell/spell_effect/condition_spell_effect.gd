class_name ConditionSpellEffect
extends SpellEffect

@export var condition: CreatureConditionDesign
@export var target_required_ability	:CreatureBattleStatistics.Type
@export var target_ability_minimum	:int

var SPELL_SAVE_DC_GETTER = preload("res://resources/value_getters/spell_save_dc_getter.tres")
func apply(_terrain:Terrain,cast_spell:CastSpell):
	var affected_target = 0
	cast_spell.spell_state["affected"] = []
	for target in cast_spell.targets:
		if target is Creature:
			if apply_on_creature(target,cast_spell):
				affected_target+=1
		if InterfaceCheckTool.is_proxy(target) and target.proxy_for is Creature:
			if apply_on_creature(target.proxy_for,cast_spell):
				affected_target+=1
	cast_spell.effect_configuration.require_concentration=cast_spell.effect_configuration.require_concentration and affected_target!=0
	cast_spell.spell_casted(cast_spell.design)
func apply_on_creature(target:Creature,cast_spell:CastSpell):
	if not check_attack_roll(target,cast_spell):
		return false
	if check_saving_throw(target,cast_spell):
		return false
	if not check_required_ability(target,cast_spell):
		return false
	affect_target(target,cast_spell)
	return true
func get_target_saving_throw(target,cast_spell:CastSpell):
	return target.get_saving_throw(cast_spell.effect_configuration.saving_throw_characteristic,condition)
func check_required_ability(target:Creature,_cast_spell:CastSpell):
	if target_required_ability == CreatureBattleStatistics.Type.NONE:
		return true
	return target.statistics.get_value(target_required_ability)>=target_ability_minimum
func affect_target(target:Creature,cast_spell:CastSpell):
	var id = MultiplayerService.generate_unique_id()
	target.on_new_condition.connect(handle_condition_attached.bind(id,cast_spell))
	target.attach_new_condition.rpc(condition.resource_path,SI.s(id))
	cast_spell.spell_state["affected"].append([target,id])
func handle_condition_attached(condition:CreatureCondition,_target:Creature,id:GameUniqueId,cast_spell:CastSpell):
	if condition.instance_id ==id:
		var getter = SPELL_SAVE_DC_GETTER.init_getter()		
		getter.state["caster"] = cast_spell.actor
		getter.state["spell"] = cast_spell.design
		condition.design.set_save_dc_getter(condition,getter)
		condition.on_detach.connect(handle_condition_detached.bind(cast_spell))
func handle_condition_detached(condition:CreatureCondition,cast_spell:CastSpell):
	var id = condition.instance_id
	var attachements:Array = cast_spell.spell_state["affected"]
	for attachement in attachements:
		if id == attachement[1]:
			attachements.erase(attachement)
	if attachements.size()==0:
		cast_spell.actor.end_concentration.rpc()
	
func configure_concentration(concentration:Concentration,cast_spell:CastSpell):
	concentration.configuration = cast_spell.spell_state["affected"]
func handle_concentration_end(_concentration:Concentration):
	detach_conditions(_concentration.configuration)
func handle_concentration_failed(_concentration:Concentration):
	detach_conditions(_concentration.configuration)
	
func detach_conditions(attachements):
	for attachement in attachements:
		var creature = attachement[0]
		var id = attachement[1]
		creature.detach_condition.rpc(SI.s(id))
	
