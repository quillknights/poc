class_name SpellEffectVisual
extends Node2D

var terrain					:Terrain
var visual_instance_id		:GameUniqueId
var _is_client				:bool = true
func attach(new_terrain:Terrain):
	terrain = new_terrain
	visual_instance_id = MultiplayerService.generate_unique_id()
	terrain.add_spell_visual(self)
	add_client_visuals()
	_is_client = false
func _exit_tree():
	if not _is_client:
		terrain.dissmiss_spell_effect_client_visual.rpc(visual_instance_id.to_save(null))
func add_client_visuals():
	terrain.add_spell_effect_client_visual.rpc(visual_instance_id,get_config())
func get_config()->Dictionary:
	push_error("Child must overide this method")
	return {}
func from_config(_config:Dictionary):
	push_error("Child must overide this method")
