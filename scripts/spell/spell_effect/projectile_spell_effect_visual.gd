class_name ProjectileSpellEffectVisual
extends SpellEffectVisual

signal on_terrain_item_hit(ProjectileSpellEffectVisua,Node)
signal on_done(ProjectileSpellEffectVisual)
var effect			:ProjectileSpellEffect
var start_hex		:Vector2
var target_hex		:Vector2
var damage_type		:String 
var tween			:Tween
@onready var animated_sprite:AnimatedSprite2D= $AnimatedSprite2D
@onready var area_2d = $AnimatedSprite2D/Area2D

func _ready():
	tween = GameEventService.get_tween()
	var target_position = HexCoordTool.hex_to_pixel(target_hex)
	var distance = HexCoordTool.distance(target_hex,start_hex)
	
	animated_sprite.position = HexCoordTool.hex_to_pixel(start_hex)
	animated_sprite.sprite_frames = effect.animation
	animated_sprite.play("Flying")
	animated_sprite.look_at(to_global(target_position))
	area_2d.area_entered.connect(handle_area_entered)
	target_position = target_position-(target_position-animated_sprite.position).normalized()*HexCoordTool.TILE_PIXEL_HEIGHT*0.40
	
	tween.tween_property(animated_sprite,"position",target_position,distance/effect.flying_speed).set_ease(Tween.EASE_IN)
	#tween.finished.connect(handle_projectile_at_target)
	tween.play()
func get_config()->Dictionary:
	return {
		"effect"		:effect.resource_path,
		"target_hex"	:target_hex,
		"start_hex"		:start_hex
	}	
func from_config(config:Dictionary):
	effect 		=BaseResourceService.get_resource(config["effect"])
	target_hex 	=config["target_hex"]
	start_hex 	=config["start_hex"]

@rpc("any_peer","call_remote","reliable")
func miss_damage():
	if not _is_client:
		miss_damage.rpc()
	tween.kill()
	animated_sprite.play("Miss")
	await animated_sprite.animation_finished
	on_done.emit(self)
@rpc("any_peer","call_remote","reliable")
func animate_damage():
	if not _is_client:
		animate_damage.rpc()
	tween.kill()
	animated_sprite.play("Hit")
	await animated_sprite.animation_finished
	on_done.emit(self)

func handle_area_entered(other:Area2D):
	var parent = other.get_parent()
	if InterfaceCheckTool.is_hittable(parent):
		on_terrain_item_hit.emit(self,parent)
	elif InterfaceCheckTool.is_proxy(parent) and InterfaceCheckTool.is_hittable(parent.proxy_for):
		on_terrain_item_hit.emit(self,parent.proxy_for)
