class_name ProjectileSpellEffect
extends SpellEffect
const PROJECTILE_SPELL_EFFECT_VISUAL = preload("res://scenes/spells/projectile_spell_effect_visual.tscn")

@export var visual					:PackedScene
@export var flying_speed			:float




func apply(terrain:Terrain,action:CastSpell):
	if action.targets:
		for target in action.targets:
			launch_at_position(target.hex_coord,terrain,action)
	if action.target_creatures:
		for target in action.target_creatures:
			launch_at_creature(target,terrain,action)
	elif action.target_positions:
		for target in action.target_positions:
			launch_at_position(target,terrain,action)
func launch_at_creature(target:Creature,terrain:Terrain,action:CastSpell):
	var projectile = visual.instantiate()
	projectile.effect = self
	projectile.start_hex = action.actor.hex_coord
	projectile.target_hex = target.hex_coord
	projectile.on_target_reached.connect(handle_terrain_item_hit.bind(action))
	projectile.on_done.connect(handle_projectile_done.bind(action))
	projectile.attach(terrain)
	return projectile
func launch_at_position(target:Vector2,terrain:Terrain,action):
	var projectile = visual.instantiate()
	projectile.effect = self
	projectile.start_hex = action.actor.hex_coord
	projectile.target_hex= target
	projectile.on_terrain_item_hit.connect(handle_terrain_item_hit.bind(action))
	projectile.on_done.connect(handle_projectile_done.bind(action))
	projectile.attach(terrain)
	return projectile
func handle_terrain_item_hit(projectile:ProjectileSpellEffectVisual,item,cast_spell:CastSpell):
	if item == cast_spell.actor and not spell.effect.can_target_self:
		return
	if not InterfaceCheckTool.is_hittable(item):
		projectile.miss_damage()
		return
	if not check_attack_roll(item,cast_spell):
		projectile.miss_damage()
		return
	if check_saving_throw(item,cast_spell):
		projectile.miss_damage()
		return
	damage_target(item,cast_spell,projectile)
	
func handle_projectile_done(projectile:ProjectileSpellEffectVisual,cast_spell:CastSpell):
	if projectile.get_parent():
		projectile.get_parent().remove_child(projectile)
		projectile.queue_free()
	cast_spell.spell_casted(cast_spell.design)
func damage_target(target,cast_spell,projectile):
	var rolled = roll_damages(target,cast_spell)
	for damage in rolled:
		GameEventService.register_child(damage.get_game_event())
		target.take_damage(damage)
	projectile.animate_damage()
