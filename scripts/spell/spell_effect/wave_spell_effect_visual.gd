class_name WaveSpellEffectVisual
extends SpellEffectVisual


var start_hex		:Vector2
var end_hex			:Vector2
var width			:int
var effect_range	:int
var effect 			:WaveSpellEffect
var entered_area	:Array
signal on_item_hit(Variant)
signal on_done(WaveSpellEffectVisual)

@onready var animation_container = $animation_container
@onready var collision_square = $animation_container/Collider/CollisionSquare
@onready var collider = $animation_container/Collider

func _ready():
	var base_y = -(float(width)/2)*HexCoordTool.TILE_PIXEL_HALF_HEIGHT
	for i in range(width):
		var animation = AnimatedSprite2D.new()
		animation.sprite_frames = effect.animation
		animation.play("Rolling")
		var y = base_y+(i)*HexCoordTool.TILE_PIXEL_HALF_HEIGHT
		animation_container.add_child(animation)		
		animation.position = Vector2(0,y)
		animation.scale 	= Vector2.ONE*(HexCoordTool.TILE_PIXEL_HALF_HEIGHT/128)
	collision_square.shape.size = Vector2(HexCoordTool.TILE_PIXEL_HALF_HEIGHT,
										HexCoordTool.TILE_PIXEL_HALF_HEIGHT*(width-1))
	collider.area_entered.connect(handle_area_entered)
	var distance = HexCoordTool.distance(end_hex,start_hex)	
	var end_position = HexCoordTool.hex_to_pixel(end_hex)
	var start_position = HexCoordTool.hex_to_pixel(start_hex)
	animation_container.position = start_position
	animation_container.look_at(to_global(end_position))
	
	var tween = GameEventService.get_tween()
	tween.tween_property(animation_container,"position",end_position,distance/effect.speed)
	tween.finished.connect(handle_wave_done)
	tween.play()



func animate_resist(_target:Creature):
	pass
func animate_damage(_target:Creature):
	pass
func handle_wave_done():
	on_done.emit(self)
func handle_area_entered(area:Area2D):
	if area in entered_area:
		return
	entered_area.append(area)
	if area.get_parent() is CreatureMini:
		on_item_hit.emit(area.get_parent())
	if area.get_parent() is TerrainObject:
		on_item_hit.emit(area.get_parent())
func get_config()->Dictionary:
	return {
		"start_hex"	:start_hex,
		"end_hex"	:end_hex,
		"width"		:width,
		"range"		:effect_range,
		"effect"	:effect.unique_id
	}		
func from_config(config:Dictionary)->void:
	start_hex 	= config["start_hex"]
	end_hex 	= config["end_hex"]
	width 		= config["width"]
	effect_range= config["range"]
	effect		= BaseResourceService.get_resource(config["effect"])

