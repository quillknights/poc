class_name WaveSpellEffectConfigurator
extends SpellConfigurator

var spell		:Spell
var effect_range		:int
var width		:int
var start_hex	:Vector2

var mouse_hovering	:bool=false

@onready var click_detector_shape		:CollisionShape2D= $EffectContainer/Area2D/CollisionShape2D
@onready var click_detector 			:Area2D= $EffectContainer/Area2D
@onready var polygon_2d 		:Polygon2D= $EffectContainer/Polygon2D
@onready var effect_container = $EffectContainer



# Called when the node enters the scene tree for the first time.
func _ready():
	super._ready()
	spell= action.design
	action.terrain = ResourceService.get_battle().terrain
	effect_range = spell.spell_range
	width = spell.effect.width
	start_hex = action.actor.hex_coord
	click_detector.input_event.connect(handle_input_event)
	click_detector.mouse_entered.connect(handle_mouse_entered)
	click_detector.mouse_exited.connect(handle_mouse_exited)
	click_detector_shape.shape.radius = effect_range*HexCoordTool.TILE_PIXEL_HEIGHT
	polygon_2d.polygon = create_preview_points()
	remove_child(effect_container)
	ResourceService.get_battle().terrain.add_overlay(effect_container)
	effect_container.position = HexCoordTool.hex_to_pixel(start_hex)
	
	
	
func create_preview_points() -> PackedVector2Array:
	var pixel_y = float(width*HexCoordTool.TILE_PIXEL_HEIGHT)/2
	var pixel_x = float(effect_range*HexCoordTool.TILE_PIXEL_HEIGHT)
	var points = PackedVector2Array(
		[
		Vector2(0,-pixel_y/2),
		Vector2(0,pixel_y/2),
		Vector2(pixel_x,pixel_y/2),
		Vector2(pixel_x,-pixel_y/2)
	])
	return points

func pick_position(new_position:Vector2):
	target_location = HexCoordTool.pixel_to_hex(new_position)
	configured()

func handle_mouse_entered():
	mouse_hovering=true
func handle_mouse_exited():
	mouse_hovering = false
func handle_input_event(_vp,input_event:InputEvent,_i):
	if input_event is InputEventMouseMotion and mouse_hovering:
		var terrain = ResourceService.get_battle().terrain
		effect_container.look_at(terrain.to_global(terrain.to_local(terrain.get_viewport().get_mouse_position())))
	if input_event is InputEventMouseButton and input_event.is_pressed and input_event.button_index == MOUSE_BUTTON_LEFT:
		var terrain = ResourceService.get_battle().terrain
		
		pick_position(terrain.to_local(terrain.get_viewport().get_mouse_position()))
	
func detach():
	super.detach()
	effect_container.get_parent().remove_child(effect_container)
	effect_container.queue_free()
