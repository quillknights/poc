class_name SpellEffect
extends Resource
@export var unique_id						:String
@export var saving_throw_characteristic		:CreatureBattleStatistics.Type
@export var need_attack_roll				:bool
@export var animation						:SpriteFrames
@export var damages							:Array[DamageSource]
@export var push_back_distance				:int 		=0
@export var require_concentration			:bool
@export var needs_line_of_sight				:bool		=true
@export var can_target_self					:bool 		=false
var spell:Spell
signal on_done(SpellEffect)

func apply(_terrain:Terrain,_action:CastSpell):
	push_error("A sub effect should have overiden this function")

func check_saving_throw(target,cast_spell:CastSpell)->bool:
	if saving_throw_characteristic != CreatureBattleStatistics.Type.NONE:
		if not target.can_save:
			return true
		var saving_throw_roll = get_target_saving_throw(target,cast_spell)
		var dc = cast_spell.actor.get_spell_difficulty_saving_class(spell)
		var result = saving_throw_roll.roll(dc)
		GameEventService.register_child(saving_throw_roll.get_game_event())
		return result >= dc.value
	return false
func get_target_saving_throw(target,cast_spell:CastSpell):
	return target.get_saving_throw(cast_spell.effect_configuration.saving_throw_characteristic)
func check_attack_roll(target,cast_spell:CastSpell) -> bool:
	if cast_spell.effect_configuration.need_attack_roll:
		var roll = cast_spell.actor.get_spell_attack_roll(target,spell)
		target.apply_conditions(roll)		
		var ac = target.get_ac()		
		var result = roll.roll(ac)
		GameEventService.register_child(roll.get_game_event())		
		return  result >= ac.value
	return true
func roll_damages(target,cast_spell:CastSpell)->Array[Damage]:
	var results:Array[Damage] = []
	for source in cast_spell.effect_configuration.damage_sources:
		var damage = source.get_damages(cast_spell.actor,target,cast_spell)
		results.append(damage)
	return results
	
func get_configuration()->SpellEffectConfiguration:
	var config = SpellEffectConfiguration.new()
	config.saving_throw_characteristic = saving_throw_characteristic
	config.need_attack_roll = need_attack_roll
	config.animation = animation
	config.damage_sources = damages
	config.push_back_distance	=push_back_distance
	config.require_concentration = require_concentration
	return config
func configure_concentration(_c:Concentration,_cast_spell:CastSpell):
	pass
func handle_concentration_end(_concentration:Concentration):
	pass
func handle_concentration_failed(_concentration:Concentration):
	pass
	
class SpellEffectConfiguration extends Object:
	var saving_throw_characteristic		:CreatureBattleStatistics.Type
	var need_attack_roll				:bool
	var animation						:SpriteFrames
	var damage_sources					:Array[DamageSource]
	var push_back_distance				:int
	var additional_targets				:int
	var state
	var require_concentration			:bool
	
