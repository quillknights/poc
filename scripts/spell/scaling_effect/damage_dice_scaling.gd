class_name DamageDiceScalingEffect
extends SpellScalingEffect

@export var damage_source	:DamageSource

func apply(_cast_spell:CastSpell,effect_configuration,level:int):
	for i in range(level-floor_level):
		effect_configuration.damage_sources.append(damage_source)
func get_tooltip()->String:
	return "Adds %s pers additional spell level"%[damage_source.get_printable_str()]

