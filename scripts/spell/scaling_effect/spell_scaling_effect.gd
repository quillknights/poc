class_name SpellScalingEffect
extends Resource

var SPELL_SLOTS = [
	preload("res://resources/battle_resources/spell_slot_lvl1.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl2.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl3.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl4.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl5.tres"), 
	preload("res://resources/battle_resources/spell_slot_lvl6.tres")
]

@export var name		:String
@export var floor_level	:int	=0
@export var ceil_level	:int	=100

func apply(_cast_spell:CastSpell,_effect_configuration,_level:int):
	push_error("Children must overide this methode")
func can_cast_at(cast_spell:CastSpell,level:int):
	return cast_spell.actor.battle_resources.has_unspent_slot_level(level)
func get_tooltip() ->String :
	return "You should not be seing this"
func get_levels(spell:Spell,resources:CreatureBattleResources)->Array[BattleResource]:
	var levels:Array[BattleResource] = []
	for i in range(spell.level,min(ceil_level,6)):
		if resources.has_unspent_slot_level(i) :
			levels.append(SPELL_SLOTS[i-1])
	return levels
