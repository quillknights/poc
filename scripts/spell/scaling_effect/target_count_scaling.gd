class_name TargetCountScalingEffect
extends SpellScalingEffect

func apply(cast_spell:CastSpell,_effect_configuration,level:int):
	if cast_spell.target_count ==-1:
		push_error("Target Count Effect can't be applyied to a non targeting spell")
		return 
	cast_spell.additional_targets = level-floor_level
func get_tooltip()->String:
	return "Add one target pers additional spell level"

