class_name ProjectileSpellCastConfigurator 
extends SpellConfigurator

var target_picker				:TargetPicker
var spell_range					:int
var center					:Vector2

func _ready():
	super._ready()
	if not action:
		push_error("Configurator must have action set before ready")
		detach()
	spell_range = action.design.spell_range
	center = action.actor.hex_coord
	init_target_selection()
	action.terrain = ResourceService.get_battle().terrain

func init_target_selection():
	target_picker = TargetPicker.new()
	var targetables:Array[Node] = []
	for target in get_tree().get_nodes_in_group("targetables"):
		if not action.design.effect.can_target_self and target == action.actor.creature_mini:
			continue
		if action.design.effect.needs_line_of_sight and target is Node2D and not action.actor.has_line_of_sight(target):
			continue
		if HexCoordTool.distance(target.hex_coord,center) <= spell_range:
			targetables.append(target)
	target_picker.targets = targetables
	target_picker.on_target_hovered.connect(handle_target_hovered)
	target_picker.on_target_unhovered.connect(handle_target_unhovered)
	target_picker.on_target_picked.connect(handle_target_picked)
	add_child(target_picker)
func handle_target_picked(target:Variant):
	targets.append(target)
	if targets.size() == action.get_total_target_count():
		configured()

func handle_target_hovered(_creature_mini:Variant):
	pass
	#action.spell.target_creatures.append(creature_mini.creature)
	#GameEventService.on_game_event_considered.emit(action.get_considered_event())

func handle_range_detector_input_event(_vp,event:InputEvent,_i):
	if event is InputEventMouseButton and event.is_pressed() and event.button_index==MOUSE_BUTTON_LEFT:
		target_location=get_global_mouse_position()
		configured()

func handle_target_unhovered(_target:Variant):
	pass
	#GameEventService.on_game_event_considered.emit(null)
	
func detach():
	if get_parent():
		get_parent().remove_child(self)	
		queue_free()

