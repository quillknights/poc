class_name Spell
extends CreatureActionDesign

static var SPELL_SLOTS = [
	preload("res://resources/battle_resources/spell_slot_lvl1.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl2.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl3.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl4.tres"),
	preload("res://resources/battle_resources/spell_slot_lvl5.tres"), 
	preload("res://resources/battle_resources/spell_slot_lvl6.tres")
]


signal on_casted(Spell)

@export var unique_id			:String
@export var spell_range			:int
@export var creature_target		:bool=false
@export var target_count		:int
@export var effect				:SpellEffect	:set=_set_effect
@export var scaling				:SpellScalingEffect
@export_range(0,6) var level	:int


func cast(terrain:Terrain,cast_spell:CastSpell):
	effect.apply(terrain,cast_spell)
func configure(cast_spell:CastSpell):
	cast_spell.target_count = target_count
	cast_spell.additional_targets = 0 
	
func configure_concentration(concentration:Concentration,cast_spell:CastSpell):
	effect.configure_concentration(concentration,cast_spell)
func handle_concentration_end(_concentration:Concentration):
	effect.handle_concentration_end(_concentration)
func handle_concentration_failed(_concentration:Concentration):
	effect.handle_concentration_failed(_concentration)
	
func get_spell_casting_ability(caster:Creature)->CreatureBattleStatistics.Type:
	for game_class in caster.get_game_classes():
		if game_class.is_spell_active(self):
			return game_class.design.casting_ability
	push_error("Spell not active in classes")
	return CreatureBattleStatistics.Type.NONE

func _set_effect(value:SpellEffect):
	effect = value
	effect.spell = self

