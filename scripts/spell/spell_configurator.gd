class_name SpellConfigurator
extends ActionConfigurator
var targets					:Array # Array of node in the targetable group
var target_location			:Vector2 = Vector2.INF
var target_creature			:Creature = null
var casting_level			:int	=-1


func _ready():
	init_spell_level_selection()
	casting_level=action.design.level
func init_spell_level_selection():
	if action.design.scaling:
		var scalling =  action.design.scaling
		var action_bar = ResourceService.get_battle().action_bar
		var levels = scalling.get_levels(action.design,action.actor.battle_resources)
		var tooltip = scalling.get_tooltip()
		var level_int=action.design.level
		var current_level = scalling.SPELL_SLOTS[level_int-1]
		action_bar.select_resource_level(action,levels,tooltip,handle_resource_level_picked,current_level)
func handle_resource_level_picked(spell_slot:SpellSlot):
	casting_level = spell_slot.level
	action.casting_level = casting_level
	action.update_configuration()
func configured():
	if casting_level != -1:
		action.set_casting_level.rpc_id(1,casting_level)
	action.update_configuration.rpc_id(1)
	if target_creature:
		action.set_target_creatures.rpc_id(1,[target_creature.unique_id])
	if targets:
		action.set_targets.rpc_id(1,Array(targets.map(func(t):return t.unique_id)))
	if target_location:
		action.set_target_locations.rpc_id(1,[target_location])
	on_configured.emit()
func detach():
	if action.design.scaling:
		var action_bar = ResourceService.get_battle().action_bar
		action_bar.remove_resource_selection()		
