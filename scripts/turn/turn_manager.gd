class_name TurnManager
extends Node
signal on_battle_start()
signal on_turn_resume(Creature)
signal on_turn_start(Creature)
signal on_initiative_change()
const CREATURE = preload("res://scenes/creature.tscn")
@export var initiative:Array[Creature]
var initiative_index:int = 0
var _battle_started:bool=false

@rpc("authority","call_local","reliable")
func start_battle(init_ids_data:String):
	var init_ids = SI.p(init_ids_data)
	update_initiative(init_ids)
	GameEventService.on_battle_start.emit(null)
	on_battle_start.emit()
	await GameEventService.tween_settled()
	_battle_started=true
	start_round()
func resume_battle():
	_battle_started=true
	GameEventService.on_battle_resume.emit(null)
	await GameEventService.tween_settled()	
	resume_round()
	
func start_round():
	initiative_index = 0
	start_turn()
func resume_round():
	resume_turn()
func start_turn():
	get_current_creature().start_turn()
	on_turn_start.emit(get_current_creature())	
	if get_current_creature().is_dead:
		end_turn()
func resume_turn():
	get_current_creature().resume_turn()
	on_turn_resume.emit(get_current_creature())
	print("Resuming turn of %s"%[get_current_creature().game_name])
@rpc("any_peer","call_local","reliable")
func end_turn():
	while GameEventService.is_event_running:
		await GameEventService.on_no_event_running
	get_current_creature().end_turn()
	initiative_index+=1
	if initiative_index == initiative.size():
		start_round()
	else:
		start_turn()
func has_battle_started()-> bool:
	return _battle_started
func get_current_creature()->Creature:
	if not initiative:return null
	return initiative[initiative_index]
func is_turn_of(creature:Creature)->bool:
	return get_current_creature()==creature
@rpc("authority","call_local","reliable")
func update_initiative(init_ids:Array):
	initiative.clear()
	for creature in init_ids.map(BaseResourceService.get_resource):
		if not creature is Creature:
			push_error("%d:	Wrong initative, all must be creatures %s"%[multiplayer.get_unique_id(),init_ids])
			return	
		initiative.append(creature as Creature)
	
	on_initiative_change.emit()


func get_class_save_id()->String:
	return "TurnManager"
func to_save(saver:GameElementSaver)->Dictionary:
	var data = {
	"initiative":saver.to_save(initiative),
	"initiative_index":initiative_index,
	"battle_started":_battle_started,
	}
	return data



func from_save(save:Dictionary,saver:GameElementSaver):
	var new_initiative :Array[Creature] = []
	new_initiative.append_array(save["initiative"]["data"].map(func(d):return saver.from_save(d)))
	initiative = new_initiative
	initiative_index = save["initiative_index"]
	_battle_started = save["battle_started"]
