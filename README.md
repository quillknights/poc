# Quill Knigths Poc
_this is a poc for the game Quill Knights, more information on the game itself can be found on the [project main page](https://gitlab.com/quillknights/quillknights)_
## Goal
A game where:
- 2 players go head to head
- Small map
- on death the other player win
- Dice throws are displayed
- Everything that is a dice throw in DND is a dice throw in the game
- There is a weapon that has advantage on hit against any creature with haste
- There is one condition
- There is a terrain that gives you the haste condition
- Damage has a type 
- One player knows the Eldritch Blast spell

The game has animations, music, and basic graphics

## Motivation
I was falling in my usual problem of overengineering in the main project, this offshout is meant to be more focused on managing to make a quick and dirty poc. Seeing what models are really necessary, and what can be skipped.

I am still evidently strugling with the particulars of game engine model design. So the goal is also to have a playground to experiment on some practices, and understand what I am not managing. Some notes may apear on this subject here, or in the main project's obsidian

## On Git and code hygiene
This project is not meant to be a production git. So commits will be at the image of the first one, big and messy.
This project is not meant to take advantage of VCS version evolution. But mostly as storage and record of attempts and so on. 
## Todo
- [x] Basic Creature Status ( Renamed as the player doesn't create it)
- [x] Creature Action system
- [x] VERY basic attack and defend handling
- [x] initiative roll handling
- [x] Turn By Turn
- [X] Game Win
- [X] Game Event Display (Basic Done)
- [X] Have the creature move along the travel path
- [X] Basic multiplayer ( ~~Haha lol this won't be short~~ Not the shortest but took a day to get everything running, should stay solo player compatible which is nice)
- [X] Dice Roll Display (Not satisfied)
- [X] Creature Condition Support
- [X] Creature Weapon Support
- [x] Floor Effect support 
- [X] Handle Creature Reactions (Add opportunity attack, and maybe some magician shit) Do now as this needs to change the animation and event system to be interuptable
- [ ] Handle Class levels
	- [X] Fighter Level 1 support
	- [X] Wizard Level 1 Support
	- [ ] Class Level Progression
		- [ ] Class Speciality
		- [ ] Multiclass
		- [ ] Health progression
		- [ ] XP Win
- [X] Spell support
	- [X] Spell Slot
	- [X] Firebolt( For cantrip and base spell)
	- [X] Poison Spray ( For damage type and resistances)
	- [X] ThunderWave (For position changes and ability check)
	- [X] Tasha irresistable laughter ( for ability checks and immobilisations)
	- [X] Concentration	( And adapt tasha's laughter to it)
	- [X] Spell aimed at position and not creature Done With Thunderwave
	- [X] Casting Level
	- [x] Make the spells appear in the game event log
	
- [X] Handle more than 5 actions in action_bar
- [X] Damage Type
- [X] Obstacles
	- [X] Create terrain object and obstacles
	- [X] Make them take space on the terrain
	- [X] make them stop push_back movement
	- [X] make them targetable with spells 
- [X] Line of sight
	- [X] make spell require line of sight
	- [X] make hidden object disapear from the player display
	- [X] Fog Of war
	- [ ] Optimize FOV Performance [See this report](documentation/profiling_reports/24-07-24:FovPerformanceIssue.png)
- [X] Walls
- [X] Handle Teams
	- [X] Player teams
	- [X] Shared FOV on Partied Teams
	- [ ] Team relationship change when hostile act taken
- [ ] AI
	- [X] Basic
		- [X] Pick random visible target
		- [X] Can Mele attack someone in range
		- [X] Can move in range of a seen target
		- [X] Can roam if no target in sight
	- [ ] Level1
		- [ ] Can use creature target spells
		- [ ] Can use distance attack
		- [ ] Can pick closest target
	- [ ] Level2
		- [ ] Can use healing spells
		- [ ] Can use AOE spells
	- [ ] Level3
		- [ ] Can use Effect spells ( Hold person etc)
		- [ ] Can use potions and objects
- [X] Save management [See Save Design](documentation/save_design.md)
	- [X] Save Creature Design
	- [X] Save Lobby configuration
	- [X] Save Battle State
		- [X] Resume terrain state
		- [X] Resume Battle conditions ( haste, haste fatigue etc)
			Consider condition configuration restauration [See Condition save design](documentation/condition_system.md#saving-and-loading-considerations)
		- [X] Resume Concentration	
		- [X] Resume Services state ( we have a couple of services that up to this hold some state, but that should be reconsidered)
- [X] Character Creation 
	- [x] Species choice
	- [x] Class Choice
	- [X] Equipement Choice
	- [x] Statistics Choice
	- [X] Name Choice
	- [X] Species/Class configuration ( variant, ability choice and so on)
		- [X] Class
		- [X] Species
		- [X] Make choice detail view
- [ ] Handle Species properties
	- [ ] Reconcile meter distance with tile size ( everywhere should be meters, the conversion should be at the hexcoortool level, so terrain)
	- [X] Handle Elf Specific wizard cantrip choice
	- [X] Hande Species choices
	- [ ] Species specific to check :
		- [X] Dwarf
			- [X] Poison resilience gives advantage against poison ( not the case right now ) 
			- [X] Test poison resilience for poisosn damage effect 
		- [ ] Mountain Dwarf
			- [ ] Weapon masteries
		- [ ] Hill Dwarf
			- [ ] Dwarf Tenacity Your hit point maximum increases by 1, and it increases by 1 every time you gain a level.
- [ ] Handle class specific abilities
	-[ ] Wizard
		- [ ] Arcane recovery configuration (see rules, but for now it only recover one spell slot per accessible level for wizard level and usable in middle of combat)
- [ ] Make actions saved and restored
- [ ] Move Spell casting ability to the cast spell itself
	Blocked By "Make actions saved and restored"
- [ ] Lights
- [ ] Outside combat game design
	- [ ] Lobby
		- [ ] Lobby listing
		- [ ] Save Sharing
		- [ ] Team Picking
	- [ ] Encounter maker
		- [ ] Introduce 2 possible enemies
		- [ ] Generate encounters based on the wished difficulty
		- [ ] Generate ( and save generated) terrain
	- [ ] XP system
	- [ ] Character screens
		- [ ] Spell preparation
	- [ ] Resting
- [X] Line of sight
- [ ] Weapon Mastery
	 - [ ] Add condition on weapon mastery bonus
	 - [ ] Add races (mountain dwarf ) and classes mastery bonus
- [ ] Game Event Log List Display ( and maybe rework displays)
- [ ] Creature Armor Support 
- [ ] Creature Inventory management
- [X] Full implem of magic missile
	- [x] D4+1 damage 
	- [x] Target Count Scaling
- [ ] Introduce a better display for action configuration details in the action bar
	Right now the spell levels has an interface, but it's effect are a bit unclear
	Some aditional information such as chance of hitting would also be welcome
	So a custom ui for action details would be good, this should include both spell and other action like attacks, hiding and so on
- [X] Solo and multiplayer ( Done And WIP as it's a system wide feature)

- [ ] Handle Critial hits
- [ ] basic music and sound effect ( Same i don't know the first thing about music and folly ><)
- [ ] make opportunity attack scale by the creature range
- [ ] Handle Rest
	- [ ] Short Rest, Wizard arcane recovery
	- [ ] Long Rest, spell replenishment, health retreival
- [ ] A custom poison damage icon ( now using the acid spray icon)
- [ ] Design a complexe action bar allowing for customisation or handling of a lot of possible action
- [ ] level generation
## Maybe 
- [ ] better object bounce
		Objects can bounce strangely in their tiles see [this strange crate bounce](documentation/CrateBounce.gif)
- [ ] Third dimension
- [ ] Dice Roll modifier and trigger ?? Not sure what i meant by that
- [ ] Try to redesign the dice estimate display 
- [ ] Better Wave Spell Configurator Design
- [ ] handling line of sights and stealth
- [ ] Handle Terrain type 
	- [ ] difficult terrain
	- [ ] flammable terrain ( damage reactive terrain)
- [ ] Handle Fall damage, and hard shock damages
- [ ] better bounce mouvement when rebounding on another terrain item
- [ ] Allow for object throwing/pushing
- [ ] Spell effects configuration needs an overall, it's a mess of uncodified and unstructures system, that is going to be a pain
- [ ] Tasha's concentration not removed when target free's himself, in save "Battle After Tasha's"
- [ ] Dwarf pushed agains't wall by thunder wave doesn't seem to be able to move after
### Bugs
- [x] make mouvement cost use computed path and not arbitrary distance
- [x] actions don't deselect on next_turn
- [x] actions don't deselect on other selection
- [x] Conditions can be double applied
- [x] Haste doesn"t remove haste fatigue on attaching
- [ ] Wave Spell Effect is visible even when out of FOV
- [ ] when pushing a creature out of the haste circle with thunder wave it is double affected by the haste fatigue
- [ ] Huge Performance issue with spectator
- [ ] Can be pushed out of terrain when dead
- [ ] Incoherent Terrain object movement between clients, see [this capture](documentation/CrateBounce.gif)
- [ ] Check if second wind heal is coherent ( Seems to always be 2)
- [ ] Check if opportunity attack is working
# On the art and credit
So keen eyes ( And not so keen eyes) may have notices that most of the art is lifted from other source

Most of the icons are taken from the great [Games-Icons](https://game-icons.net)
Great resource for prototyping

Some other may be, badly, redrawn from other sources.
I don't intend to keep them in the project for production.

So here is a list, i will try to keep as exaustive as possible of resources that should be remade before a "production" build could be considered or at least correctly licenced
### background art
![alt text](assets/Backgrounds/GrassHills.png)
### Condition icons
The haste icon

![alt text](assets/UI/Conditions/Haste.png)

redrawn from the baldur's gate one 

### Abilities Icons

The SecondWind Icon redrawn over an online repository

![alt text](assets/UI/Actions/Buttons/SecondWind.png)
### Potential source for artwork
https://graphicriver.net
https://itch.io/game-assets
### Assets packs used:
https://xyezawr.itch.io/free [here](assets/assets_packs/Fx_pack.rar)
https://craftpix.net/freebies/free-warrior-pixel-art-sprite-sheets/ [here](assets/assets_packs//craft-pix-free-warrior/)
https://zerie.itch.io/tiny-rpg-character-asset-pack [Here](assets/assets_packs//Tiny%20RPG%20Character%20Asset%20Pack%20v1.03%20-Full%2020%20Characters/)
https://jimemosqueda.gumroad.com/l/dndicons ( Replace it in prod this is not for commercial use)
# Technical notes

I will try to document as i go along, asumptions, and conventions that i introduce, as well as choices i make here, of a technical nature
Interfaces for classes, node settigns ( z draw order for example)

## Z-Index Ranges:

- [0-9] background
- [10-19] terrain
	- 10: terrain tiles
	- 13: terrain effects
	- 15: terrain_object and minis
	- 16: spells
	- 18: Fog Of War
- [20-29] Terrain UI
- [30-39] General UI

## Visual Layers
Rendering layers for the visual elements
- 1: All visible elements
- 2: Elements not visible for the player
- 3: Debug

## Collision layers
Layers for any colliding object (area,body etc)
- 1: Occluding objects
- 2: Non occluding object
