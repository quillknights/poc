import tkinter as tk
import sys
import pyperclip
import re

NATIVE_TYPES = {"bool", "String", "int", "float"}
RESOURCE_TYPE = {"Weapon", "Armor", "GameClassLevel"}

def get_var_load_line(var):
    name = var["name"]
    if var["type"] in NATIVE_TYPES:
        return f'\t{name} = save["{name}"]'
    if "Array" in var["type"]:
        type = var['type']
        return f'''\tvar new_{name} :{type} = []
\tnew_{name}.append_array(saver.from_save(save["{name}"])) # TODO: Adapt array loading
\t{name} = new_{name}'''
    if var["type"] in RESOURCE_TYPE:
        class_instance_id = "Resource"
    else:
        class_instance_id = var["type"]
    return f'\t{name}  = saver.from_save(save["{name}"])'

def get_load_function(vars):
    save_assignement_string = "\n".join(
        "\t" + get_var_load_line(var) for var in vars
    )
    return f"""
\tfunc from_save(save:Dictionary,saver:GameElementSaver):
{save_assignement_string}
"""

def get_var_save_line(var):
    name = var["name"]
    if var["type"] in NATIVE_TYPES:
        return f'\t"{name}":{name}'
    return f'\t"{name}":saver.to_save({name})'

def get_save_function(var_dicts):
    save_dict_string = ",\n".join([get_var_save_line(var) for var in var_dicts])
    f = f"""
\tfunc to_save(saver:GameElementSaver)->Dictionary:
\t\tvar data = {{
{save_dict_string}
\t\t}}
\t\treturn data
"""
    return f

def get_class_save_id(class_name):
    return f'"{class_name}"'

def get_var_dict(var):
    declaration, type, *other = var.split(":")
    declaration = declaration.split(" ")
    i = 0
    while declaration[i] != "var":
        i += 1
    name = declaration[i + 1].strip()
    type = type.split(" ")[0]
    if "=" in type:
        type = type.split("=")[0]
    type = type.strip()
    return {"name": name, "type": type}

def get_vars_dicts(vars):
    d = []
    for v in vars:
        if v.strip() != "" and ":" in v:
            d.append(get_var_dict(v))
    return d

def extract_variables(code):
    variable_pattern = r'var\s+(\w+)\s*:\s*(\w+)'
    class_pattern = r'class_name\s+(\w+)'
    variable_matches = re.findall(variable_pattern, code)
    class_match = re.search(class_pattern, code)
    variables = []
    class_name = None
    if class_match:
        class_name = class_match.group(1)
    for match in variable_matches:
        var_name, var_type = match
        variables.append(f'var {var_name} : {var_type}')
    return variables, class_name

def process_input(input_text):
    variables, class_name = extract_variables(input_text)
    vars = get_vars_dicts(variables)
    output_text = get_save_function(vars)
    output_text += "\n"
    output_text += get_load_function(vars)
    output_text = f"""
func get_class_save_id() -> String:
\treturn {get_class_save_id(class_name)}
""" + output_text
    return output_text

def on_paste(event):
    input_text = event.widget.get("1.0", tk.END)
    output_text = process_input(input_text)
    output_box.delete("1.0", tk.END)
    output_box.insert(tk.END, output_text)

def select_output_text():
    output_box.tag_add("sel", "1.0", tk.END)
    output_box.focus_set()

if sys.argv[1] == "gui":
    root = tk.Tk()
    root.title("Input-Output App")

    input_box = tk.Text(root, height=15, width=100)
    input_box.pack()

    output_box = tk.Text(root, height=15, width=100)
    output_box.pack()

    select_button = tk.Button(root, text="Select Output", command=select_output_text)
    select_button.pack()

    input_box.bind("<<Modified>>", on_paste)

    root.mainloop()
else:
    print("This tool generates a quick and dirty method for serializing and deserializing an object, from its var declaration")
    print("Generated code MUST BE read and corrected, don't expect this to work ")
    print("Paste your Godot class code, and d or Done when over")
    code = ""
    while True:
        line = input()
        if line.lower() in ["d", "done"]:
            break
        code += line + "\n"
    output_text = process_input(code)
    print("============ Save Function =========== \n\n")
    print(output_text)