
# I am not that good at statistics, so this is a bit of code to try out my formulas for dice result estimate
from random import * 
def roll(size,minimum,maximum):
	return [randint(minimum,maximum) for i in range(size)]
def roll_advantage(size,minimum,maximum):
	return [(randint(minimum,maximum),randint(minimum,maximum)) for i in range(size)]
def success_rate(dc,rolls,mod):
	count = 0
	for roll in rolls:
		if dc<=(roll+mod):
			count+=1
	return count/len(rolls)
def success_rate_advantage(dc,rolls,mod):
	count = 0
	for roll in rolls:
		if dc<=(roll[0]+mod )or dc<=(roll[1]+mod):
			count+=1
	return count/len(rolls)
def success_rate_disadvantege(dc,rolls,mod):
	count = 0
	for roll in rolls:
		if dc<=(roll[0]+mod )and dc<=(roll[1]+mod):
			count+=1
	return count/len(rolls)
def classic_success(maximum,minimum,dc,modifier):
	r = abs(maximum-minimum)+1
	return 1-((dc-1-modifier)/r)
def adv_success(maximum,minimum,dc,modifier):
	r = abs(maximum-minimum)+1
	return 1-pow((dc-modifier-1)/r,2)
def dis_success(maximum,minimum,dc,modifier):
	r= abs(maximum-minimum)+1
	return pow(1-((dc-modifier-1)/r),2)

MINIMUM=1
MAXIMUM=20
SAMPLE_SIZE=1000000
DC=10
MODIFIER = 0

simple_rolls = roll(SAMPLE_SIZE,MINIMUM,MAXIMUM)
print(f"Sucess rate for a normal roll, between {MINIMUM} and {MAXIMUM} with dc {DC} is {success_rate(DC,simple_rolls,MODIFIER)}")
advantage_roll = roll_advantage(SAMPLE_SIZE,MINIMUM,MAXIMUM)
print(f"Sucess rate for a advantage roll, between {MINIMUM} and {MAXIMUM} with dc {DC} is {success_rate_advantage(DC,advantage_roll,MODIFIER)}")
print(f"Sucess rate for a disadventage roll, between {MINIMUM} and {MAXIMUM} with dc {DC} is {success_rate_disadvantege(DC,advantage_roll,MODIFIER)}")
print(f"Computed for: classic {classic_success(MAXIMUM,MINIMUM,DC,MODIFIER)} advantage {adv_success(MAXIMUM,MINIMUM,DC,MODIFIER)} dis {dis_success(MAXIMUM,MINIMUM,DC,MODIFIER)}")

for modifier in range(MINIMUM,MAXIMUM+1):
	for dc in range(MINIMUM,MAXIMUM+MODIFIER+1):
		if abs(success_rate(dc,simple_rolls,modifier)-round(classic_success(MAXIMUM,MINIMUM,dc,modifier),4))>0.001:
			print(f"Failed classic rate compute dc {dc} mod {modifier} {success_rate(dc,simple_rolls,modifier)} vs {classic_success(MAXIMUM,MINIMUM,dc,modifier)}")
		if abs(success_rate_advantage(dc,advantage_roll,modifier)-round(adv_success(MAXIMUM,MINIMUM,dc,modifier),4))>0.001:
			print(f"Failed advantage rate compute dc {dc} mod {modifier} {success_rate_advantage(dc,advantage_roll,modifier)} vs {adv_success(MAXIMUM,MINIMUM,dc,modifier)}")
		if abs(success_rate_disadvantege(dc,advantage_roll,modifier)-round(dis_success(MAXIMUM,MINIMUM,dc,modifier),4))>0.001:
			print(f"Failed classic rate compute dc {dc} mod {modifier} {success_rate_disadvantege(dc,advantage_roll,modifier)} vs {dis_success(MAXIMUM,MINIMUM,dc,modifier)}")
		
print("examined all range")