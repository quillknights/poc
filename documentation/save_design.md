# Save Design

_ Goal: Be able to store the current state of an adventure, from the character general profile to the specifics of a battle that is in progress_

## Needed data:
_ What we need to keep, not necesarly what is directly saved, buffs born from conditions for example don't need to be stored, it's the condition that applyes them_
- Creature States:
	- Creature Sheet
		- Icon
		- Name
		- Class levels ( and their configuration)
		- Species
		- Statistics
	- Creature Battle State
		- Position
		- Conditions
			- Condition states and type
		- Health
		- Battle Resources
		
- Battle Event log history
- Terrain State:
	- Terrain effects
	- Terrain Objects
		- Position
		- Health
## Needed Restoration Behavior
_What needs to be reinitated, reset etc, from saved data, on reload. Listeners, condition attachements and so on_

- Unknown

## Save File Structure
_We need some mechanisme for reference restauration_
Save file is a json dictionary ( the byte save file is not yet determined, but it will certainly be similar)
```json
	{
		"root_id": root saved object instance id,
		"instance_registry":Dictionary id:save_dict of instances
	}
```
When saving a instance registry is built
The first time a savable variant is saved, the saver object adds it to the instance registry with a unique id and return the used id
Next times the savable is processed for saving by the same saver, no conversion is done, and a the id is simply return

This works with theoricaly any savable as root, but for general game saving we use the GameState savable as root