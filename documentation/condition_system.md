# Condition System

Conditions are time bound effects that attach to a creature and may affect it's behavior, in the most general sense

For example conditions may come from:

- Spells
- Creature Species
- Terrain Effects
- Ennemy attack
- Class

## Data model

This model is born out of developement history, i started with having only a resource, and later on needed to split that into node and resource as a resource is not meant to be duplicated, and don't behave very well when you try to replicate it

- CreatureConditionDesign: Resource
  The general template for a condition, contains the condition behavior and so on, BUT is not a condition instance, merely the template for a condition, Is unique accros the game
- CreatureCondition	:Node
  The condition instance, attaches to a creature and hold the instance specific configuration
  Exist per condition applyed on any creature

## Lifecycle

The condition has generaly two lifecycle hooks `attach and detach`
They are triggered by the server, based on the unique resource id we want to attach, and a unique instance id used to identify the condition accross the game instances

```mermaid
flowchart LR
ca(["Creature.attach_condition(design_id,instance_id)"])
cca(["CreatureCondition.attach(creature)"])
ccdc(["CreatureConditionDesign.configure(condition,creature)"])
ccturndelay(["CreatureCondition.attach_turn_delay()"])
ccda(["CreautreConditionDesign.attach(creature,condition)"])
ca --> cca 
cca --1-->ccdc
cca --2-->ccturndelay
cca --3-->ccda

cd(["Creature.detach_condition(instance_id)"])
ccd(["CreatureCondition.detach(creature)"])
ccdturndelay(["CreatureCondition.detach_turn_delay()"])
ccdd(["CreautreConditionDesign.detach(creature,condition)"])
cd --> ccd
ccd --1-->ccdturndelay
ccd --2-->ccdd
ccd --3-->on_detach
```


## Condition effect hooks:

the conditions effects hook are not a static list
As they are meant to be able to affect a lot of things, the plugin hooks will grow other time
This may become an issue of both definition and cross effects so we want to keep this in check

- turn_start
- turn end
- dice roll
- the condition target difficulty class on a dice ( AC or DC of and adversary ) ! THIS SHOULD BE RENAMED!
- the targets actions ( this is not used, so definition is still unclear)
- damages ( both dealt and taken )

## Saving and Loading considerations

Two sticking point
1. Condition restauration, when comming from alreay active sources
2. Configuration restauration

### 1. Condition Restauration

Temporary solution (LOL) : Rely on the non stacking effects to avoid double applying, when the probleme of condition persistence arrises realy
treat it instance by instance, as the surounding circumstances will have evolved

This stays an issue, as we have incoherent behavior accross potential condition sources.

GameClasses are restaured and reattached, so they reattach their potential conditions, but they are resources, so no instance custom loading there

Terrain Effects are nodes, and so customization may be possible here, and safeguarding against reapplying a condition is feasable

As we intend to convert most resources that have instance specific behavior to nodes, and so instance serializable data this is a probleme that is going to disapear. For now marking which condition is savable and which should not be saved will be acceptable


### 2. Configuration restauration

Decision: Introduce ConditionState, separate from condition config

A config is something imparted by the design and that is not lifecycle dependent.

A state is something lifecycle dependent and that MUST be serializable.

Disucssion

The resource part of the system should be saved and loaded from resource path.
This means that any data that is instance specific must be in serialized and deserialized in the condition itself

This is ok for the condition but poses probleme for it's configuration, as configs are basicaly under the perview of the resource and not the condition. 

A custom serialization system would mean taht any object handled by the standard saver would not be viable attribute for a configuration, wich render the config useless

Same goes for asking the resource to restore the config: a config is meant to hold condition state ( turn remaining that kind of thing ) this information would be lost in translation

So the question is

How do we include the configuration in the standard saving system without ballooning the number of serializable class.

Answer is :We don't

Configs include things that are not meant to be serializable, other nodes that will not end up in the serialized system and so on, and must be dynamicaly retreived upon reloading

If needed we can extract the notion of state from our config, and treat it as a separate concern. To simplify thing we might use a dictionary, with the explicit rule that any item in the dict MUST BE SERIALIZABLE ( the standart saver should throw an error if this is not the case so we are a little bit safeguarded there)

<style>#mermaid-1722069650206{font-family:sans-serif;font-size:16px;fill:#333;}#mermaid-1722069650206 .error-icon{fill:#552222;}#mermaid-1722069650206 .error-text{fill:#552222;stroke:#552222;}#mermaid-1722069650206 .edge-thickness-normal{stroke-width:2px;}#mermaid-1722069650206 .edge-thickness-thick{stroke-width:3.5px;}#mermaid-1722069650206 .edge-pattern-solid{stroke-dasharray:0;}#mermaid-1722069650206 .edge-pattern-dashed{stroke-dasharray:3;}#mermaid-1722069650206 .edge-pattern-dotted{stroke-dasharray:2;}#mermaid-1722069650206 .marker{fill:#333333;}#mermaid-1722069650206 .marker.cross{stroke:#333333;}#mermaid-1722069650206 svg{font-family:sans-serif;font-size:16px;}#mermaid-1722069650206 .actor{stroke:hsl(259.6261682243,59.7765363128%,87.9019607843%);fill:#ECECFF;}#mermaid-1722069650206 text.actor > tspan{fill:black;stroke:none;}#mermaid-1722069650206 .actor-line{stroke:grey;}#mermaid-1722069650206 .messageLine0{stroke-width:1.5;stroke-dasharray:none;stroke:#333;}#mermaid-1722069650206 .messageLine1{stroke-width:1.5;stroke-dasharray:2,2;stroke:#333;}#mermaid-1722069650206 #arrowhead path{fill:#333;stroke:#333;}#mermaid-1722069650206 .sequenceNumber{fill:white;}#mermaid-1722069650206 #sequencenumber{fill:#333;}#mermaid-1722069650206 #crosshead path{fill:#333;stroke:#333;}#mermaid-1722069650206 .messageText{fill:#333;stroke:#333;}#mermaid-1722069650206 .labelBox{stroke:hsl(259.6261682243,59.7765363128%,87.9019607843%);fill:#ECECFF;}#mermaid-1722069650206 .labelText,#mermaid-1722069650206 .labelText > tspan{fill:black;stroke:none;}#mermaid-1722069650206 .loopText,#mermaid-1722069650206 .loopText > tspan{fill:black;stroke:none;}#mermaid-1722069650206 .loopLine{stroke-width:2px;stroke-dasharray:2,2;stroke:hsl(259.6261682243,59.7765363128%,87.9019607843%);fill:hsl(259.6261682243,59.7765363128%,87.9019607843%);}#mermaid-1722069650206 .note{stroke:#aaaa33;fill:#fff5ad;}#mermaid-1722069650206 .noteText,#mermaid-1722069650206 .noteText > tspan{fill:black;stroke:none;}#mermaid-1722069650206 .activation0{fill:#f4f4f4;stroke:#666;}#mermaid-1722069650206 .activation1{fill:#f4f4f4;stroke:#666;}#mermaid-1722069650206 .activation2{fill:#f4f4f4;stroke:#666;}#mermaid-1722069650206:root{--mermaid-font-family:sans-serif;}#mermaid-1722069650206:root{--mermaid-alt-font-family:sans-serif;}#mermaid-1722069650206 sequence{fill:apa;}</style>
