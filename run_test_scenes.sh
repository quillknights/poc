#!/bin/bash

GODOT=../Godot_4.2 ## This is because I put my Godot right above my project

results_dir=$(mktemp -d)
declare -a pids
declare -a files
declare -a statuses

function process_file() {
    local file="$1"
    local result_file="$2"
    
    # Suppress the actual output of the tests
    if timeout 1m $GODOT --time-scale 10 "$file" &> /dev/null; then
        echo "Success $file" > "$result_file"
    else
        result=$?
        if [ $result -eq 124 ]; then
            echo "Failed $file: Timeout after 1 minute" > "$result_file"
        else
            echo "Failed $file: Exit code $result" > "$result_file"
        fi
    fi
}

function display_progress() {
    echo -ne "\033[2J"  # Clear the entire screen
    echo -ne "\033[H"   # Move cursor to the top-left corner

    echo "In progress:"
    for idx in "${!files[@]}"; do
        if [ "${statuses[$idx]}" == "done" ]; then
            echo -e "\033[0;32mCompleted ${files[$idx]}\033[0m"  # Green color for completed
        else
            echo -e "\033[1;33mRunning ${files[$idx]} (PID: ${pids[$idx]})\033[0m"  # Yellow color for in-progress
        fi
    done
}

# Find all test files and run them in parallel
index=0
while IFS= read -r -d '' file; do
    files[$index]="$file"
    statuses[$index]="in progress"
    result_file="$results_dir/result_$index.txt"
    process_file "$file" "$result_file" &
    pids[$index]=$!
    index=$((index + 1))
done < <(find "./scenes/test_scenes" -name "test_*.tscn" -type f -print0)

# Initial display
display_progress

# Wait for all parallel jobs to complete and update statuses
while [[ ${#pids[@]} -gt 0 ]]; do
    for idx in "${!pids[@]}"; do
        if ! kill -0 "${pids[$idx]}" 2> /dev/null; then
            statuses[$idx]="done"
            unset 'pids[$idx]'
            display_progress
        fi
    done
    sleep 1
done

# Collect the results
echo "Recap:"
for result_file in "$results_dir"/*.txt; do
    result=$(cat "$result_file")
    if [[ $result == Success* ]]; then
        echo -e "\033[0;32m$result\033[0m"  # Green color for success
    else
        echo -e "\033[0;31m$result\033[0m"  # Red color for failure
    fi
done

# Clean up temporary files
rm -r "$results_dir"

#$GODOT -d --time-scale 10 scenes/test_scenes/save_tests.tscn 
#$GODOT -d --time-scale 10 scenes/test_scenes/composite_condition_tests.tscn
